/**
 * List of Environments
 * DEV  :   Development
 * QAS  :   Qaulity Assurance (UAT)
 * PRD  :   Production
 */
const env = "DEV";
let envconfigs = {};
// envconfigs.CHANGIGMC_URL = "https://www.changigmc.com/";
envconfigs.ACTION_GET_GATES_DATA = "getGates"; //get
envconfigs.ACTION_GET_CLEANED_GATES_DATA = "getCleanedFlights"; //get
envconfigs.ACTION_ON_GATES_UPDATE = "onGateUpdate"; //websocket updates to all active connection
envconfigs.ACTION_UPDATE_VALIDATE_INSTRUCTION = "update"; //update
envconfigs.TIME_CAP = 10; //in minutes
envconfigs.HEARTBEAT_INTERVAL = 600000; //in mili seconds

if (env === "DEV") {
  envconfigs.DEBUG = true;
  envconfigs.USERPOOLID = "ap-southeast-1_2DXL7Ub0Y";
  envconfigs.CLIENTID = "3tb8ld30oprrveaf54523facmt";
  envconfigs.LOGINS = "cognito-idp.ap-southeast-1.amazonaws.com/ap-southeast-1_2DXL7Ub0Y";
  // envconfigs.SOCKET_ROOT_URL = "wss://j4hu4g1fg3.execute-api.ap-southeast-1.amazonaws.com/devW2C?access_token=";
  //socket URL for where2clean
  envconfigs.SOCKET_ROOT_URL = "wss://48citdq98i.execute-api.ap-southeast-1.amazonaws.com/devW2C?user_id=";
  envconfigs.MEDIA_PATH = "/content/dam/where2clean/static/media/";
  envconfigs.USERNAME = ""; //CAG_T3_FM
  envconfigs.PASSWORD = ""; //Manager@T3FM
} else if (env === "QAS") {
  envconfigs.DEBUG = false;
  envconfigs.USERPOOLID = "ap-southeast-1_2DXL7Ub0Y";
  envconfigs.CLIENTID = "3tb8ld30oprrveaf54523facmt";
  envconfigs.LOGINS = "cognito-idp.ap-southeast-1.amazonaws.com/ap-southeast-1_2DXL7Ub0Y";
  envconfigs.SOCKET_ROOT_URL =
    "wss://48citdq98i.execute-api.ap-southeast-1.amazonaws.com/devW2C?user_id=";
  envconfigs.MEDIA_PATH = "/content/dam/where2clean/static/media/";
  envconfigs.USERNAME = "";
  envconfigs.PASSWORD = "";
} else if (env === "PRD") {
  envconfigs.DEBUG = false;
  envconfigs.USERPOOLID = "ap-southeast-1_j60r1COm4";
  envconfigs.CLIENTID = "7orh8io92s6e2l8oa120n94o74";
  envconfigs.LOGINS = "cognito-idp.ap-southeast-1.amazonaws.com/ap-southeast-1_j60r1COm4";
  envconfigs.SOCKET_ROOT_URL =
    "wss://vjt998nuvj.execute-api.ap-southeast-1.amazonaws.com/prodW2C?user_id=";
  envconfigs.MEDIA_PATH = "/content/dam/where2clean/static/media/";
  envconfigs.USERNAME = "";
  envconfigs.PASSWORD = "";
}

export default envconfigs;
