/* sort array list by a key field */
export const sortByKey = (array, key, des, num) => {
  var descending = false;
  var numeric = true;
  if (des == null) {
    descending = false;
  } else {
    descending = des;
  }
  if (num == null) {
    numeric = true;
  } else {
    numeric = num;
  }

  if (descending) {
    return array.sort(function(a, b) {
      var x = String(a[key]);
      var y = String(b[key]);

      return y.localeCompare(x, "en", {
        sensitivity: "base",
        numeric: numeric
      });
    });
  } else {
    return array.sort(function(a, b) {
      var x = String(a[key]);
      var y = String(b[key]);
      return x.localeCompare(y, "en", {
        sensitivity: "base",
        numeric: numeric
      });
    });
  }
};
export const uniqueByKey = (arr, key) => {
  //filter unique elements from an array list by a key
  return sortByKey(arr, key).filter(function(value, index, array) {
    return index === 0 || value[key] !== array[index - 1][key];
  });
};

export const getUrlParameter = (url, paramname) => {
  paramname = paramname.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + paramname + "=([^&#]*)");
  var results = regex.exec(url);
  return results === null
    ? ""
    : decodeURIComponent(results[1].replace(/\+/g, " "));
};

export const validateEmail = email => {
  var re = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`|~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
  return re.test(String(email).toLowerCase());
};

export const validateName = name => {
  var re = /^[a-z ,.'-]+$/;
  return re.test(String(name).toLowerCase());
};

export const validateReceipt = receiptno => {
  var re = /^(\d{4})$/;
  return re.test(String(receiptno).toLowerCase());
};

export const validatePassword = password => {
  var re = /^(?=.*\d)(?=.*[a-zA-Z]).{8,}$/;
  return re.test(String(password).toLowerCase());
};

export const setCookie = (cname, cvalue, exseconds) => {
  var d = new Date();
  if (exseconds === 0) {
    d.setUTCHours(
      16,
      0,
      0
    ); /*set UTC time 16:00 hour as expirey of cookie as per current requirement.*/
  } else {
    d.setTime(d.getTime() + exseconds * 1000);
  }

  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
};

export const getCookie = cname => {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) === " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
};

export const getDistanceFromLatLonInKm = (lat1, lon1, lat2, lon2) => {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2 - lat1); // deg2rad below
  var dLon = deg2rad(lon2 - lon1);
  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) *
      Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c; // Distance in km
  return d;
};

const deg2rad = deg => {
  return deg * (Math.PI / 180);
};

export const doesFileExist = urlToFile => {
  var xhr = new XMLHttpRequest();
  xhr.open("HEAD", urlToFile, false);
  xhr.send();

  if (xhr.status == "404") {
    console.log("File doesn't exist");
    return false;
  } else {
    console.log("File exists");
    return true;
  }
};


export const groupBy = (objectArray, property) => {
  return objectArray.reduce(function (acc, obj) {
    let key = obj[property]
    if (!acc[key]) {
      acc[key] = []
    }
    acc[key].push(obj)
    return acc
  }, {})
}


export const sortByLetterFirst = (objectArray, objectype) => {
  let OriginalArray = objectArray.sort();
  let FirstArray = [];
  let SecondArray = [];

  if (objectype == "JSON") {
    try {
      OriginalArray.map((data,index) => {
        if ( data['value'].includes("00s") ){
          SecondArray.push(data);
        } else {
          FirstArray.push(data);
        }
      })
      console.log("inside sortByLetterFirst");

      console.log(SecondArray.sort());
      FirstArray = FirstArray.sort(function(a, b){
        var nameA=a.value.toLowerCase(), nameB=b.value.toLowerCase()
        if (nameA < nameB) //sort string ascending
            return -1 
        if (nameA > nameB)
            return 1
        return 0 //default return value (no sorting)
      });
      SecondArray = SecondArray.sort(function(a, b){
        var nameA=a.value.toLowerCase(), nameB=b.value.toLowerCase()
        if (nameA < nameB) //sort string ascending
            return -1 
        if (nameA > nameB)
            return 1
        return 0 //default return value (no sorting)
      });
      OriginalArray = FirstArray;
      OriginalArray = OriginalArray.concat(SecondArray);
    } catch (error) {
      
    }
  } else {
    try {
      OriginalArray.map((data,index) => {
        if ( data.includes("00s") ){
          SecondArray.push(data)
        } else {
          FirstArray.push(data);
        }
      })
    
      OriginalArray = FirstArray;
      OriginalArray = OriginalArray.concat(SecondArray);
    } catch (error) {
      
    }
  }

  return OriginalArray;
}

export const uniqueArray = (a) => {
  return a.sort().filter(function(item, pos, ary) {
      return !pos || item != ary[pos - 1];
  })
}