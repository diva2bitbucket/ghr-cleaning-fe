
//backend status
const allStatus = ["CLEANING", "AVAILABLE", "CLEANED", "NO CLEANING REQUIRED", "NOT AVAILABLE"];
const availableStatus = ["CLEANING", "AVAILABLE"];
const toBeCleanedStatus = ["CLEANING", "AVAILABLE", "NOT AVAILABLE"];
const cleanedStatus = ["CLEANED"];

//front end status
const Status = [
    {
        name: 'ALL',
        id: 1,
        value: 'All'
    },
    {
        name: 'ALL AVAILABLE',
        id: 2,
        value: 'Available'
    },
    {
        name: 'TO BE CLEANED',
        id: 3,
        value: 'ToBeCleaned'
    },
    {
        name: 'CLEANED',
        id: 4,
        value: 'Cleaned'
    },
];

const Terminal = [
    {
        name: 'ALL',
        id: 0,
        value: 'ALL'
    },
    {
        name: 'T1',
        id: 1,
        value: 'T1'
    },
    {
        name: 'T2',
        id: 2,
        value: 'T2'
    },
    {
        name: 'T3',
        id: 3,
        value: 'T3'
    },
    {
        name: 'T4',
        id: 4,
        value: 'T4'
    },
];

//all callsign
const AllCallSign = [
    {
        name: 'ALL',
        id: 0,
        value: 'ALL'
    },
    {
        name: 'COMPASS 4',
        id: 1,
        value: 'COMPASS 4'
    },
    {
        name: 'DELTA 3',
        id: 2,
        value: 'DELTA 3'
    },
    {
        name: 'DELTA 4',
        id: 3,
        value: 'DELTA 4'
    },
    {
        name: 'DELTA 5',
        id: 4,
        value: 'DELTA 5'
    },
    {
        name: 'DELTA 6',
        id: 5,
        value: 'DELTA 6'
    },
    {
        name: 'ECHO 4',
        id: 6,
        value: 'ECHO 4'
    },
    {
        name: 'ECHO 7',
        id: 7,
        value: 'ECHO 7'
    },
    {
        name: 'ECHO 8',
        id: 8,
        value: 'ECHO 8'
    },
    {
        name: 'ECHO 9',
        id: 9,
        value: 'ECHO 9'
    },
    {
        name: 'VICTOR 7',
        id: 10,
        value: 'VICTOR 7'
    },
    {
        name: 'BETA 4',
        id: 11,
        value: 'BETA 4'
    },
    {
        name: 'BETA 6',
        id: 12,
        value: 'BETA 6'
    },
    {
        name: 'BETA 7',
        id: 13,
        value: 'BETA 7'
    },
    {
        name: 'BETA 8',
        id: 14,
        value: 'BETA 8'
    },
    {
        name: 'BETA 9',
        id: 15,
        value: 'BETA 9'
    },
    {
        name: 'ZETA 9',
        id: 16,
        value: 'ZETA 9'
    },
    {
        name: 'ZETA 10',
        id: 17,
        value: 'ZETA 10'
    },
];


//callsign
const T1CallSign = [
    {
        name: 'ALL',
        id: 0,
        value: 'ALL'
    },
    {
        name: 'COMPASS 4',
        id: 1,
        value: 'COMPASS 4'
    },
    {
        name: 'DELTA 3',
        id: 2,
        value: 'DELTA 3'
    },
    {
        name: 'DELTA 4',
        id: 3,
        value: 'DELTA 4'
    },
    {
        name: 'DELTA 5',
        id: 4,
        value: 'DELTA 5'
    },
    {
        name: 'DELTA 6',
        id: 5,
        value: 'DELTA 6'
    },
];

const T2CallSign = [
    {
        name: 'ALL',
        id: 0,
        value: 'ALL'
    },
    {
        name: 'ECHO 4',
        id: 1,
        value: 'ECHO 4'
    },
    {
        name: 'ECHO 7',
        id: 2,
        value: 'ECHO 7'
    },
    {
        name: 'ECHO 8',
        id: 3,
        value: 'ECHO 8'
    },
    {
        name: 'ECHO 9',
        id: 4,
        value: 'ECHO 9'
    },
    {
        name: 'VICTOR 7',
        id: 5,
        value: 'VICTOR 7'
    },
];

const T3CallSign = [
    {
        name: 'ALL',
        id: 0,
        value: 'ALL'
    },
    {
        name: 'BETA 4',
        id: 1,
        value: 'BETA 4'
    },
    {
        name: 'BETA 6',
        id: 2,
        value: 'BETA 6'
    },
    {
        name: 'BETA 7',
        id: 3,
        value: 'BETA 7'
    },
    {
        name: 'BETA 8',
        id: 4,
        value: 'BETA 8'
    },
    {
        name: 'BETA 9',
        id: 5,
        value: 'BETA 9'
    },
];

const T4CallSign = [
    {
        name: 'ALL',
        id: 0,
        value: 'ALL'
    },
    {
        name: 'ZETA 9',
        id: 1,
        value: 'ZETA 9'
    },
    {
        name: 'ZETA 10',
        id: 2,
        value: 'ZETA 10'
    },
];

export {
    allStatus,
    availableStatus,
    toBeCleanedStatus,
    cleanedStatus,
    Status,
    Terminal,
    AllCallSign,
    T1CallSign,
    T2CallSign,
    T3CallSign,
    T4CallSign
};