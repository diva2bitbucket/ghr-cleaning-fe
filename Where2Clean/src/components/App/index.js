import React from "react";
import { Route } from "react-router-dom";
import { CookiesProvider } from 'react-cookie';
// import variables from "../../variables.scss";
import Home from "../Home";
import Login from "../Login";
import ATCO from "../ATCO";
// import GMC from "../GMC";
// import PBRHOME from "../GMC/home";
import './app.scss';

const App = props => {
  return (
    <CookiesProvider>
      <React.Fragment>
        <div className="main-container"
        // style={{
        //   padding: 0,
        //   maxWidth: variables.appMaxWidth,
        //   boxShadow: "0 0 16px 2px rgba(0,0,0,.25)",
        //   marginRight: "auto",
        //   marginLeft: "auto",
        //   background: `${variables.appBackgroundColor}`,
        //   minHeight: '100vh',
        //   color: `${variables.appColor}`
        // }}
        >
          <Route path="/" exact component={Home} />
          <Route path="/login" exact component={Login} />
          <Route path="/dashboard" exact component={ATCO} />
          {/* <Route path="/pbr" exact component={GMC} />
          <Route path="/brhome" exact component={PBRHOME} /> */}

        </div>
      </React.Fragment>
    </CookiesProvider>
  );
};

export default App;
