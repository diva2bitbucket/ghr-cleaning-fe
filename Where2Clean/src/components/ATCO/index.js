import React, { useEffect, useState, useRef, useMemo } from "react";
import { Row, Col, Button, Divider, Input, Icon, Radio, Select, Popover, Alert } from "antd";
import { useCookies } from 'react-cookie';
import { CognitoUserPool } from "amazon-cognito-identity-js";
import AWS from 'aws-sdk';


import variables from "../../variables.scss";
//styles for both web and ruggear
import "./atco.scss";
import "./styles.scss";

// import icon_user from "../../assets/images/icon_user.png";
// import icon_information from "../../assets/images/icon_information.png";

import constants from "../../configs/constants";
import { useSelector, useDispatch } from "react-redux";

import moment from "moment";
// import { sortByKey } from "../../configs/utils";
import { wsConnect, wsDisconnect, wsSend, wsUpdate } from "../../store/pushback";
import { updateUser, restoreSession } from "../../store/session";
import FlipMove from "react-flip-move";
import ModalInformation from "../ModalInformation";
import ChecklistModal from "../ChecklistModal"
import CleaningModal from "../CleaningModal"
import { SearchOutlined, CaretDownOutlined, CaretRightOutlined } from "@ant-design/icons";

//commonValues
import { Status, allStatus, availableStatus, toBeCleanedStatus, cleanedStatus, Terminal, T1CallSign, T2CallSign, T3CallSign, T4CallSign, AllCallSign } from "../../library/commonValues"
// import { QAS_LOGIN, PROD_LOGIN } from "../../library/awsConfig";

import app_logo from "../../assets/images/app_logo.png";
import { usePrevious } from "../../hooks/usePrevious";

// import fakeData from "../../assets/data/fakeData.json";
// import gatesInfo from "../../assets/data/gate_mapping.json";
// import Papa from 'papaparse';
// import aircraft_size_mapping_csv from "../../assets/data/aircraft_size_mapping.csv";
// import gate_mapping_csv from "../../assets/data/gate_mapping.csv";

// import terminalGates from "../../assets/data/terminal_gates.json";

const icon_profile_svg = require("../../assets/images/icon_profile.svg");
const icon_information_svg = require("../../assets/images/icon_information.svg");
const dep_icon = require("../../assets/images/dep-icon.svg");
const arr_icon = require("../../assets/images/arr-icon.svg");


const ATCO = (props) => {
  // console.log("fake data")
  // console.log(fakeData)
  const dispatch = useDispatch();
  const user = useSelector((state) => state.session.user);
  const wsOpened = useSelector((state) => state.pushback.wsOpened);
  const prevWSOpened = usePrevious(wsOpened);
  const gatesData = useSelector((state) => state.pushback.gatesData);
  const cleanedGatesData = useSelector((state) => state.pushback.cleanedGatesData);
  const getUpdateData = useSelector((state) => state.pushback.getUpdateData);
  const gateMapping = useSelector((state) => state.session.gateMapping); //gate mapping store isntance

  const aircraftSizeMapping = useSelector((state) => state.session.aircraftSizeMapping);

  // const gateMapping = useSelector(
  //   (state) => state.session.gateMapping
  // );

  // console.log("gate mapping:", gateMapping);

  const [pushbackData, setPushbackData] = useState();
  

  const [gatesAllData, setGatesAllData] = useState([]);
  const [cleanedAllGatesData, setCleanedAllGatesData] = useState([]);
  const [originalPushbackData, setOriginalPushbackData] = useState([]);
  const [refetch, setRefetch] = useState(false);
  // const [sortKey, setSortKey] = useState("");
  const [searchValue, setSearchValue] = useState("");
  const [infoModalVisibility, setInfoModalVisibility] = useState(false);
  // const [data, setData] = useState();

  // const [callsignFilter, setCallsignFilter] = useState();

  // const [results, setResults] = useState([]);
  // const [funcCalled, setFuncCalled] = useState(false);

  //creating modal checklists
  const [availableModalVisibility, setAvailableModalVisibility] = useState(false);
  const [cleaningModalVisibility, setCleaningModalVisibility] = useState(false);
  //selected value
  const [selectedCallSign, setSelectedCallSign] = useState("ALL");
  const [selectedAllCallSign, setSelectedAllCallSign] = useState("ALL");
  const [selectedStatus, setSelectedStatus] = useState('ToBeCleaned');
  const [selectedTerminal, setSelectedTerminal] = useState('');
  const [selectedAllTerminal, setSelectedAllTerminal] = useState('ALL');
  const [selectedSort, setselectedSort] = useState('Status');
  // const [prioritySelected, setPrioritySelected] = useState([]);

  //get gate for checklist
  const [gate, setGate] = useState()
  const [gateTerminal, setGateTerminal] = useState()
  const [flightId, setFlightId] = useState()
  const [statusModal, setStatusModal] = useState('');
  const [userLocal, setUserLocal] = useState(JSON.parse(localStorage.getItem("userToken")));
  const [direction, setDirection] = useState()
  //cookies
  const [filterCookies, setFilterCookie] = useCookies(['filterCookie']);
  const [errorDisplay, setErrorDisplay] = useState(false);
  const [checkConnection, setCheckConnection] = useState(false);

  console.log("gatesdata:", gatesData);

  useEffect(() => {
    // console.log("ATCO FIRST");
    if (user) {
      let websocketURL = constants.SOCKET_ROOT_URL + user.accessToken.jwtToken;
      // let websocketURL = ""
      // console.log("websocketURL: ", websocketURL);
      dispatch(wsConnect(websocketURL));
      let userTerminal = user.idToken.payload['custom:visible_terminal'];
      // console.log("USER TERMINAL", userTerminal);
      setSelectedTerminal(userTerminal);
      // console.log("selectedTerminal", selectedTerminal);
      // console.log("USER LOCAL INTIAL LOAD WITH USER", userLocal);
      // checkUserTokenValidity();
    }
  }, [user]);

  // on load from local storage
  useEffect(() => {
    if (userLocal) {
      // console.log("RESTORE SESSION");
      dispatch(restoreSession());
      // console.log("ATCO FIRST INITIAL LOAD ON PAGE OF WHERE2CLEAN");
      const getToken = window.localStorage.getItem("accessToken");
      let websocketURL = constants.SOCKET_ROOT_URL + getToken;
      dispatch(wsConnect(websocketURL));
      // setting terminal
      let userTerminal = userLocal.idToken.payload['custom:visible_terminal'];
      setSelectedTerminal(userTerminal);

      //store the userlocal from the local storage to the user store 
      dispatch(updateUser(userLocal));
      // console.log("UPDATE USER WITH USERLOCAL2", user);
    }
  }, []);

  //logouts the user after session is over
  useEffect(() => {
    if (user) {
      setTimeout(() => {
        // console.log("SESSION OVER");
        logoutUser();
      }, 21600000);
    }
  }, [user]);

  useEffect(() => {
    if (getUpdateData) {
      console.log("UPDATESSS", getUpdateData);
      const accessToken = localStorage.getItem("accessToken");
        setTimeout(() => {
            dispatch(wsUpdate({ action: constants.ACTION_GET_CLEANED_GATES_DATA, access_token: accessToken }));
          }, 1000);
    }
  }, [getUpdateData]);

  // useEffect(() => {
  //   // console.log("ATCO SECOND")
  //   if (wsOpened) {
  //     console.log("WS is opened! Fetch GET_CLEANED_GATES_DATA");
  //     const accessToken = localStorage.getItem("accessToken");
  //     dispatch(wsUpdate({ action: constants.ACTION_GET_CLEANED_GATES_DATA, access_token: accessToken }));

  //   }
  // }, []);

  useEffect(() => {
    // console.log("ATCO SECOND")
    if (wsOpened) {
      console.log("WS is opened!");
      const accessToken = localStorage.getItem("accessToken");
      dispatch(wsSend({ action: constants.ACTION_GET_GATES_DATA, access_token: accessToken }));
      dispatch(wsUpdate({ action: constants.ACTION_GET_CLEANED_GATES_DATA, access_token: accessToken }));

      setInterval(() => {
        console.log("Heartbeat: fetch gates data");
        dispatch(wsSend({ action: constants.ACTION_GET_GATES_DATA, access_token: accessToken }));
        dispatch(wsUpdate({ action: constants.ACTION_GET_CLEANED_GATES_DATA, access_token: accessToken }));
        setErrorDisplay(true);
      }, constants.HEARTBEAT_INTERVAL);

      // setInterval(() => {
      //   console.log("Triggering Re fetch");
      //   setRefetch(Math.random());
      // }, 60000);

    }
  }, [wsOpened]);


  // useEffect(() => {
  //   if (wsOpened && gatesData) {
  //     console.log("WS is opened! 22");
  //     const accessToken = localStorage.getItem("accessToken");
  //     dispatch(wsUpdate({ action: constants.ACTION_GET_CLEANED_GATES_DATA, access_token: accessToken }));

  //     // setInterval(() => {
  //     //   console.log("Heartbeat: fetch clean gates data");
  //     //   dispatch(wsUpdate({ action: constants.ACTION_GET_CLEANED_GATES_DATA, access_token: accessToken }));
  //     //   setRefetch(Math.random());
  //     // }, 60000);

  //   }
  // }, [cleanedGatesData]);

  // check internet connection
  useEffect(() => {
    setInterval(() => {
      var condition = navigator.onLine ? 'online' : 'offline';
      // console.log('NAVIGATOR ONLINE', navigator.onLine);
      if (condition === 'online') {
        setCheckConnection(false);
      } else {
        // console.log('OFFLINE');
        setCheckConnection(true);
      }
    }, 60000);
  }, []);

  // check user token validity 
  useEffect(() => {
    // console.log("TTime SESSION New");
    checkTimeSessionToken();
    setInterval(() => {
      checkTimeSessionToken();
      reloadPage();
    }, 2700000); //2700000 this will check every 45mins

  }, []);


  //set cookie on mount
  useEffect(() => {
    checkCookies();
  }, []);


  // getdata on initial load
  useEffect(() => {
    if (gatesData) {
      let pbdata = gatesData.map((e) => {
        let pbrow = {}

        //getting gates
        if (e.current_gate) {
          pbrow.current_gate = e.current_gate
        }

        //getting terminal 
        let terminal = gateMapping.data.filter((item) => {
          return item.gate == pbrow.current_gate
        });

        //map assign call sign
        terminal.map(res => pbrow.terminal = res.terminal);

        // call sign filter
        let call_sign_code = gateMapping.data.filter((item) => {
          return item.gate == pbrow.current_gate
        });

        //map assign call sign
        call_sign_code.map(res => pbrow.assigned_call_sign = res.team_call_sign);

        if (e.flightno === undefined) {
          pbrow.flightno = "";
        } else {
          pbrow.flightno = e.flightno
        }


        // getting flight id
        if (e.flight_id === undefined) {
          pbrow.flight_id = "";
        } else {
          pbrow.flight_id = e.flight_id
        }
        // if (e.flight_id) {
        //   pbrow.flight_id = e.flight_id
        // }

        // aircraft_type size
        if (e.aircraft_type) {
          pbrow.aircraft_type = e.aircraft_type
        }

        //check aircraft type in the aircraftSizeMapping
        let aircraft_size = aircraftSizeMapping.data.filter((item) => {
          return item.aircraft_type == pbrow.aircraft_type
        })

        //map thru the aircraft_size
        aircraft_size.map(res => pbrow.aircraft_size = res.size);

        // departure/arrival
        if (e.direction) {
          pbrow.direction = e.direction
        }

        // next flight id
        if (e.next_flight_id) {
          pbrow.next_flight_id = e.next_flight_id
        }

        if (e.arr_dep_flag) {
          pbrow.arr_dep_flag = e.arr_dep_flag;
        }

        //----------- computing Free from ----------------
        // need to transform value from to json into date 

        //set time if cleaned:
        const calFreeFrom = () => {
          let TODAY = moment().startOf('day');
          var TOMORROW = TODAY.clone().add(1, 'days').startOf('day');
          let format = "DD/MM/YYYY";

          if (e.latest_cleaning_status == "AVAILABLE" || e.latest_cleaning_status == "CLEANING") {
            return pbrow.free_from = "Now"
          } else if (e.latest_cleaning_status == "CLEANED" || e.gate_free_from == "") {
            return pbrow.free_from = ""
          } else {
            if (moment(e.gate_free_from).isSame(TOMORROW, 'day')) {
              let tomorrowDate = moment(e.gate_free_from).format(format);
              pbrow.free_from_date = tomorrowDate;
              // console.log('checker free from', e.gate_free_from);
            }
            let time = e.gate_free_from.slice(11, 16);
            return pbrow.free_from = time;
          }

        }
        calFreeFrom()
        setInterval(calFreeFrom, 60000)

        //------------------------------------------------

        //----------- computing Free till ----------------
        //need to transform value from to json into date 

        const calFreeTill = () => {
          let TODAY = moment().startOf('day');
          var TOMORROW = TODAY.clone().add(1, 'days').startOf('day');
          let format = "DD/MM";

          if (e.latest_cleaning_status == "CLEANED") {
            return pbrow.free_till = ""
          } else if (e.gate_free_till == "TBA") {
            return pbrow.free_till = e.gate_free_till;
          } else if (e.gate_free_till == "" || e.gate_free_till === undefined) {
            return pbrow.free_till = "TBA";
          } else {
            //check if date is for tomorrow
            if (moment(e.gate_free_till).isSame(TOMORROW, 'day')) {
              let tomorrowDate = moment(e.gate_free_till).format(format);
              pbrow.free_till_date = tomorrowDate;
            }

            let time = e.gate_free_till.slice(11, 16);
            return pbrow.free_till = time;
          }

        }
        calFreeTill()
        setInterval(calFreeTill, 60000)

        //------------------------------------------------


        // time left
        // need to compute this
        // need to transform value from to json into date then subtract them

        //----------- computing time left ----------------
        //if status is cleaned
        const calTimeLeft = () => {
          if (e.latest_cleaning_status == "CLEANED" || e.gate_free_till == "" || e.gate_free_till === undefined || e.latest_cleaning_status == "NOT AVAILABLE" || e.gate_free_till == "TBA") {
            pbrow.time_remaining = ""; //for sorting
            return pbrow.time_left = ""
          }
          else {
            let now = moment().format('HH:mm')
            let time = e.gate_free_till.slice(11, 16);
            let remaining_time = moment(time, 'HH:mm').subtract(now, "HH:mm").format('HH:mm');

            if (remaining_time.toString().split(":")[0] == "00") {
              if (remaining_time.toString().split(":")[1].charAt(0) == "0") {
                pbrow.time_remaining = remaining_time; //for sorting
                return pbrow.time_left = remaining_time.toString().split(":")[1].slice(1) + "min"
              }
              else {
                pbrow.time_remaining = remaining_time; //for sorting
                return pbrow.time_left = remaining_time.toString().split(":")[1] + "min"
              }

            }
            else if (remaining_time.toString().split(":")[0].charAt(0) == "0") {
              pbrow.time_remaining = remaining_time; //for sorting
              return pbrow.time_left = remaining_time.toString().split(":")[0].slice(1) + "hr " + remaining_time.toString().split(":")[1] + "min"
            }
            else {
              pbrow.time_remaining = remaining_time; //for sorting
              return pbrow.time_left = remaining_time.toString().split(":")[0] + "hr " + remaining_time.toString().split(":")[1] + "min"
            }

          }
        }
        calTimeLeft()
        setInterval(calTimeLeft, 60000)


        //------------------------------------------------

        // status
        if (e.latest_cleaning_status) {
          pbrow.latest_cleaning_status = e.latest_cleaning_status
          // if (e.latest_cleaning_status === 'CLEANED') {
          //   pbrow.aircraft_type = '';
          //   pbrow.flightno = '';
          //   pbrow.direction = '';
          //   pbrow.aircraft_size = '';
          // }
        }

        if (e.availability_status) {
          pbrow.availability_status = e.availability_status
        }

        // last cleaned time
        if (e.last_cleaned_time) {
          let format = "DD/MM/YYYY HH:mm"
          let cleaned_date = new Date(e.last_cleaned_time);
          let cleaned_time = moment(cleaned_date).format(format);
          pbrow.last_cleaned_time = cleaned_time
          pbrow.last_cleaned_time_day_duration = moment(cleaned_time, "DD/MM/YYYY HH:mm").add(1, 'days').format(format);
          pbrow.last_cleaned_time_day_duration_unix = moment(cleaned_time, "DD/MM/YYYY HH:mm").add(1, 'days').unix();
        }



        // available next flight
        if (e.available_next_flight) {
          pbrow.available_next_flight = e.available_next_flight
        }

        // available shared gate
        if (e.available_shared_gate) {
          pbrow.available_shared_gate = e.available_shared_gate
        }

        //priority
        if (e.priority) {
          pbrow.priority = e.priority;
        }

        // shared gate flight id
        if (e.shared_gate_flight_id === "" || e.shared_gate_flight_id === undefined) {
          pbrow.shared_gate_flight_id = "";
        } else {
          pbrow.shared_gate_flight_id = e.shared_gate_flight_id;
          let arraySharedFlightId = e.shared_gate_flight_id.split("-");
          pbrow.shared_gate_flight_no = arraySharedFlightId[1];
        }

        // unavailable_shared_gate
        if (e.unavailable_shared_gate === "" || e.unavailable_shared_gate === undefined) {
          pbrow.unavailable_shared_gate = "";
        } else {
          pbrow.unavailable_shared_gate = e.unavailable_shared_gate;
        }


        //search item
        pbrow.searchstr = (pbrow.current_gate + pbrow.flightno).toLowerCase();

        return pbrow;
      })

      // console.log('PBDATA', pbdata);

      // filter gates data by AVAILABLE, UNAVAILABLE gates
      let filterGatesData = pbdata.filter(item => {
        return item.latest_cleaning_status !== "CLEANED"
      });
      // console.log("GG gates other data", filterGatesData);

      // // filter gates data by CLEANED gates
      // let filterCompleteData = pbdata.filter(item => {
      //   return item.latest_cleaning_status === "CLEANED"
      // });

      // // console.log("GG complete today data", filterCompleteData);

      // // get the complete data in local storage
      // let getAllCleanedGates = localStorage.getItem('allCleanedGates');
      // getAllCleanedGates = getAllCleanedGates ? JSON.parse(getAllCleanedGates) : [];

      // // flights with next flight id
      // let getNewlyCleanedGates = localStorage.getItem('newlyCleanedGates');
      // getNewlyCleanedGates = getNewlyCleanedGates ? JSON.parse(getNewlyCleanedGates) : [];
      // // console.log("ALLCLEANED GATES", getNewlyCleanedGates);

      // let allCleanedGates = [...filterCompleteData, ...getAllCleanedGates, ...getNewlyCleanedGates];
      // // console.log("GG CONCAT1", allCleanedGates);

      // let TODAY = moment().startOf('day');
      // let YESTERDAY = TODAY.clone().subtract(1, 'days').startOf('day');

      // // // get the complete data in local storage
      // // let getAllCleanedGates = localStorage.getItem('allCleanedGates2');
      // // getAllCleanedGates = getAllCleanedGates ? JSON.parse(getAllCleanedGates) : [];
      // // console.log("ALLCLEANED GATES", getAllCleanedGates);

      // let filterTodayComplete = allCleanedGates.filter(item => {
      //   return (moment(item.last_cleaned_time, "DD/MM/YYYY HH:mm").isSameOrAfter(TODAY))
      // });

      // //store it in Today complete in local storage
      // if (filterTodayComplete) {
      //   localStorage.setItem("todayCleaned", JSON.stringify(filterTodayComplete));
      //   // console.log("filterTodayComplete: ", filterTodayComplete);
      // }


      // // compare and concat complete data from api to local
      // let filterYesterdayComplete = allCleanedGates.filter(item => {
      //   if ((moment(item.last_cleaned_time, "DD/MM/YYYY").isSame(YESTERDAY))) {
      //     // console.log("itemsss", item.last_cleaned_time);
      //     return item.last_cleaned_time
      //   }
      // });

      // if (filterYesterdayComplete) {
      //   //store it in local storage
      //   localStorage.setItem("previousDayCleaned", JSON.stringify(filterYesterdayComplete));
      //   // console.log("filterYesterdayComplete: ", filterYesterdayComplete);
      // }

      // // let cleanedFilterArray = getCompleteDataLocal.concat(getCleanedLocal);
      // let cleanedFilterArray = [...filterTodayComplete, ...filterYesterdayComplete];
      // // console.log("GG CONCAT2", cleanedFilterArray);

      // // remove same value in array for local storage previousDayCleaned and todayCleaned
      // // let newCleanedFilterArray = cleanedFilterArray.filter((v, i, a) => a.findIndex(t => (t.current_gate === v.current_gate)) === i);
      // let newCleanedFilterArray = cleanedFilterArray.reduce((acc, current) => {
      //   const x = acc.find(item => item.current_gate === current.current_gate);
      //   if (!x) {
      //     return acc.concat([current]);
      //   } else {
      //     return acc;
      //   }
      // }, []);
      // // console.log("GG Cleaned from api and local unique values", newCleanedFilterArray);
      // localStorage.setItem("allCleanedGates", JSON.stringify(newCleanedFilterArray));

      // let joinGates = [...filterGatesData, ...newCleanedFilterArray];
      // console.log('PBDATA AFTER FILTER', joinGates);

      // setOriginalPushbackData(pbdata);
      setGatesAllData(filterGatesData);
      // setOriginalPushbackData(joinGates);
      // const getToken = window.localStorage.getItem("accessToken");
      // dispatch(wsUpdate({ action: constants.ACTION_GET_CLEANED_GATES_DATA, access_token: getToken }));
    }
  }, [gatesData]);

  
  // get cleaned gates data
   useEffect(() => {
    if (cleanedGatesData) {
      let pbdata = cleanedGatesData.map((e) => {
        let pbrow = {}

        //getting gates
        if (e.current_gate) {
          pbrow.current_gate = e.current_gate
        }

        if (e.flight_id) {
          pbrow.flight_id = e.flight_id
        }

        if (e.flightno) {
          pbrow.flightno = e.flightno
        }
        // if (e.flightno) {
        //   pbrow.flightno = e.flightno
        // }

        pbrow.flightno = "";
        //getting terminal 
        let terminal = gateMapping.data.filter((item) => {
          return item.gate == pbrow.current_gate
        });

        //map assign call sign
        terminal.map(res => pbrow.terminal = res.terminal);

        // call sign filter
        let call_sign_code = gateMapping.data.filter((item) => {
          return item.gate == pbrow.current_gate
        });

        //map assign call sign
        call_sign_code.map(res => pbrow.assigned_call_sign = res.team_call_sign);

        // status
        if (e.latest_cleaning_status) {
          pbrow.latest_cleaning_status = e.latest_cleaning_status
        }

        if (e.availability_status) {
          pbrow.availability_status = e.availability_status
        }

        // last cleaned time
        if (e.last_cleaned_time) {
          let format = "DD/MM/YYYY HH:mm"
          let cleaned_date = new Date(e.last_cleaned_time);
          let cleaned_time = moment(cleaned_date).format(format);
          pbrow.last_cleaned_time = cleaned_time;
        }

        // available next flight
        if (e.available_next_flight) {
          pbrow.available_next_flight = e.available_next_flight
        }

        // available shared gate
        if (e.available_shared_gate) {
          pbrow.available_shared_gate = e.available_shared_gate
        }

        //priority
        if (e.priority) {
          pbrow.priority = e.priority;
        }

        // shared gate flight id
        if (e.shared_gate_flight_id === "" || e.shared_gate_flight_id === undefined) {
          pbrow.shared_gate_flight_id = "";
        } else {
          pbrow.shared_gate_flight_id = e.shared_gate_flight_id;
          let arraySharedFlightId = e.shared_gate_flight_id.split("-");
          pbrow.shared_gate_flight_no = arraySharedFlightId[1];
        }

        // unavailable_shared_gate
        if (e.unavailable_shared_gate === "" || e.unavailable_shared_gate === undefined) {
          pbrow.unavailable_shared_gate = "";
        } else {
          pbrow.unavailable_shared_gate = e.unavailable_shared_gate;
        }


        //search item
        pbrow.searchstr = (pbrow.current_gate + pbrow.flightno).toLowerCase();

        return pbrow;

      })

      setCleanedAllGatesData(pbdata);
    }
  }, [cleanedGatesData]);


  // combine gatesdata and cleaned gates data
  useEffect(() => {
    if (gatesAllData && cleanedAllGatesData) {
      // const isgatesArray = Array.isArray(gatesAllData);
      // console.log("isgatesArray", gatesAllData);
      let allGates = [...gatesAllData, ...cleanedAllGatesData];

       // // remove same value in array for local storage previousDayCleaned and todayCleaned
      // let newCleanedFilterArray = cleanedFilterArray.filter((v, i, a) => a.findIndex(t => (t.current_gate === v.current_gate)) === i);
      let filterGatesArray = allGates.reduce((acc, current) => {
        const x = acc.find(item => item.flight_id === current.flight_id);
        if (!x) {
          return acc.concat([current]);
        } else {
          return acc;
        }
      }, []);
      console.log("Cleaned filter gates with gates", filterGatesArray);
      setOriginalPushbackData(filterGatesArray);

    }
  }, [gatesAllData, cleanedGatesData]);


  useEffect(() => {
    if (wsOpened && gatesData && selectedStatus === "Cleaned") {
      const accessToken = localStorage.getItem("accessToken");
      dispatch(wsUpdate({ action: constants.ACTION_GET_CLEANED_GATES_DATA, access_token: accessToken }));
    }
  }, [wsOpened, selectedStatus]);

  // get data by filter 
  useEffect(() => {
    if (originalPushbackData.length > 0) {
      filterData();
    }
  }, [
    originalPushbackData,
    selectedCallSign,
    selectedAllCallSign,
    selectedStatus,
    searchValue,
    selectedAllTerminal,
    selectedSort,
    statusModal,
  ]);

  

  //all filter data
  const filterData = () => {
    let newPushbackData = originalPushbackData;
    let userTerminal = user ? user.idToken.payload['custom:visible_terminal'] : userLocal.idToken.payload['custom:visible_terminal'];

    // for terminal 1-4 (the locked Terminal)
    if (userTerminal === "T1" || userTerminal === "T2" || userTerminal === "T3" || userTerminal === "T4") {
      setSelectedAllTerminal("");
      setSelectedAllCallSign("");

      //terminal values filter disabled
      if (selectedTerminal === "T1") {
        let termValue = selectedTerminal.slice(1);
        let filteredGatesData = originalPushbackData.filter(item => {
          return item.terminal.includes(termValue);
        })
        newPushbackData = filteredGatesData;
      } else if (selectedTerminal === "T2") {
        let termValue = selectedTerminal.slice(1);
        let filteredGatesData = originalPushbackData.filter(item => {
          return item.terminal.includes(termValue);
        })
        newPushbackData = filteredGatesData;
      } else if (selectedTerminal === "T3") {
        let termValue = selectedTerminal.slice(1);
        let filteredGatesData = originalPushbackData.filter(item => {
          return item.terminal.includes(termValue);
        })
        newPushbackData = filteredGatesData;
      } else if (selectedTerminal === "T4") {
        let termValue = selectedTerminal.slice(1);
        let filteredGatesData = originalPushbackData.filter(item => {
          return item.terminal.includes(termValue);
        })
        newPushbackData = filteredGatesData;
      }

      //filter by callsign
      if (selectedCallSign) {
        let filteredGatesData = originalPushbackData.filter(item => {
          return item.assigned_call_sign.includes(selectedCallSign);
        })
        newPushbackData = filteredGatesData;
      }


      //check if selected callsign is equal to ALL in selectedTerminal
      if (selectedTerminal === "T1" && selectedCallSign === "ALL") {
        let termValue = selectedTerminal.slice(1);
        let filteredGatesData = originalPushbackData.filter(item => {
          return item.terminal.includes(termValue);
        })
        newPushbackData = filteredGatesData;
      } else if (selectedTerminal === "T2" && selectedCallSign === "ALL") {
        let termValue = selectedTerminal.slice(1);
        let filteredGatesData = originalPushbackData.filter(item => {
          return item.terminal.includes(termValue);
        })
        newPushbackData = filteredGatesData;
      } else if (selectedTerminal === "T3" && selectedCallSign === "ALL") {
        let termValue = selectedTerminal.slice(1);
        let filteredGatesData = originalPushbackData.filter(item => {
          return item.terminal.includes(termValue);
        })
        newPushbackData = filteredGatesData;
      } else if (selectedTerminal === "T4" && selectedCallSign === "ALL") {
        let termValue = selectedTerminal.slice(1);
        let filteredGatesData = originalPushbackData.filter(item => {
          return item.terminal.includes(termValue);
        })
        newPushbackData = filteredGatesData;
      }

      // console.log("originalPushbackData", originalPushbackData);
    }



    // for terminal for all
    if (userTerminal === "ALL") {
      setSelectedTerminal("");
      setSelectedCallSign("");

      //terminal values filter for all
      if (selectedAllTerminal === "T1") {
        let termValue = selectedAllTerminal.slice(1);
        let filteredGatesData = originalPushbackData.filter(item => {
          return item.terminal.includes(termValue);
        })
        newPushbackData = filteredGatesData;
      } else if (selectedAllTerminal === "T2") {
        let termValue = selectedAllTerminal.slice(1);
        let filteredGatesData = originalPushbackData.filter(item => {
          return item.terminal.includes(termValue);
        })
        newPushbackData = filteredGatesData;
      } else if (selectedAllTerminal === "T3") {
        let termValue = selectedAllTerminal.slice(1);
        let filteredGatesData = originalPushbackData.filter(item => {
          return item.terminal.includes(termValue);
        })
        newPushbackData = filteredGatesData;
      } else if (selectedAllTerminal === "T4") {
        let termValue = selectedAllTerminal.slice(1);
        let filteredGatesData = originalPushbackData.filter(item => {
          return item.terminal.includes(termValue);
        })
        newPushbackData = filteredGatesData;
      }

      //filter by callsign all terminal
      if (selectedAllCallSign) {
        let filteredGatesData = originalPushbackData.filter(item => {
          return item.assigned_call_sign.includes(selectedAllCallSign);
        })
        // console.log('Filter all callsign', selectedAllCallSign);
        newPushbackData = filteredGatesData;
      }

      //selected all terminal
      if ((selectedAllTerminal === "T1" && selectedAllCallSign === "ALL") || (selectedAllTerminal === "T1" && selectedAllCallSign === "")) {
        let termValue = selectedAllTerminal.slice(1);
        let filteredGatesData = originalPushbackData.filter(item => {
          return item.terminal.includes(termValue);
        })
        newPushbackData = filteredGatesData;
      } else if ((selectedAllTerminal === "T2" && selectedAllCallSign === "ALL") || (selectedAllTerminal === "T2" && selectedAllCallSign === "")) {
        let termValue = selectedAllTerminal.slice(1);
        let filteredGatesData = originalPushbackData.filter(item => {
          return item.terminal.includes(termValue);
        })
        newPushbackData = filteredGatesData;
      } else if ((selectedAllTerminal === "T3" && selectedAllCallSign === "ALL") || (selectedAllTerminal === "T3" && selectedAllCallSign === "")) {
        let termValue = selectedAllTerminal.slice(1);
        let filteredGatesData = originalPushbackData.filter(item => {
          return item.terminal.includes(termValue);
        })
        newPushbackData = filteredGatesData;
      } else if ((selectedAllTerminal === "T4" && selectedAllCallSign === "ALL") || (selectedAllTerminal === "T4" && selectedAllCallSign === "")) {
        let termValue = selectedAllTerminal.slice(1);
        let filteredGatesData = originalPushbackData.filter(item => {
          return item.terminal.includes(termValue);
        })
        newPushbackData = filteredGatesData;
      } else if ((selectedAllTerminal === "ALL" && selectedAllCallSign === "ALL") || (selectedAllTerminal === "ALL" && selectedAllCallSign === "")) {
        newPushbackData = originalPushbackData;
      }


    }


    //check if there is no data in newpushbackData and show the originalPushbackData
    if (newPushbackData.length === 0 && !selectedCallSign && !selectedAllCallSign &&
      !selectedStatus) {
      newPushbackData = originalPushbackData;
    }


    //filter by status, please refer to the common values for the variables: allStatus, availableStatus and toBeCleanedStatus
    if (selectedStatus === "All") {
      let filteredGatesData = newPushbackData.filter(item => {
        return allStatus.includes(item.latest_cleaning_status);
      })
      newPushbackData = filteredGatesData;
    } else if (selectedStatus === "Available") {
      let filteredGatesData = newPushbackData.filter(item => {
        return availableStatus.includes(item.latest_cleaning_status);
      })
      newPushbackData = filteredGatesData;
    } else if (selectedStatus === "ToBeCleaned") {
      let filteredGatesData = newPushbackData.filter(item => {
        return toBeCleanedStatus.includes(item.latest_cleaning_status);
      })
      newPushbackData = filteredGatesData;
    } else if (selectedStatus === "Cleaned") {
      // const accessToken = localStorage.getItem("accessToken");
      // dispatch(wsUpdate({ action: constants.ACTION_GET_CLEANED_GATES_DATA, access_token: accessToken }));
      // submitCleanedGate();
      // let filteredGatesData = cleanedAllGatesData;
      let filteredGatesData = newPushbackData.filter(item => {
        return cleanedStatus.includes(item.latest_cleaning_status);
      })
      
      newPushbackData = filteredGatesData;
    }


    //search value
    if (searchValue) {
      newPushbackData = newPushbackData.filter((item) =>
        item.searchstr.includes(searchValue.toLowerCase())
      );
    }

    //chrome 
    

    // sort by date last cleaned time
    newPushbackData.sort((a, b) => {
      let time_conversion_a = moment(a.last_cleaned_time, 'DD/MM/YYYY HH:mm:ss');
      let time_conversion_b = moment(b.last_cleaned_time, 'DD/MM/YYYY HH:mm:ss');

      let primary = 0;

      //check first the browser type
      if (navigator.userAgent.indexOf("Firefox") !== -1){

        // console.log("web browser firefox");

        if (a.last_cleaned_time === null || a.last_cleaned_time === '' || a.last_cleaned_time === undefined) {
          return 1;
        }
        else if (b.last_cleaned_time === null || b.last_cleaned_time === '' || b.last_cleaned_time === undefined) {
          return -1;
        }
        else if (time_conversion_a.isBefore(time_conversion_b)) {
          primary = -1;
        }
        else if (time_conversion_a.isSameOrAfter(time_conversion_b)) {
          primary = 1
        }
        if (primary !== 0) {
          return primary;
        } 

      } else {
        // console.log("web browser chrome")

        if (a.last_cleaned_time === null || a.last_cleaned_time === '' || a.last_cleaned_time === undefined) {
          return 1;
        }
        else if (b.last_cleaned_time === null || b.last_cleaned_time === '' || b.last_cleaned_time === undefined) {
          return -1;
        }
        else if (time_conversion_a.isBefore(time_conversion_b)) {
        // else if (time_conversion_a > time_conversion_b) {
          primary = 1;
        }
        else if (time_conversion_a.isSameOrAfter(time_conversion_b)) {
        // else if (time_conversion_a < time_conversion_b) {
          primary = -1
        }
        if (primary !== 0) {
          return primary;
        } 

      }

      
      // return a.current_gate > b.current_gate ? 1 : -1

      // return time_conversion_a > time_conversion_b ? -1 : 1

      // return (time_conversion_a.isBefore(time_conversion_b)) ? 1 : -1


    });


    //sort
    if (selectedSort === "Gate") {
      // newPushbackData.sort((a, b) => a.current_gate > b.current_gate ? 1 : -1);
      newPushbackData.sort((a, b) => naturalCompare(a.current_gate, b.current_gate));
    } else if (selectedSort === "Flight") {
      let size = {
        S: 0,
        M: 1,
        L: 2,
        XL: 3
      }
      //doesnt have a secodary sort
      newPushbackData.sort((a, b) => {
        var primary = 0;
        if (a.flightno === null || a.flightno === '' || a.flightno === undefined) {
          return 1;
        }
        else if (b.flightno === null || b.flightno === '' || b.flightno === undefined) {
          return -1;
        }
        else if (a.flightno > b.flightno) {
          primary = 1;
        }
        else if (a.flightno < b.flightno) {
          primary = -1
        }
        if (primary !== 0) {
          return primary;
        }

        return size[a.aircraft_size] - size[b.aircraft_size] ? -1 : 1
      });
    } else if (selectedSort === "Direction") {
      let status = {
        'CLEANING': 0,
        'AVAILABLE': 1,
        'NOT AVAILABLE': 2,
        'CLEANED': 3
      }
      newPushbackData.sort((a, b) => {
        var primary = 0;
        if (a.direction === null || a.direction === '' || a.direction === undefined) {
          return 1;
        }
        else if (b.direction === null || b.direction === '' || b.direction === undefined) {
          return -1;
        }
        else if (a.direction < b.direction) {
          primary = 1;
        }
        else if (a.direction > b.direction) {
          primary = -1
        }
        if (primary !== 0) {
          return primary;
        }

        return status[a.latest_cleaning_status] > status[b.latest_cleaning_status] ? 1 : -1
      });
    } else if (selectedSort === "FreeFrom") {
      let status = {
        'CLEANING': 0,
        'AVAILABLE': 1,
        'NOT AVAILABLE': 2,
        'CLEANED': 3
      }
      let now = moment().format('HH:mm');
      // .subtract(now, "HH:mm")
      newPushbackData.sort((a, b) => {
        let time_conversion_a = moment(a.free_from, 'HH:mm');
        let time_conversion_b = moment(b.free_from, 'HH:mm');
        let date_a = moment(a.free_from_date, 'YYYY-MM-DD');
        let date_b = moment(b.free_from_date, 'YYYY-MM-DD');

        if (a.free_from === "Now") {
          return -1;
        }

        let primary = 0;
        if (a.free_from === null || a.free_from === '' || a.free_from === undefined) {
          return 1;
        }
        else if (b.free_from === null || b.free_from === '' || b.free_from === undefined) {
          return -1;
        }

        else if (time_conversion_a.subtract(now, "HH:mm").isBefore(time_conversion_b.subtract(now, "HH:mm"))) {
          primary = -1;
          if ((a.free_from_date && !b.free_from_date) || (a.free_from_date && b.free_from_date && date_a.isAfter(date_b))) {
            primary = 1
          }
        }
        else if (time_conversion_a.subtract(now, "HH:mm").isSameOrAfter(time_conversion_b.subtract(now, "HH:mm"))) {
          primary = 1
          if ((b.free_from_date && !a.free_from_date) || (a.free_from_date && b.free_from_date && date_b.isAfter(date_a))) {
            primary = -1
          }
        }

        if (primary !== 0) {
          return primary;
        }

        return (status[a.latest_cleaning_status] > status[b.latest_cleaning_status]) ? 1 : -1
      });

    } else if (selectedSort === "FreeTill") {
      let status = {
        'CLEANING': 0,
        'AVAILABLE': 1,
        'NOT AVAILABLE': 2,
        'CLEANED': 3
      }
      let now = moment().format('HH:mm');

      //doesnt have a secondary sort
      newPushbackData.sort((a, b) => {
        let time_conversion_a = moment(a.free_till, 'HH:mm');
        let time_conversion_b = moment(b.free_till, 'HH:mm');
        let date_a = moment(a.free_till_date, 'YYYY-MM-DD');
        let date_b = moment(b.free_till_date, 'YYYY-MM-DD');

        let primary = 0;

        if (a.free_till === null || a.free_till === '' || a.free_till === undefined) {
          return 1;
        }
        else if (b.free_till === null || b.free_till === '' || b.free_till === undefined) {
          return -1;
        }
        else if (a.free_till === "TBA") {
          return 1;
        }
        else if (b.free_till === "TBA") {
          return -1;
        }
        else if (time_conversion_a.subtract(now, "HH:mm").isBefore(time_conversion_b.subtract(now, "HH:mm"))) {
          primary = -1;
          if ((a.free_till_date && !b.free_till_date) || (a.free_till_date && b.free_till_date && date_a.isAfter(date_b))) {
            primary = 1
          }
        }
        else if (time_conversion_a.subtract(now, "HH:mm").isSameOrAfter(time_conversion_b.subtract(now, "HH:mm"))) {
          primary = 1
          if ((b.free_till_date && !a.free_till_date) || (a.free_till_date && b.free_till_date && date_b.isAfter(date_a))) {
            primary = -1
          }
        }
        if (primary !== 0) {
          return primary;
        }

        return (status[a.latest_cleaning_status] > status[b.latest_cleaning_status]) ? 1 : -1

      });

    } else if (selectedSort === "TimeLeft") {
      let status = {
        'CLEANING': 0,
        'AVAILABLE': 1,
        'NOT AVAILABLE': 2,
        'CLEANED': 3
      }

      let now = moment().format('HH:mm');

      newPushbackData.sort((a, b) => {
        let time_conversion_a = moment(a.time_remaining, 'HH:mm');
        let time_conversion_b = moment(b.time_remaining, 'HH:mm');

        let primary = 0;
        // let time = 0;
        if (a.time_remaining === null || a.time_remaining === "" || a.time_remaining === undefined) {
          return 1;
          // time = 1;
        }
        else if (b.time_remaining === null || b.time_remaining === "" || b.time_remaining === undefined) {
          return -1;
          // time = -1;
        }

        else if (time_conversion_a.subtract(now, "HH:mm").isBefore(time_conversion_b.subtract(now, "HH:mm"))) {
          primary = -1;
        }
        else if (time_conversion_a.subtract(now, "HH:mm").isSameOrAfter(time_conversion_b.subtract(now, "HH:mm"))) {
          primary = 1
        }
        if (primary !== 0) {
          return primary;
        }

        return (status[a.latest_cleaning_status] > status[b.latest_cleaning_status]) ? 1 : -1
        // return a.current_gate > b.current_gate ? 1 : -1
      });

    } else if (selectedSort === "Status") {
      let status = {
        'CLEANING': 0,
        'AVAILABLE': 1,
        'NOT AVAILABLE': 2,
        'CLEANED': 3
      }
      newPushbackData.sort((a, b) => {
        let primary = 0;
        // Make all NOW at the front

        if (a.latest_cleaning_status === "") {
          return -1;
        }
        if (a.latest_cleaning_status === null || a.latest_cleaning_status === undefined) {
          return 1;
        }
        else if (b.latest_cleaning_status === null || b.latest_cleaning_status === undefined) {
          return -1;
        }
        else if (status[a.latest_cleaning_status] > status[b.latest_cleaning_status]) {
          primary = 1;
        }
        else if (status[a.latest_cleaning_status] < status[b.latest_cleaning_status]) {
          primary = -1;
        }
        else if (a.priority === null || a.priority === undefined) {
          primary = 1;
        }
        else if (b.priority === null || b.priority === undefined) {
          primary = -1;
        }
        if (primary !== 0) {
          return primary;
        }
        return a.priority > b.priority ? -1 : 1

      });

    }

    // console.log("newPushbackData", newPushbackData);
    let fmUsername = user ? user.idToken.payload['cognito:username'] : userLocal.idToken.payload['cognito:username'];
    setFilterCookie('filterCookie', { user: fmUsername, terminal: selectedTerminal, terminalAll: selectedAllTerminal, callSign: selectedCallSign, callSignAll: selectedAllCallSign, status: selectedStatus }, { maxAge: 31536000 });
    setPushbackData(newPushbackData);
  }

  const submitCleanedGate = () => {
    const accessToken = localStorage.getItem("accessToken");
    setTimeout(() => {
        dispatch(wsUpdate({ action: constants.ACTION_GET_CLEANED_GATES_DATA, access_token: accessToken }));
      }, 1000);
   
}

  const checkCookies = () => {
    let fmUsername = user ? user.idToken.payload['cognito:username'] : userLocal.idToken.payload['cognito:username'];
    //checking cookies
    if (filterCookies.filterCookie) {
      if (filterCookies.filterCookie.user === fmUsername) {
        if (filterCookies.filterCookie.callSign) {
          setSelectedCallSign(filterCookies.filterCookie.callSign);
        }
        if (filterCookies.filterCookie.callSignAll) {
          setSelectedAllCallSign(filterCookies.filterCookie.callSignAll);
        }
        if (filterCookies.filterCookie.status) {
          setSelectedStatus(filterCookies.filterCookie.status);
        }
        if (filterCookies.filterCookie.terminalAll) {
          setSelectedAllTerminal(filterCookies.filterCookie.terminalAll);
        }
      }
      // else {
      //   console.log("DIFFERENT USER");
      //   console.log("FILTER COOKIE:", filterCookies.filterCookie);
      // }

    } else {
      setFilterCookie('filterCookie', { user: fmUsername, terminal: selectedTerminal, terminalAll: selectedAllTerminal, callSign: selectedCallSign, callSignAll: selectedAllCallSign, status: selectedStatus }, { maxAge: 31536000 });
    }
  }

  const checkUserTokenValidity = () => {
    // call refresh token
    let poolData = {
      UserPoolId: constants.USERPOOLID,
      ClientId: constants.CLIENTID,
    };
    let userPool = new CognitoUserPool(poolData);
    const cognitoUser = userPool.getCurrentUser();
    cognitoUser.getSession(function (err, data) {
      if (err) {
        // Prompt the user to reauthenticate by hand...
        props.history.push('/');
      } else {
        const cognitoUserSession = data;
        const yourAccessToken = cognitoUserSession.getAccessToken().jwtToken;
        let refresh_token = cognitoUserSession.getRefreshToken();

        // setup AWS config
        AWS.config.credentials = new AWS.CognitoIdentityCredentials({
          IdentityPoolId: '',
          Logins: {
            [constants.LOGINS]: yourAccessToken
          },
        },
          {
            region: 'ap-southeast-1'
          },
        );
        if (AWS.config.credentials.needsRefresh()) {
          cognitoUser.refreshSession(refresh_token, (refErr, cognitoUserSession) => {
            //this provide new accessToken, IdToken, refreshToken
            // you can add you code here once you get new accessToken, IdToken, refreshToken
            if (refErr) {
              console.log("TTime error", refErr);
            }
            else {
              AWS.config.credentials.params.Logins[constants.LOGINS] = cognitoUserSession.getAccessToken().jwtToken;

              //get existing local storage
              let existingLocalStorage = window.localStorage.getItem('userToken');
              let accessTokenLocalStorage = localStorage.getItem("accessToken");

              // If no existing data, create an array
              // Otherwise, convert the localStorage string to an array
              existingLocalStorage = existingLocalStorage ? JSON.parse(existingLocalStorage) : {};
              accessTokenLocalStorage = accessTokenLocalStorage ? accessTokenLocalStorage : '';

              // store new session in localStorage
              existingLocalStorage = cognitoUserSession;
              accessTokenLocalStorage = cognitoUserSession.getAccessToken().jwtToken;

              // Save back to localStorage
              setUserLocal(existingLocalStorage);
              localStorage.setItem('userToken', JSON.stringify(existingLocalStorage));
              localStorage.setItem('accessToken', accessTokenLocalStorage);

              // add new session to the store
              dispatch(updateUser(cognitoUserSession));
              // console.log("TTIME TODAY", moment().format('HH:mm:ss'));
              console.log("TTIME UNIX", moment.unix(userLocal.idToken.payload.exp).format('MMMM Do YYYY hh:mm:ss A'));


              // console.log("TTTime AWS.config.credentials", AWS.config.credentials);
              // AWS.config.credentials.refresh(err => {
              //   if (err) {
              //     console.log(err);
              //   } else {
              //     console.log('TTIME TOKEN SUCCESSFULLY UPDATED');
              //   }
              // });
            }
          });
        }

      }
    });


  }


  //check time interval function
  const checkTimeSessionToken = () => {
    // let today = moment().format('HH:mm:ss');
    // let expirationTime = moment.unix(userLocal.idToken.payload.exp).format('HH:mm:ss');
    let today = moment().unix();
    let expirationTime = moment
      .unix(userLocal.idToken.payload.exp);

    // if (moment(today, 'HH:mm:ss').isBefore(moment(expirationTime, 'HH:mm:ss'))) {
    if (today < expirationTime) {
      checkUserTokenValidity();
    } else {
      console.log("TTIME THERE IS TIME LEFT");
      // logoutUser();
    }
  }

  const reloadPage = () => {
    window.location.reload();
  }


  //sortByValues 
  const naturalCompare = (a, b) => {
    var ax = [], bx = [];

    a.replace(/(\d+)|(\D+)/g, function (_, $1, $2) { ax.push([$1 || Infinity, $2 || ""]) });
    b.replace(/(\d+)|(\D+)/g, function (_, $1, $2) { bx.push([$1 || Infinity, $2 || ""]) });

    while (ax.length && bx.length) {
      var an = ax.shift();
      var bn = bx.shift();
      var nn = (an[0] - bn[0]) || an[1].localeCompare(bn[1]);
      if (nn) return nn;
    }

    return ax.length - bx.length;

  }

  //function to put selected value in callsign
  const callSignSelected = (newValue) => {
    setSelectedCallSign(newValue);
  }

  const callSignAllSelected = (newValue) => {
    setSelectedAllCallSign(newValue);
  }

  //function to put the selected value in status
  const statusSelected = (newvalue) => {
    setSelectedStatus(newvalue);
  }

  //function to put the selected value in status
  const terminalAllSelected = (newvalue) => {
    setSelectedAllTerminal(newvalue);
    setSelectedAllCallSign("ALL");
  }

  //function to put the selected value in sort
  const sortSelected = (newvalue) => {
    setselectedSort(newvalue)
  }

  const showAtcoInfoModal = () => {
    setInfoModalVisibility(true);
  };

  const closeAtcoInfoModal = () => {
    setInfoModalVisibility(false);
  };

  const showAvailableChecklistModal = (gate, terminal, flight_id, direction) => {
    setAvailableModalVisibility(true)
    setGate(gate)
    setGateTerminal(terminal)
    setFlightId(flight_id)
    setDirection(direction)
  }
  const closeAvailableChecklistModal = () => {
    setAvailableModalVisibility(false)
  }

  const showCleaningChecklistModal = (gate, terminal, flight_id, direction) => {
    setCleaningModalVisibility(true)
    setGate(gate)
    setGateTerminal(terminal)
    setFlightId(flight_id)
    setDirection(direction)
  }
  const closeCleaningChecklistModal = () => {
    setCleaningModalVisibility(false)
  }

  const setStatusChecklist = (newValue, id) => {
    if (id === flightId) {
      setStatusModal(newValue);
    }
  }

  const logoutUser = () => {
    dispatch(wsDisconnect()); //disconnect websocket
    dispatch(updateUser(null)); //logout -> clear user data
    window.localStorage.removeItem('accessToken');
    window.localStorage.removeItem('userToken');
    window.localStorage.removeItem('newlyCleanedGates');
    props.history.push('/');
  }

  const { Option } = Select;
  console.log("pushback", pushbackData);
  let FMTerminal = user ? user.idToken.payload['custom:visible_terminal'] : userLocal.idToken.payload['custom:visible_terminal'];

  return (
    <React.Fragment>
      {(!wsOpened && errorDisplay) && (
        <div className="alert_notice">
          <Alert message="Error retrieving latest flights. Please reload the page and try again." type="error" className="alert_error" />
        </div>
      )}
      {checkConnection && (
        <div className="alert_notice">
          <Alert message="Error retrieving latest flights. Check internet connection and try again." type="error" className="alert_error" />
        </div>
      )}
      <div className="Appbar">
        <Row type="flex" justify="space-between" align="middle">
          <Col>
            <Button type="link" className="btn-profile">
              <img
                src={icon_profile_svg}
                alt="icon_user"
                className="info-profile"
              />
              {user ? user.idToken.payload.name : userLocal.idToken.payload.name}
            </Button>
            <Divider type="vertical" className="headerDivider" />
            <Button
              type="link"
              style={{ color: `${variables.appColor}` }}
              onClick={showAtcoInfoModal}
            >
              <img
                src={icon_information_svg}
                alt="icon_info"
                className="info-img"
              />
              <span className="hide-info">Information</span>
            </Button>
            <Divider type="vertical" className="headerDivider" />
            <Button
              type="link"
              style={{ color: `${variables.appColor}` }}
              onClick={logoutUser}
            >
              Log Out
            </Button>
          </Col>
          <Input
            size="small"
            placeholder="Quick Search"
            suffix={<SearchOutlined />}
            onChange={(e) => setSearchValue(e.target.value)}
            className="search"
          />
        </Row>
        <Row
          type="flex"
          className="filterRow"
        >
          <Col span={14}>
            <Row type="flex" justify="space-between">
              <div className="filterText" >
                Filter by Call Sign
              </div>
            </Row>
            <Row>
              {/* disable selectedTerminal on T1, T2, T3, T4 */}
              {(selectedTerminal === "T1" || selectedTerminal === "T2" || selectedTerminal === "T3" || selectedTerminal === "T4") ? (
                <Select
                  // suffixIcon={<CaretDownOutlined
                  //   style={{ fontSize: 15 }} />}
                  className="select terminal-select terminal-locked"
                  value={selectedTerminal}
                  disabled
                >
                  <Option value="disabled" disabled>{selectedTerminal}</Option>
                </Select>
              ) : (
                  <Select
                    // suffixIcon={<CaretDownOutlined
                    //   style={{ fontSize: 15 }} />}
                    className="select terminal-select"
                    onChange={terminalAllSelected}
                    value={selectedAllTerminal}
                  >
                    {Terminal.map((itm =>
                      <Option key={itm.id} value={itm.value}>{itm.name}</Option>
                    ))}
                  </Select>
                )}
              {/* selectedTerminal === all */}
              {/* {selectedTerminal === 'ALL' && (
                <Select
                  defaultValue="T1"
                  suffixIcon={<CaretDownOutlined
                    style={{ fontSize: 15 }} />}
                  className="select terminal-select"
                  onChange={setSelectedTerminal}
                >
                  {Terminal.map((itm =>
                    <Option key={itm.id} value={itm.value}>{itm.name}</Option>
                  ))}
                </Select>
              )} */}
              {(selectedTerminal === "T1" || selectedTerminal === "T2" || selectedTerminal === "T3" || selectedTerminal === "T4") && (
                <Select
                  // suffixIcon={<CaretDownOutlined
                  //   style={{ fontSize: 15 }} />}
                  defaultValue="ALL"
                  onChange={callSignSelected}
                  value={selectedCallSign}
                  className="select callSign-select"
                >
                  {(selectedTerminal === 'T1') && (
                    T1CallSign.map((itm =>
                      <Option key={itm.id} value={itm.value}>{itm.name}</Option>
                    ))
                  )}
                  {(selectedTerminal === 'T2') && (
                    T2CallSign.map((itm =>
                      <Option key={itm.id} value={itm.value}>{itm.name}</Option>
                    ))
                  )}
                  {(selectedTerminal === 'T3') && (
                    T3CallSign.map((itm =>
                      <Option key={itm.id} value={itm.value}>{itm.name}</Option>
                    ))
                  )}
                  {(selectedTerminal === 'T4') && (
                    T4CallSign.map((itm =>
                      <Option key={itm.id} value={itm.value}>{itm.name}</Option>
                    ))
                  )}
                </Select>
              )}
              {FMTerminal === "ALL" && (
                <Select
                  // suffixIcon={<CaretDownOutlined
                  //   style={{ fontSize: 15 }} />}

                  onChange={callSignAllSelected}
                  value={selectedAllCallSign}
                  className="select callSign-select"
                >
                  {(selectedAllTerminal === 'T1') && (
                    T1CallSign.map((itm =>
                      <Option key={itm.id} value={itm.value}>{itm.name}</Option>
                    ))
                  )}
                  {(selectedAllTerminal === 'T2') && (
                    T2CallSign.map((itm =>
                      <Option key={itm.id} value={itm.value}>{itm.name}</Option>
                    ))
                  )}
                  {(selectedAllTerminal === 'T3') && (
                    T3CallSign.map((itm =>
                      <Option key={itm.id} value={itm.value}>{itm.name}</Option>
                    ))
                  )}
                  {(selectedAllTerminal === 'T4') && (
                    T4CallSign.map((itm =>
                      <Option key={itm.id} value={itm.value}>{itm.name}</Option>
                    ))
                  )}
                  {(FMTerminal === 'ALL' && selectedAllTerminal === 'ALL') && (
                    AllCallSign.sort((a, b) => naturalCompare(a.value, b.value)).map((itm =>
                      <Option key={itm.id} value={itm.value}>{itm.name}</Option>
                    ))
                  )}
                </Select>
              )}
            </Row>
          </Col>
          <Col span={10}>
            <Row type="flex" justify="end">
              <div className="filterText">
                Filter by Status
              </div>
            </Row>
            <Row justify="end">
              <Select
                onChange={statusSelected}
                className="select status-select"
                value={selectedStatus}
              >
                {Status.map((itm =>
                  <Option key={itm.id} value={itm.value}>{itm.name}</Option>
                ))}
              </Select>
            </Row>
          </Col>
        </Row>
      </div>
      <div>
        <Row className="headerRow">
          <Col>
            <Radio.Group
              style={{ fontSize: 12 }}
              buttonStyle="solid"
              onChange={(e) => setselectedSort(e.target.value)}
            // value={selectedSort}
            >
              <Radio.Button value="Gate" span={3}>Gate</Radio.Button>
              <Radio.Button value="Flight">Flight Info</Radio.Button>
              <Radio.Button value="Direction">
                <span className="arr-span1">Arr/Dep</span>
                <span className="arr-span2">Arr/ Dep</span>
              </Radio.Button>
              <Radio.Button value="FreeFrom">Free From</Radio.Button>
              <Radio.Button value="FreeTill">Free Till</Radio.Button>
              <Radio.Button value="TimeLeft">Time Left</Radio.Button>
              <Radio.Button value="Status">Status</Radio.Button>
            </Radio.Group>
          </Col>
        </Row>
      </div>

      {/* <div style={styles.container}>   */}
      <div className="container">

        <FlipMove duration={700} staggerDurationBy={100}>
          {pushbackData &&
            pushbackData.map((pbdata, index) =>
              <Row type="flex" className={`row ${(pbdata.priority >= 2 && pbdata.latest_cleaning_status === 'AVAILABLE') ? "row-priority" : ""}`} key={index} align="middle">
                {/* verify color of status */}
                {/* {pbdata.latest_cleaning_status === 'AVAILABLE' && (
                  <div style={{ backgroundColor: "#F2600D", width: "4.5px", height: "100%" }} />
                )}
                {pbdata.latest_cleaning_status === 'CLEANING' && (
                  <div style={{ backgroundColor: "#F2600D", width: "4.5px", height: "100%" }} />
                )}
                {pbdata.latest_cleaning_status === 'NOT AVAILABLE' && (
                  <div style={{ backgroundColor: "#E5E5E5", width: "4.5px", height: "100%" }} />
                )}
                {pbdata.latest_cleaning_status === 'CLEANED' && (
                  <div style={{ backgroundColor: "#299935", width: "4.5px", height: "100%" }} />
                )} */}
                {/* {pbdata.terminal} */}
                {/* verify color of arr/dep */}
                {pbdata.direction === 'DEP' && (
                  <div className="dep-border" />
                )}
                {pbdata.direction === 'ARR' && (
                  <div className="arr-border" />
                )}

                {/* current_gate and assigned_call_sign */}
                <Col span={3}>
                  <Row align="center" className="mobile_text">{pbdata.current_gate}</Row>
                  <Row className="subRow" align="center">{pbdata.assigned_call_sign}</Row>
                </Col>
                <Divider type="vertical" className="divider" />

                {/* flight no and aircraft_type */}
                <Col span={3}>
                  {pbdata.flightno ? (
                    <Row align="center" className={`mobile_text ${pbdata.flightno.length >= 6 ? "mobile_text2" : ""}`}>{pbdata.flightno}</Row>
                  ) : (
                      <Row align="center" className="mobile_text">-</Row>
                    )}
                  {/* <Row align="center">{pbdata.flightno}</Row> */}
                  <Row className="subRow" align="center">{pbdata.aircraft_size}</Row>
                </Col>
                <Divider type="vertical" className="divider" />

                {/* arrival/depature */}
                {/* need to change this to icon arrival / departure */}
                {pbdata.direction === "DEP" && (
                  <Col span={2}>
                    <img
                      src={dep_icon}
                      alt="dep_icon"
                      className="dep_icon"
                    // style={{
                    //   width: "58px",
                    //   marginTop: "10px",
                    // }}
                    />
                  </Col>
                )}
                {pbdata.direction === "ARR" && (
                  <Col span={2}>
                    <img
                      src={arr_icon}
                      alt="arr_icon"
                      className="arr_icon"
                    // style={{
                    //   width: "58px",
                    //   marginTop: "10px",
                    // }}
                    />
                  </Col>
                )}
                {(pbdata.direction === undefined || pbdata.direction === null || pbdata.direction === '') && (
                  <Col span={2} className="mobile_text">-</Col>
                )}
                <Divider type="vertical" className="divider" />

                {/* free from */}
                {/* {pbdata.free_from ? (
                  <Col span={3} className="mobile_text">{pbdata.free_from} {pbdata.free_from_date} </Col>
                ) : (
                    <Col span={3} className="mobile_text">-</Col>
                  )} */}

                {pbdata.free_from ? (
                  <Col span={3}>
                    <Row align="center" className="mobile_text">{pbdata.free_from}</Row>
                    <Row align="center" className="date_text"> {pbdata.free_from_date ? pbdata.free_from_date.slice(0, 5) : null}</Row>
                  </Col>
                ) : (
                    <Col span={3}>
                      <Row align="center" className="mobile_text">-</Row>
                    </Col>
                  )}
                <Divider type="vertical" className="divider" />

                {/* free till */}
                {/* {pbdata.free_till ? (
                  <Col span={3} className="mobile_text">{pbdata.free_till} </Col>
                ) : (
                    <Col span={3} className="mobile_text">-</Col>
                  )} */}
                {pbdata.free_till ? (
                  <Col span={3}>
                    <Row align="center" className="mobile_text">{pbdata.free_till}</Row>
                    <Row align="center" className="date_text">{pbdata.free_till_date ? pbdata.free_till_date.slice(0, 5) : null}</Row>
                  </Col>
                ) : (
                    <Col span={3}>
                      <Row align="center" className="mobile_text">-</Row>
                    </Col>
                  )}
                <Divider type="vertical" className="divider" />

                {/* time left */}
                {/* this needs to be computed */}
                {pbdata.time_left ? (
                  <Col span={4} className="mobile_text">{pbdata.time_left} </Col>
                ) : (
                    <Col span={4} className="mobile_text">-</Col>
                  )}
                {/* <Col span={4}>{pbdata.time_left}</Col> */}
                <Divider type="vertical" className="divider" />


                {/* status */}
                {pbdata.latest_cleaning_status === 'AVAILABLE' && (
                  <Col span={4} className="col-btn">
                    <Button className={`btn ${pbdata.priority >= 2 ? "btn-cleanedNow" : "btn-available"}`} onClick={e => showAvailableChecklistModal(pbdata.current_gate, pbdata.terminal, pbdata.flight_id, pbdata.direction)}>
                      {`${pbdata.priority >= 2 ? "CLEAN NOW" : "AVAILABLE"}`} <CaretRightOutlined className="btn-caret" />
                    </Button>
                  </Col>
                )}
                {/* {pbdata.latest_cleaning_status === 'AVAILABLE' && (
                  <Col span={4} className="col-btn">
                    <Button className="btn btn-available" onClick={showAvailableChecklistModal}>
                      AVAILABLE <CaretRightOutlined className="btn-caret" />
                    </Button>
                  </Col>
                )} */}
                {pbdata.latest_cleaning_status === 'CLEANING' && (
                  <Col span={4} className="col-btn">
                    <Button className="btn btn-cleaning" onClick={e => showCleaningChecklistModal(pbdata.current_gate, pbdata.terminal, pbdata.flight_id, pbdata.direction)}>
                      CLEANING <CaretRightOutlined className="btn-caret" />
                    </Button>
                  </Col>
                )}
                {pbdata.latest_cleaning_status === 'NOT AVAILABLE' && (
                  <Col span={4} className="col-btn">
                    {/* available_next_flight is false (logic is not yet done for both false) */}
                    {(pbdata.availability_status === "NOT AVAILABLE - CURRENT FLIGHT") && (
                      <Popover placement="topRight" content="Unavailable due to current flight not yet arrived or departed" trigger="click">
                        <Button className="btn btn-notAvailable">
                          UNAVAIL.  <CaretRightOutlined className="btn-caret" />
                        </Button>
                      </Popover>
                    )}
                    {(pbdata.availability_status === "NOT AVAILABLE - SHARED GATE") && (
                      <Popover placement="topRight" content={`Unavailable due to flight ${pbdata.shared_gate_flight_no ?? ""} at a shared gate { ${pbdata.unavailable_shared_gate ?? ""} }`} trigger="click">
                        <Button className="btn btn-notAvailable">
                          UNAVAIL.  <CaretRightOutlined className="btn-caret" />
                        </Button>
                      </Popover>
                    )}
                    {/* {(pbdata.availability_status === "NOT AVAILABILE - CURRENT FLIGHT") ? (
                      <Popover placement="topRight" content="Unavailable due to flight has yet to arrive or depart" trigger="click">
                        <Button className="btn btn-notAvailable">
                          UNAVAIL.  <CaretRightOutlined className="btn-caret" />
                        </Button>
                      </Popover>
                    ) : (pbdata.availability_status === "NOT AVAILABILE - NEXT FLIGHT") ? (
                      <Popover placement="topRight" content="Unavailable due to next flight at this gate" trigger="click">
                        <Button className="btn btn-notAvailable">
                          UNAVAIL.  <CaretRightOutlined className="btn-caret" />
                        </Button>
                      </Popover>
                    ) : (
                          <Popover placement="topRight" content="Unavailable due to shared flight at this gate" trigger="click">
                            <Button className="btn btn-notAvailable">
                              UNAVAIL.  <CaretRightOutlined className="btn-caret" />
                            </Button>
                          </Popover>
                        )} */}
                    {/* available_shared_gate is false */}
                    {/* {pbdata.available_shared_gate != true && (
                      <Tooltip placement="topLeft" title="Unavailable due to shared flight at this gate">
                        <Button className="btn btn-notAvailable">
                          UNAVAIL.  <CaretRightOutlined className="btn-caret" />
                        </Button>
                      </Tooltip>
                    )} */}

                  </Col>
                )}
                {pbdata.latest_cleaning_status === 'CLEANED' && (
                  <Col span={5}>
                    <Row align="center" className="row-complete">
                      COMPLETE
                    </Row>
                    <Row align="center" className="row-complete-time">
                      {pbdata.last_cleaned_time}
                    </Row>
                  </Col>
                )}

              </Row>

            )
          }
        </FlipMove>

        <ModalInformation
          infoModalVisibility={infoModalVisibility}
          closeAtcoInfoModal={closeAtcoInfoModal}
        />

        <ChecklistModal
          availableModalVisibility={availableModalVisibility}
          closeAvailableChecklistModal={closeAvailableChecklistModal}
          gate={gate}
          terminal={gateTerminal}
          flight_id={flightId}
          direction={direction}
        />

        <CleaningModal
          cleaningModalVisibility={cleaningModalVisibility}
          closeCleaningChecklistModal={closeCleaningChecklistModal}
          gate={gate}
          terminal={gateTerminal}
          flight_id={flightId}
          setStatusChecklist={setStatusChecklist}
          direction={direction}
          gatesData={pushbackData}
        />
      </div>

      <div className="footer">
        <Row type="flex" justify="center">
          <img src={app_logo} alt="app_logo" className="footer_img" />
        </Row>
      </div>
    </React.Fragment>
  );
};

export default ATCO;
