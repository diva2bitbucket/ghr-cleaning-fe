import React, { useEffect } from "react";
import { Redirect } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { restoreSession } from "../../store/session";
import SplashScreen from "../SplashScreen";
// import ATCO from "../ATCO";
import Login from "../Login";


const Home = props => {
  const restoring = useSelector(state => state.session.restoring);
  const user = useSelector(state => state.session.user);
  const getToken = window.localStorage.getItem("accessToken");
  const dispatch = useDispatch();

  useEffect(() => {
    if (getToken) {
      checkAccessToken();
    }
  }, [user]);

  useEffect(() => {
    dispatch(restoreSession());
  }, [dispatch]);

  const checkAccessToken = () => {
    if (user) {
      if (user.accessToken.jwtToken === getToken) {
        // alert('ACCESS TOKEN SAME');
        return <Redirect to='/dashboard' />
      } else {
        // alert('ACCESS TOKEN NOT THE SAME');
        return <Redirect to='/' />;
      }
    } else {
      // alert('USER IS NULL');
      console.log("USER IS NULL");
      return <Login />;
    }

  }

  if (restoring == true) {
    return <SplashScreen />;
  }
  // if (user) {
  //   return <ATCO/>
  //   // return user.accessToken.payload["cognito:groups"] && user.accessToken.payload["cognito:groups"][0] == "atco" ? (
  //   //   <ATCO />
  //   // ) : (
  //   //   <Driver />
  //   // );
  // }
  // return <Login />;
  if (user) {
    // return <ATCO />
    if (user.accessToken.jwtToken === getToken) {
      return <Redirect to='/dashboard' />
    } else {
      return <Redirect to='/' />;
    }
  } else {
    return <Login />;
  }
};

export default Home;
