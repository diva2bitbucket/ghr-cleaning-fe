import React, {useState, useEffect, useRef} from "react";
import { Row, Col, Button, Icon, Modal, Divider, Input, Select, Checkbox} from "antd";
import { CaretRightOutlined, SearchOutlined, CaretDownOutlined } from "@ant-design/icons";
import "./styles.scss";
import moment from "moment";
import { wsConnect, wsDisconnect, wsSend, wsUpdate } from "../../store/pushback";
import constants from "../../configs/constants";
import { useSelector, useDispatch } from "react-redux";
import { getAllByTestId } from "@testing-library/react";
import _ from 'lodash';


const CleaningModal = props => {
    const [wallFinishesGoodCondition, setWallFinishesGoodCondition] = useState("")
    const [ceilingFinishesGoodCondition, setCeilingFinishesGoodCondition] = useState("")
    const [floorFinishesGoodCondition, setFloorFinishesGoodCondition] = useState("")
    const [waterCoolerGoodCondition, setWaterCoolerGoodCondition] = useState("")
    const [seatsGoodCondition, setSeatsGoodCondition] = useState("")
    const [binsGoodCondition, setBinsGoodCondition] = useState("")
    const [downRampGoodCondition, setDownRampGoodCondition] = useState("")
    const [otherFixturesGoodCondition, setOtherFixturesGoodCondition] = useState("")
    const [handRailingGoodCondition, setHandRailingGoodCondition] = useState("")
    const [airconGoodCondition, setAirconGoodCondition] = useState("")
    //const [dataList, setDataList] = useState(JSON.parse(localStorage.getItem("dataList")))
    const [startTime,setStartTime] = useState("")
 


    const dispatch = useDispatch()
    const user = useSelector((state) => state.session.user);
 
    
    // console.log("PROPS TERMINAL", props.terminal);
    // console.log("GATES DATA CLEANING", props.gatesData);
    

   
    const cancelSelection=(props)=>{
        props.closeCleaningChecklistModal()
    }

    let data;

    let dataList = JSON.parse(localStorage.getItem("dataList"));
    let cleanedGates = localStorage.getItem("newlyCleanedGates");
    useEffect(()=>{
       
        // console.log("USE EFFECT CALLED")
        let dataList2 = JSON.parse(localStorage.getItem("dataList"))
        if(dataList2) {
            let results = _.isEqual(dataList2, dataList)
            // console.log("RESULTS OF COMPARISON")
            // console.log(results)
            if(_.isEqual(dataList2, dataList)){
                // console.log("datalist are the same!")
            }
            else{
                // console.log("SET NOW")
                dataList = dataList2
            }
       }
      
        
        let timeStart = moment().format("YYYY-MM-DD HH:mm:ss")
        setStartTime(timeStart)
        
        if(props.direction){
            if(props.direction == "ARR"){
                // console.log("SETTING DEFAULT")
                if (props.terminal == "4") {
                    setCeilingFinishesGoodCondition("FALSE");
                    setWallFinishesGoodCondition("FALSE");
                    setFloorFinishesGoodCondition("FALSE");
                    setHandRailingGoodCondition("FALSE");
                    setAirconGoodCondition("FALSE");
                    setDownRampGoodCondition("");
                    setBinsGoodCondition("");
                    setWaterCoolerGoodCondition("");
                    setSeatsGoodCondition("");
                    setOtherFixturesGoodCondition("");
                } else {
                    setCeilingFinishesGoodCondition("FALSE")
                    setWallFinishesGoodCondition("FALSE");
                    setFloorFinishesGoodCondition("FALSE");
                    setHandRailingGoodCondition("FALSE");
                    setOtherFixturesGoodCondition("FALSE");
                    setAirconGoodCondition("");
                    setDownRampGoodCondition("");
                    setBinsGoodCondition("");
                    setWaterCoolerGoodCondition("");
                    setSeatsGoodCondition("");
                    
                }   
            }
            else{
                //direction is DEP
                if(props.terminal == "1" || props.terminal == "2"){
                    // console.log("TRUE 2")
                    setCeilingFinishesGoodCondition("FALSE");
                    setWallFinishesGoodCondition("FALSE");  
                    setFloorFinishesGoodCondition("FALSE");
                    setHandRailingGoodCondition("FALSE");
                    setOtherFixturesGoodCondition("FALSE");
                    setSeatsGoodCondition("");
                    setBinsGoodCondition("");
                    setDownRampGoodCondition("");
                    setWaterCoolerGoodCondition("")
                    setAirconGoodCondition("")
                }
                else if(props.terminal == "3"){
                    // console.log("TRUE 3")
                    setCeilingFinishesGoodCondition("FALSE")
                    setWallFinishesGoodCondition("FALSE");   
                    setFloorFinishesGoodCondition("FALSE");
                    setHandRailingGoodCondition("FALSE");
                    setOtherFixturesGoodCondition("FALSE");
                    setSeatsGoodCondition("");
                    setBinsGoodCondition("");
                    setDownRampGoodCondition("");
                    setWaterCoolerGoodCondition("");
                    setAirconGoodCondition("");
                }
                else if(props.terminal == "4"){
                    // console.log("TRUE 4")
                    setWallFinishesGoodCondition("FALSE");  
                    setFloorFinishesGoodCondition("FALSE");
                    setSeatsGoodCondition("FALSE");
                    setBinsGoodCondition("FALSE");
                    setDownRampGoodCondition("FALSE");
                    setCeilingFinishesGoodCondition("");
                    setOtherFixturesGoodCondition("");
                    setWaterCoolerGoodCondition("");
                    setAirconGoodCondition("");
                    setHandRailingGoodCondition("");
                }
            }
        }
        
        

        if(dataList !== null){
            for(var i = 0; i< dataList.length; i++){

                if(dataList[i].flight_id == props.flight_id){
                    // console.log("MATCH FOUND")
            
                    setCeilingFinishesGoodCondition(dataList[i].ceiling_finishes_good_condition)
                    setWallFinishesGoodCondition(dataList[i].wall_finishes_good_condition)   
                    setFloorFinishesGoodCondition(dataList[i].floor_finishes_good_condition)
                    setSeatsGoodCondition(dataList[i].seats_good_condition)
                    setBinsGoodCondition(dataList[i].bins_good_condition)
                    setDownRampGoodCondition(dataList[i].down_ramp_good_condition)
                    setOtherFixturesGoodCondition(dataList[i].other_fixtures_good_condition)
                    setWaterCoolerGoodCondition(dataList[i].water_cooler_good_condition)
                    setStartTime(dataList[i].last_cleaned_start)
                    setAirconGoodCondition(dataList[i].air_con_good_condition)
                    setHandRailingGoodCondition(dataList[i].hand_railings_good_condition)
                }
    
            }
        }

        
        // if (props.gatesData) {
        //     let gatesData = props.gatesData;
        //     let getCleanedGate = gatesData.find(item => {
        //     return item.flight_id == props.flight_id
        //     });

        //     console.log("CLEANED GATESSS", getCleanedGate);
        // }

    },[dataList,
    wallFinishesGoodCondition,
    ceilingFinishesGoodCondition,
    floorFinishesGoodCondition,
    waterCoolerGoodCondition,
    seatsGoodCondition,
    binsGoodCondition,
    downRampGoodCondition,
    otherFixturesGoodCondition, airconGoodCondition,
    handRailingGoodCondition, props.terminal, props.flight_id, props.direction, props.gatesData])

    const sendInfo =(e, flight_id,gate,terminal) =>{
     
        let timeCompleted = moment().format("YYYY-MM-DD HH:mm:ss")
        // console.log("TIME:")
        // console.log(timeCompleted)

        let status = "CLEANING";
 
        data = {
            action:constants.ACTION_UPDATE_VALIDATE_INSTRUCTION,
            flight_id: flight_id,
            user_id: user.idToken.payload['cognito:username'],
            ceiling_finishes_good_condition: ceilingFinishesGoodCondition,
            wall_finishes_good_condition: wallFinishesGoodCondition,
            floor_finishes_good_condition: floorFinishesGoodCondition,
            seats_good_condition: seatsGoodCondition,
            bins_good_condition: binsGoodCondition,
            down_ramp_good_condition: downRampGoodCondition,
            other_fixtures_good_condition: otherFixturesGoodCondition,
            last_cleaned_start: startTime,
            cleaning_status: "COMPLETE",
            last_cleaned_complete: timeCompleted,
            access_token: user.accessToken.jwtToken,
            water_cooler_good_condition: waterCoolerGoodCondition,
            current_gate: gate,
            hand_railings_good_condition: handRailingGoodCondition,
            air_con_good_condition: airconGoodCondition
        }

        
     
        // console.log("--CLEANING MODAL--")
        // console.log("data", data);
        console.log(wsSend(data))
        dispatch(wsSend(data));

        if(dataList !== null){
            for(var i = 0; i< dataList.length; i++){
                if(dataList[i].flight_id == flight_id){
                    dataList.splice(i, 1)
                    localStorage.setItem('dataList',JSON.stringify(dataList))
                }
            }
        }

        // // add new completed item in localStorage all complete gates

        // let latest_cleaning_status = 'CLEANED';
        // let availability_status = 'CLEANED';
        // let direction = '';
        // // let flight_id = '';
        // let flightno = '';
        // let free_from = '';
        // let free_till = '';
        // let time_left = '';
        // let time_remaining = '';
        // let aircraft_size = '';

        // let gatesData = props.gatesData;
        // let getCleanedGate = gatesData.find(item => {
        //     return item.flight_id == props.flight_id
        //     });

        // console.log("getCleanedGate next_flight_id", getCleanedGate.next_flight_id);

        // getCleanedGate = { 
        //     ...getCleanedGate, 
        //     latest_cleaning_status, 
        //     availability_status, 
        //     direction, 
        //     free_from, 
        //     free_till, 
        //     time_left, 
        //     flight_id: '', 
        //     flightno, 
        //     aircraft_size,
        //     time_remaining, 
        // };

        // console.log("CLEANED GATESSS", getCleanedGate);

        // if (getCleanedGate.next_flight_id) {
        //     if (cleanedGates === null || cleanedGates === undefined) {
        //         cleanedGates = cleanedGates ? JSON.parse(cleanedGates) : [];
        //         cleanedGates.push(getCleanedGate)
        //     } else {
    
        //         cleanedGates = cleanedGates ? JSON.parse(cleanedGates) : [];
        //         cleanedGates.push(getCleanedGate);
        //     }
        //     console.log("CLEANED GATESSS1", getCleanedGate);
        //     window.localStorage.setItem("newlyCleanedGates", JSON.stringify(cleanedGates));
        // } else {
        //     console.log("CLEANED GATESSS2", getCleanedGate);
        // }

        // console.log("CLEANED GATESSS", getCleanedGate2);

        // if (cleanedGates === null) {
        //     cleanedGates = cleanedGates ? JSON.parse(cleanedGates) : [];
        //     cleanedGates.push(getCleanedGate);
        // }

        



        setWallFinishesGoodCondition("")
        setFloorFinishesGoodCondition("")
        setCeilingFinishesGoodCondition("")
        setDownRampGoodCondition("")
        setBinsGoodCondition("")
        setWaterCoolerGoodCondition("")
        setSeatsGoodCondition("")
        setOtherFixturesGoodCondition("")
        setHandRailingGoodCondition("")
        setAirconGoodCondition("")



        props.closeCleaningChecklistModal();
        props.setStatusChecklist(status, flight_id);

    }

    const submitCleanedGate = () => {
        const accessToken = localStorage.getItem("accessToken");
        setTimeout(() => {
            dispatch(wsUpdate({ action: constants.ACTION_GET_CLEANED_GATES_DATA, access_token: accessToken }));
          }, 3000);
       
    }

    let count = 0
    const counter = () =>{
        
        count++;
        return count
    }




    return (
        <div className="Cleaning-Modal">
            <Modal
                className="cleaningModal cleaning-modal"
                title=""
                footer=""
                visible={props.cleaningModalVisibility}
                onOk={''}
                closeIcon=" "
                onCancel={ () => cancelSelection(props)}
            > 
            <Row align="center" className="cleaning-header">
                COMPLETE CLEANING
            </Row>
            <Row align="center" className="cleaning-subHeader">
                for gate
            </Row>
            <Row align="center" className="cleaning-gate">
                {props.gate}
            </Row>
            <Row style={{marginTop: "20px"}}>
                <Col className="cleaning-colHeader">
                    Please check that these items are cleaned.
                </Col>
            </Row>
            <Divider className="cleaning-divider1"/>

            {wallFinishesGoodCondition == "FALSE" ? <Row className="cleaning-row" align="middle">
                <Col span={1} align="center" className="cleaning-colText">
                {counter()}
                </Col>
                <Col span={8} align="center" className="cleaning-colText">
                <div style ={{textAlign:"left", paddingLeft:"5px"}}>Wall Finishes</div>
                </Col>
                
            </Row> : null}
            {wallFinishesGoodCondition == "FALSE" ? <Divider className="cleaning-divider2" /> : null}

            {ceilingFinishesGoodCondition == "FALSE" ? <Row className="cleaning-row"  align="middle">
                <Col span={1} align="center" className="cleaning-colText">
                {counter()}
                </Col>
                <Col span={8} align="center" className="cleaning-colText">
                <div style ={{textAlign:"left", paddingLeft:"5px"}}>Ceiling Finishes</div>
                </Col>
                
            </Row> : null}
            {ceilingFinishesGoodCondition == "FALSE"? <Divider className="cleaning-divider2"/> : null}

            
            {floorFinishesGoodCondition == "FALSE" ? <Row className="cleaning-row" align="middle">
                <Col span={1} align="center" className="cleaning-colText">
                {counter()}
                </Col>
                <Col span={8} align="center" className="cleaning-colText">
                <div style ={{textAlign:"left", paddingLeft:"5px"}}>Floor Finishes </div>
                </Col>
                
            </Row> : null}
            {floorFinishesGoodCondition == "FALSE" ? <Divider className="cleaning-divider2"/> : null}

            {seatsGoodCondition ? <Row className="cleaning-row" align="middle">
                <Col span={1} align="center" className="cleaning-colText">
                {counter()}
                </Col>
                <Col span={8} align="center" className="cleaning-colText">
                <div style ={{textAlign:"left", paddingLeft:"5px"}}> Seats </div>
                </Col>
                
            </Row> : null}
            {seatsGoodCondition == "FALSE" ? <Divider className="cleaning-divider2"/> : null}
            
            {binsGoodCondition == "FALSE" ? <Row className="cleaning-row" align="middle">
                <Col span={1} align="center" className="cleaning-colText">
                {counter()}
                </Col>
                <Col span={8} align="center" className="cleaning-colText">
                <div style ={{textAlign:"left", paddingLeft:"5px"}}>Bins</div>
                </Col>
                
            </Row> : null}
            {binsGoodCondition == "FALSE" ? <Divider className="cleaning-divider2"/> : null}

            

            {downRampGoodCondition == "FALSE" ? <Row className="cleaning-row" align="middle">
                <Col span={1} align="center" className="cleaning-colText">
                {counter()}
                </Col>
                <Col span={8} align="center" className="cleaning-colText">
                <div style ={{textAlign:"left", paddingLeft:"5px"}}>Down Ramp </div>
                </Col>
                
            </Row> : null}
            {downRampGoodCondition == "FALSE" ? <Divider className="cleaning-divider2"/> : null}


            {waterCoolerGoodCondition == "FALSE" ? <Row className="cleaning-row" align="middle">
                <Col span={1} align="center" className="cleaning-colText">
                {counter()}
                </Col>
                <Col span={8} align="center" className="cleaning-colText">
                <div style ={{textAlign:"left", paddingLeft:"5px"}}>Water Cooler</div>
                </Col>
                
            </Row> : null}
            {waterCoolerGoodCondition == "FALSE" ? <Divider className="cleaning-divider2"/> : null}

            {handRailingGoodCondition == "FALSE" ? <Row className="cleaning-row" align="middle">
                <Col span={1} align="center" className="cleaning-colText">
                {counter()}
                </Col>
                <Col span={8} align="center" className="cleaning-colText">
                <div style ={{textAlign:"left", paddingLeft:"5px"}}>Hand Railings </div>
                </Col>
                
            </Row> : null}
            {handRailingGoodCondition == "FALSE" ? <Divider className="cleaning-divider2"/> : null}

            {otherFixturesGoodCondition == "FALSE" ? <Row className="cleaning-row" align="middle">
                <Col span={1} align="center" className="cleaning-colText">
                {counter()}
                </Col>
                <Col span={8} align="center" className="cleaning-colText">
                <div style ={{textAlign:"left", paddingLeft:"5px"}}>Other Fixtures / Fittings Outlets</div>
                </Col>
                
            </Row> : null}
            {otherFixturesGoodCondition == "FALSE" ? <Divider className="cleaning-divider2"/> : null}

            {airconGoodCondition == "FALSE" ? <Row className="cleaning-row" align="middle">
                <Col span={1} align="center" className="cleaning-colText">
                {counter()}
                </Col>
                <Col span={8} align="center" className="cleaning-colText">
                <div style ={{textAlign:"left", paddingLeft:"5px"}}>Air Con Outlets </div>
                </Col>
                
            </Row> : null}
            {airconGoodCondition == "FALSE" ? <Divider className="cleaning-divider2"/> : null}
            <Row style={{marginTop: "5px"}}>
                <Col className="cleaning-colHeader" style={{ color: "#ff0800" }}>
                    Please remember to disinfect all common touchpoints.
                </Col>
            </Row>


            <Row align="middle" className="cleaning-buttonRow">
                <Col span={10} align="center">
                    <Button
                        className={"button-cleaning-cancel"} onClick={(e)=>cancelSelection(props)}
                        >
                            CANCEL
                    </Button>
                </Col>
                <Col span={10} align="center">
                    <Button
                        className={"button-complete"}  
                        // onClick={ 
                        //     (e) => sendInfo(e, props.flight_id, props.gate, props.terminal);
                        //     submitCleanedGate()
                        // }
                        onClick={ (e) => {
                            submitCleanedGate();
                            sendInfo(e, props.flight_id, props.gate, props.terminal);
                        }}
                        >
                        COMPLETE
                    </Button>
                </Col>

            </Row>
            </Modal>

        </div>
   
    );
};

export default CleaningModal; 
