import React, { useEffect, useState } from "react";
import { Redirect } from "react-router-dom";
import {
  AuthenticationDetails,
  CognitoUser,
  CognitoUserPool,
} from "amazon-cognito-identity-js";
import { Input, Button, message, Row } from "antd";
import { useSelector, useDispatch } from "react-redux";
import { updateUser } from "../../store/session";
import { usePrevious } from "../../hooks/usePrevious";
import constants from "../../configs/constants";
import app_logo from "../../assets/images/app_logo.png";
import './login.scss';

// const styles = {
//   container: {
//     maxWidth: 300,
//     marginLeft: "auto",
//     marginRight: "auto",
//   },
// };
const Login = (props) => {
  const user = useSelector((state) => state.session.user);
  // const [userName, setUserName] = useState(constants.USERNAME);
  // const [password, setPassword] = useState(constants.PASSWORD);
  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const dispatch = useDispatch();
  const prevUser = usePrevious(user);


  useEffect(() => {
    if (!prevUser && user) {
      // props.history.push("/atco");
    }
  }, [user]);

  const onUserLogin = () => {
    let sessionStorageValue = "";
    let userTokenValue = "";
    let authenticationData = {
      Username: userName,
      Password: password,
    };
    let authenticationDetails = new AuthenticationDetails(authenticationData);
    var poolData = {
      UserPoolId: constants.USERPOOLID,
      ClientId: constants.CLIENTID,
    };
    var userPool = new CognitoUserPool(poolData);
    let user = new CognitoUser({
      Username: userName,
      Pool: userPool,
    });


    user.authenticateUser(authenticationDetails, {
      onFailure: (err) => {
        console.log(err);
        message.error(err.message);
      },
      onSuccess: (result) => {


        try {
          // if( result.accessToken.payload["cognito:groups"][0] == "atco" ) {
          // sessionStorageValue = result.accessToken.payload["cognito:groups"][0] + "," + result.idToken.payload.name;
          // sessionStorageValue = result.idToken.payload.name;
          sessionStorageValue = result.accessToken.jwtToken;
          userTokenValue = result;
          // console.log("sessionStorageValue (login)")
          // console.log(sessionStorageValue)
          // sessionStorage.setItem("pbrsession", sessionStorageValue);
          window.localStorage.setItem("accessToken", sessionStorageValue);
          window.localStorage.setItem("userToken", JSON.stringify(userTokenValue));


          // }

        } catch (error) {
          console.log("session :" + error);
        }
        console.log("LOGIN RESULT", result);
        dispatch(updateUser(result));
        return <Redirect to='/dashboard' />
      },
    });
  };

  return (
    <div className="login-container">
      <div className="logo-container">
        <Row type="flex" justify="center">
          <img src={app_logo} alt="app_logo" className="app-logo" />
        </Row>
      </div>
      <div className="form-container">
        <Input
          className="input"
          placeholder="Username"
          value={userName}
          onChange={(e) => setUserName(e.target.value)}
          onPressEnter={() => { onUserLogin() }}
        />

        <Input
          className="input"
          placeholder="Password"
          type="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          onPressEnter={() => { onUserLogin() }}
        />
        <Button type="primary" className="btn-login" onClick={onUserLogin}>
          Login
        </Button>
      </div>
    </div>
  );
};

export default Login;
