import React from "react";
import logo_white from "../../assets/images/logo_white.png";
import logo_dark from "../../assets/images/logo_dark.png";

const ChangiLogo = props => (
  <img
    style={props.style}
    src={props.type === "white" ? logo_white : logo_dark}
    alt="logo"
  />
);

export default ChangiLogo;
