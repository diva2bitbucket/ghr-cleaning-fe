import React, { useEffect, useState, useRef, useLayoutEffect } from "react";
import { Row, Col, Button, Divider, Input, Icon, Radio, Checkbox, Menu, Dropdown, List, Card } from "antd";
import variables from "../../variables.scss";
// import icon_anchor from "../../assets/images/icon_anchor.png";
import icon_user from "../../assets/images/icon_user.png";
import icon_information from "../../assets/images/icon_information.png";

import constants from "../../configs/constants";
import { useSelector, useDispatch } from "react-redux";
// import Checkboxcustom from "../Checkbox";
import moment from "moment";
import { sortByKey, groupBy, sortByLetterFirst, uniqueArray } from "../../configs/utils";
import { wsConnect, wsDisconnect, wsSend } from "../../store/pushback";
import { updateUser } from "../../store/session";
import FlipMove from "react-flip-move";
import ModalInformation from "../ModalInformation/pbrmodalinfo";
import { SearchOutlined, EnterOutlined, CloseOutlined } from "@ant-design/icons";
import app_logo from "../../assets/images/app_logo.png";
import Login from "../Login";
import { getPbRestriction } from "../../store/session";
import { Link } from "react-router-dom";
import { usePrevious } from "../../hooks/usePrevious";


const icon_profile_svg = require("../../assets/images/icon_profile.svg");
const icon_information_svg = require("../../assets/images/icon_information.svg");

const styles = {
  container: {
    minHeight: "100vh",
    padding: "260px 20px 30px 20px",
  },
  row: {
    color: `${variables.appColor}`,
    boxShadow: "0px 4px 5px rgba(0, 0, 0, 0.3)",
    textAlign: "center",
    background: "#2B2C41",
    marginBottom: "1em",
    fontSize: 14,
  },
  divider: {
    background: "rgba(255, 255, 255, 0.5)",
    height: 16,
    top: 0,
  },
};


const PBRHOME = (props) => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.session.user);
  const wsOpened = useSelector((state) => state.pushback.wsOpened);
  const gatesData = useSelector((state) => state.pushback.gatesData);
  const aircraftSizeMapping = useSelector((state) => state.session.aircraftSizeMapping);
  const pbRestrictionMapping = useSelector(state => state.session.pbRestrictionMapping);
  const gateMapping = useSelector(
    (state) => state.session.gateMapping
  );
  const [pushbackData, setPushbackData] = useState();
  const [originalPushbackData, setOriginalPushbackData] = useState([]);  

  const [gmcClearFlag, setGmcClearFlag] = useState(false);
  const [gmc1Checked, setGMC1Checked] = useState(false);
  const [gmc2Checked, setGMC2Checked] = useState(false);
  const [gmc3Checked, setGMC3Checked] = useState(false);
  const [gmc4Checked, setGMC4Checked] = useState(false);
  const [sortKey, setSortKey] = useState("S");
  const [searchValue, setSearchValue] = useState("");
  const [infoModalVisibility, setInfoModalVisibility] = useState(false);

  const userDetails = sessionStorage.getItem('pbrsession') || [];
  const userInfo = (userDetails.length > 0) ? userDetails.split(',') : [];
  const [groupOfPier, setGroupByPier] = useState(); //aggregated array base on the groupby filter
  const [pierArray, setPierArray] = useState(); // unique values of pier/filter_location
  const [defaultPierArray, setDefaultPierArray] = useState(); // unique values of pier/filter_location
  const [subFilterOption, setSubFilterOption] = useState([]); // contains seconday option filters
  const [selectedSubFilter, setSelectedSubFilter] = useState([]); // selected secondary filter
  const [quickSearch1, setQuickSearch1] = useState("");
  const [quickSearch2, setQuickSearch2] = useState("");
  const [quickSearch3, setQuickSearch3] = useState("");
  const [quickSearch4, setQuickSearch4] = useState("");
  const [qsVisibility, setqsVisibility] = useState("");
  const [sessionCounter, setSessionCounter] = useState(0);

  let sessionFilterSub = sessionStorage.getItem("pbrsession_filter2");
  try {
    sessionFilterSub = (sessionFilterSub.length !== null && sessionFilterSub.length > 0 ) ? sessionFilterSub.split(",") : [];  
  } catch (error) {
    sessionFilterSub = [];
  }
  const [secondaryFilter, setSecondaryFilter] = useState(0);
  const [secFilterFlag, setSecFilterFlag] = useState(sessionFilterSub);

  const [numArrayFirst, setNumArrayFirst] = useState([]);
  const [numArraySecond, setNumArraySecond] = useState([]);
  const [numArrayThird, setNumArrayThird] = useState([]);
  const [numArrayFourth, setNumArrayFourth] = useState([]);
  
  const prevpierarray = usePrevious(pierArray);

  const node = useRef();
  

  useEffect(() => {
      dispatch(getPbRestriction());
    }, [dispatch]
  );


  //Effect for Main Filter, If there is session variable set it as filter
  useEffect(() => {
    try {
      // This will contain the new session filter
      let newSessFilter1 = []; 

      // Gather the session variable datas
      let gmcCheckedSession = sessionStorage.getItem("pbrsession_filter1");
      gmcCheckedSession = gmcCheckedSession.split(",");
  
      //Converts the sessionStorage value to boolean
      //We have to convert since session storage is string
      gmcCheckedSession.map((data,index) =>{
        if (data == "true") {
          newSessFilter1.push(true)
        }
        if (data == "false" || data == "") {
          newSessFilter1.push(false)
        }
      });
  
      //We re-trigger to set filterring base on the session values
      //As requirement we need to retain user filter selection
      if (newSessFilter1.includes(true) ) {
        setTimeout(function(){ 
          setGMC1Checked(newSessFilter1[0]); 
          setGMC2Checked(newSessFilter1[1]); 
          setGMC3Checked(newSessFilter1[2]); 
          setGMC4Checked(newSessFilter1[3]); 
        }, 3000);
      };
  
      // Gather the subfilter session variable data
      // This weill handle to check if user has secondary filter made
      // We need to re-trigger user filter selection if page reload
      let gmcCheckedSessionSub = sessionStorage.getItem("pbrsession_filter2");

      // Set the subfilter
      if (gmcCheckedSessionSub.length !== null && gmcCheckedSessionSub.length > 0) {
        gmcCheckedSessionSub = gmcCheckedSessionSub.split(",");
        if ( gmcCheckedSessionSub[0] != "" ) {
          let tempholder = [];
          tempholder = gmcCheckedSessionSub;
          setSecondaryFilter(gmcCheckedSessionSub.length);
          setTimeout(function(){
            setSelectedSubFilter(tempholder);
          }, 3500);

        } else {
          setSecondaryFilter(0);
        }

      }

      window.scrollTo(0, 0);
    } catch (error) {
      
    }

  }, []);


  useEffect( () => {
    // Get the pbrestricion mapping base on the csv
    try {
      let pbSorted = pbRestrictionMapping.data;
      pbSorted.sort(GetSortOrder("bay"));
      pbSorted.sort(function(a, b) {
        return a.bay.localeCompare(b.bay, undefined, {
            numeric: true,
            sensitivity: 'base'
        });
      });
      const grouppier = groupBy(pbSorted, 'filter_location');
      setGroupByPier(grouppier);
    } catch (error) {
    }
  }, [pbRestrictionMapping])



  useEffect( () => {
    let arr = [];

    for (let x in groupOfPier) {
      arr.push(x);
    }

    arr.sort(function(a, b) {
      return a.localeCompare(b, undefined, {
          numeric: false,
          sensitivity: 'base'
      });
    });

    setPierArray(sortByLetterFirst(arr));
    setDefaultPierArray(sortByLetterFirst(arr));
    window.scrollTo(0, 0);
  }, [groupOfPier])


  
  useEffect(() => {
    let subArr = [];

    for (let x in groupOfPier) {
      let objOptionValue = {};
      objOptionValue.label = x; // Secondary Filter Options Properites
      objOptionValue.value = x; // Secondary Filter Options Properites
      objOptionValue.checked = false; // Secondary Filter Options Properites
      subArr.push(objOptionValue); // We store temporary the option value to be used afterwards
    }
    
    subArr = sortByLetterFirst(subArr,"JSON");
    setSubFilterOption(subArr);

  },[pierArray, secondaryFilter])

  useEffect(() => {
    
    let arr = [];
    for (let x in groupOfPier) {
      arr.push(x);
    }



    if(selectedSubFilter !== undefined && selectedSubFilter.length > 0) {
      let selectFilter = sortByLetterFirst(selectedSubFilter);
      let filter1Length = sessionFilterGet("pbrsession_filter1");
      let filter2Length = sessionFilterGet("pbrsession_filter2");

      if (!filter1Length.includes("true") && filter2Length.length == 0) {
        setSelectedSubFilter(defaultPierArray);
      } else {
        let tempArr = sortByLetterFirst(selectedSubFilter);
        setSelectedSubFilter(selectedSubFilter);
      }
      
    } else {
      let filter1Length = sessionFilterGet("pbrsession_filter1");
      let filter2Length = sessionFilterGet("pbrsession_filter2");
      if (!filter1Length.includes("true") && filter2Length.length == 0) {
        setSelectedSubFilter(defaultPierArray);
      } else {
        let tempArr = sortByLetterFirst(arr);
        setSelectedSubFilter(tempArr);
      }
    }

  },[subFilterOption,selectedSubFilter])


  useEffect(() => {
    filterData();
    window.scrollTo(0, 0);
  }, [
    gmc1Checked,
    gmc2Checked,
    gmc3Checked,
    gmc4Checked,
    searchValue
  ]);

  useEffect(() => {
    try {
      let splitValue = searchValue.toUpperCase();
      splitValue = splitValue.split("");
  
      let tempValue = "";
      let pbSorted = pbRestrictionMapping.data;
      let groupBay = [];
      let groupBayIndex2 = [];
      let groupBayIndex3 = [];
      let groupBayIndex4 = [];
  
      pbSorted.sort(GetSortOrder("bay"));
      pbSorted.sort(function(a, b) {
        return a.bay.localeCompare(b.bay, undefined, {
            numeric: true,
            sensitivity: 'base'
        });
      });
  
      pbSorted.sort(GetSortOrder("bay"));
      pbSorted.sort(function(a, b) {
        return a.bay.localeCompare(b.bay, undefined, {
            numeric: true,
            sensitivity: 'base'
        });
      });
  
  
      if(splitValue.length >= 1) {
        splitValue.map((data,index) => {
          switch(index) {
            case 0:
              //Get bays
              let grouppier = groupBy(pbSorted, 'filter_location');
              
              let indexSearchValue1 = "";
              if (data == "2" || data == "3" || data == "4" || data == "5" || data == "6") {
                indexSearchValue1 = data+"00s";
              } else {
                indexSearchValue1 = data;
              }
      
              grouppier = grouppier[indexSearchValue1];
              grouppier.map( (index,data) => {
                groupBay.push(index.bay);
              });
      
              //Get second index of bays
              groupBay.map((data,index) => {
                //get the second char on the string
                groupBayIndex2.push(data.charAt(1));
              });
      
              groupBayIndex2 = uniqueArray(groupBayIndex2);
      
              setNumArraySecond(groupBayIndex2);
      
              if(data.includes("00s")) {
                tempValue = tempValue.replace("00s","");
              };
              
              setQuickSearch1(data);
              setQuickSearch2("");
              setQuickSearch3("");
              setQuickSearch4("");
              setNumArrayThird([]);
              setNumArrayFourth([]);
              break;
            case 1:
              //Get bays
              let indexSearchValue = quickSearch1+data;
              let grouppier2 = groupBy(pbSorted, 'filter_location');
              let tempGroupBayIndex3 = [];
              
              if (quickSearch1 == '2' || quickSearch1 == '3' || quickSearch1 == '4' || quickSearch1 == '5' || quickSearch1 == '6' ) {
                grouppier2 = grouppier2[quickSearch1+"00s"];  
              } else {
                grouppier2 = grouppier2[quickSearch1]; 
              }
      
              grouppier2.map( (index,data) => {
                groupBay.push(index.bay);
              });
      
              //Get second index of bays
              groupBay.map((datagroup,index) => {
                //combined first and second to check what possible for third
                if(datagroup.includes(indexSearchValue)) {
                  tempGroupBayIndex3.push(datagroup);
                };
              });
      
              tempGroupBayIndex3.map((datagroup,index) => {
                let tempData = datagroup.charAt(2);
      
                if (tempData != "") {
                  groupBayIndex3.push(datagroup.charAt(2));
                }
              })
      
      
      
              groupBayIndex3 = uniqueArray(groupBayIndex3);
              setNumArrayThird(groupBayIndex3);
              setQuickSearch2(data);
              
              if (groupBayIndex3.length == 0 && numArraySecond.includes(data)) {
                window.open(
                  "#/way2go/pbr?bay=" + searchValue.toLowerCase(),
                  "_self"
                )
              }

              setNumArrayFourth([]);
              break;
            case 2:
              //Get bays
              let indexSearchValue3 = quickSearch1+quickSearch2+data;
              let grouppier3 = groupBy(pbSorted, 'filter_location');
              let tempGroupBayIndex4 = [];
      
              if (quickSearch1 == '2' || quickSearch1 == '3' || quickSearch1 == '4' || quickSearch1 == '5' || quickSearch1 == '6' ) {
                grouppier3 = grouppier3[quickSearch1+"00s"];  
              } else {
                grouppier3 = grouppier3[quickSearch1]; 
              }
      
              grouppier3.map( (index,data) => {
                groupBay.push(index.bay);
              });
      
              //Get third index of bays
              groupBay.map((datagroup,index) => {
                //combined first and second and third to check what possible for third
                if(datagroup.includes(indexSearchValue3)) {
                  tempGroupBayIndex4.push(datagroup);
                };
              });
      
              tempGroupBayIndex4.map((datagroup,index) => {
                let tempData = datagroup.charAt(3);
                if (tempData != "") {
                  groupBayIndex4.push(datagroup.charAt(3));
                }
              })
      
      
              groupBayIndex4 = uniqueArray(groupBayIndex4);
              setQuickSearch3(data);

              if (groupBayIndex4.length == 0 && numArrayThird.includes(data)) {
                window.open(
                  "#/way2go/pbr?bay=" + searchValue.toLowerCase(),
                  "_self"
                )
              }

              setNumArrayFourth(groupBayIndex4);
              break;
            case 3:
              setQuickSearch4(data);
              if (numArrayFourth.includes(data)) {
                window.open(
                  "#/way2go/pbr?bay=" + searchValue.toLowerCase(),
                  "_self"
                )
              }
              break;
            default:
              setQuickSearch1("");
              setQuickSearch2("");
              setQuickSearch3("");
              setQuickSearch4("");
              // code block
          }
    
        })
      } else {
        setQuickSearch1("");
        setQuickSearch2("");
        setQuickSearch3("");
        setQuickSearch4("");
        setNumArraySecond([]);
        setNumArrayThird([]);
        setNumArrayFourth([]);
      }
    } catch (error) {
      
    }



  }, [
    searchValue
  ]);

  useEffect(() => {
    
    let stringValue = quickSearch1+quickSearch2+quickSearch3+quickSearch4;
    setSearchValue(stringValue);



  }, [
    quickSearch1,
    quickSearch2,
    quickSearch3,
    quickSearch4
  ]);

  useEffect(() => {
    // add when mounted
    document.addEventListener("mousedown", handleClick);
    // return function to be called when unmounted
    return () => {
      document.removeEventListener("mousedown", handleClick);
    };
  }, []);

  
  useEffect(() => {
    let filterCount = sessionFilterGet("pbrsession_filter2");
    setSecondaryFilter(filterCount.length);
  }, []);

  useEffect(() => {
    setSecFilterFlag(sessionFilterGet("pbrsession_filter2"))
  }, [secondaryFilter]);

  const filterData = () => {

    let newFilterData = [];
    let subArr = [];
    let newSessFilter1 = [];

    if (sessionCounter >= 1) {
      let sessionFilter1 = [gmc1Checked,gmc2Checked,gmc3Checked,gmc4Checked];
      sessionStorage.setItem("pbrsession_filter1", sessionFilter1);
    }

    if (gmc1Checked) {
        let filteredGatesData = pbRestrictionMapping.data.filter(
          (item) => item.gmc === "GMC1"
        )
        newFilterData = newFilterData.concat(filteredGatesData);
    }

    if (gmc2Checked) {
      let filteredGatesData = pbRestrictionMapping.data.filter(
        (item) => item.gmc === "GMC2"
      )
      newFilterData = newFilterData.concat(filteredGatesData);
    }

    if (gmc3Checked) {
        let filteredGatesData = pbRestrictionMapping.data.filter(
          (item) => item.gmc === "GMC3"
        )
        newFilterData = newFilterData.concat(filteredGatesData);
    }

    if (gmc4Checked) {
      let filteredGatesData = pbRestrictionMapping.data.filter(
        (item) => item.gmc === "GMC4"
      )
      newFilterData = newFilterData.concat(filteredGatesData);
    }

    //Group by pier default if no selection
    if (newFilterData.length == 0 ) {
      try {

        const grouppier = groupBy(pbRestrictionMapping.data, 'filter_location');
        setGroupByPier(grouppier);

        let arr = [];
        for (let x in grouppier) {
          let objOptionValue = {};
          arr.push(x);
          objOptionValue.label = x;
          objOptionValue.key = x;
          objOptionValue.value = x;
          objOptionValue.checked = false;
          subArr.push(objOptionValue);
        }

        arr = sortByLetterFirst(arr);
        setPierArray(arr);

        subArr = sortByLetterFirst(subArr);
        setSubFilterOption(subArr);
        
        let sessionFilter2 = sessionStorage.getItem('pbrsession_filter2');
        sessionFilter2 = sessionFilter2.split(",");
        if(sessionFilter2.length > 0) {
          setSelectedSubFilter(selectedSubFilter);
        } else {
          setSelectedSubFilter(arr);
        }

      } catch (error) {

      }
    } else {

      try {
        newFilterData = newFilterData.sort(GetSortOrder("bay"));
        newFilterData = newFilterData.sort(function(a, b) {
          return a.bay.localeCompare(b.bay, undefined, {
              numeric: true,
              sensitivity: 'base'
          });
        });

        const grouppier = groupBy(newFilterData, 'filter_location');
        setGroupByPier(grouppier);

        let arr = [];
        for (let x in grouppier) {
          let objOptionValue = {};
          arr.push(x);
          objOptionValue.label = x;
          objOptionValue.value = x;
          subArr.push(objOptionValue);
        }

        arr = sortByLetterFirst(arr);
        setPierArray(arr);
        subArr = sortByLetterFirst(subArr,"JSON");
        setSubFilterOption(subArr);


      } catch (error) {
        
      }

    }

    setSessionCounter(1);

  }


  const showAtcoInfoModal = () => {
    setInfoModalVisibility(true);
  };

  const closeAtcoInfoModal = () => {
    setInfoModalVisibility(false);
  };

  const onSubFilterChange = (e) => {
    try {
      let selectValue = e;
      sessionStorage.setItem("pbrsession_filter2", selectValue);

      if(selectValue.length > 0 && selectValue.length != null) {
        setSecondaryFilter(selectValue.length);
      } else {
        setSecFilterFlag(selectValue);
        setSecondaryFilter(0);
      }

      if (selectValue.length == 0) {
        setSelectedSubFilter(pierArray);
      } else {
        setSelectedSubFilter(e);
      }
      
    } catch (error) {

    }

  }

  const GetSortOrder = (prop) => {  
    return function(a, b) {  
        if (a[prop] > b[prop]) {  
            return 1;  
        } else if (a[prop] < b[prop]) {  
            return -1;  
        }  
        return 0;  
    }  
  }
  

  const quickSearchChange = (e,index,type) => {

    


    try {

      setqsVisibility(true);
      let value = "";
      if (type == "direct") {
        e.preventDefault();      
        value = e.target.value;
      } else {
        value = e;
      }

      let tempValue = value;
      let pbSorted = pbRestrictionMapping.data;
      let groupBay = [];
      let groupBayIndex2 = [];
      let groupBayIndex3 = [];
      let groupBayIndex4 = [];
  
      pbSorted.sort(GetSortOrder("bay"));
      pbSorted.sort(function(a, b) {
        return a.bay.localeCompare(b.bay, undefined, {
            numeric: true,
            sensitivity: 'base'
        });
      });
  
      pbSorted.sort(GetSortOrder("bay"));
      pbSorted.sort(function(a, b) {
        return a.bay.localeCompare(b.bay, undefined, {
            numeric: true,
            sensitivity: 'base'
        });
      });
  
      switch(index) {
        case '1':
          //Get bays
          let grouppier = groupBy(pbSorted, 'filter_location');
          
          let indexSearchValue1 = "";
          if (value == "2" || value == "3" || value == "4" || value == "5" || value == "6") {
            indexSearchValue1 = value+"00s";
          } else {
            indexSearchValue1 = value;
          }
  
          grouppier = grouppier[indexSearchValue1];
          grouppier.map( (index,data) => {
            groupBay.push(index.bay);
          });
  
          //Get second index of bays
          groupBay.map((data,index) => {
            //get the second char on the string
            groupBayIndex2.push(data.charAt(1));
          });
  
          groupBayIndex2 = uniqueArray(groupBayIndex2);
  
          setNumArraySecond(groupBayIndex2);
  
          if(value.includes("00s")) {
            tempValue = tempValue.replace("00s","");
          };
          
          setQuickSearch1(tempValue);
          setQuickSearch2("");
          setQuickSearch3("");
          setQuickSearch4("");
          setNumArrayThird([]);
          setNumArrayFourth([]);
  
          break;
        case '2':
          //Get bays
          let indexSearchValue = quickSearch1+value;
          let grouppier2 = groupBy(pbSorted, 'filter_location');
          let tempGroupBayIndex3 = [];
          
          if (quickSearch1 == '2' || quickSearch1 == '3' || quickSearch1 == '4' || quickSearch1 == '5' || quickSearch1 == '6' ) {
            grouppier2 = grouppier2[quickSearch1+"00s"];  
          } else {
            grouppier2 = grouppier2[quickSearch1]; 
          }
  
          grouppier2.map( (index,data) => {
            groupBay.push(index.bay);
          });
  
          //Get second index of bays
          groupBay.map((data,index) => {
            //combined first and second to check what possible for third
            if(data.includes(indexSearchValue)) {
              tempGroupBayIndex3.push(data);
            };
          });
  
          tempGroupBayIndex3.map((data,index) => {
            let tempData = data.charAt(2);
  
            if (tempData != "") {
              groupBayIndex3.push(data.charAt(2));
            }
          })
  
  
  
          groupBayIndex3 = uniqueArray(groupBayIndex3);
          setNumArrayThird(groupBayIndex3);
  
  
          setQuickSearch2(value);
          setNumArrayFourth([]);
          break;
  
        case '3':
          //Get bays
          let indexSearchValue3 = quickSearch1+quickSearch2+value;
          let grouppier3 = groupBy(pbSorted, 'filter_location');
          let tempGroupBayIndex4 = [];
  
          if (quickSearch1 == '2' || quickSearch1 == '3' || quickSearch1 == '4' || quickSearch1 == '5' || quickSearch1 == '6' ) {
            grouppier3 = grouppier3[quickSearch1+"00s"];  
          } else {
            grouppier3 = grouppier3[quickSearch1]; 
          }
  
          grouppier3.map( (index,data) => {
            groupBay.push(index.bay);
          });
  
          //Get third index of bays
          groupBay.map((data,index) => {
            //combined first and second and third to check what possible for third
            if(data.includes(indexSearchValue3)) {
              tempGroupBayIndex4.push(data);
            };
          });
  
          tempGroupBayIndex4.map((data,index) => {
            let tempData = data.charAt(3);
            if (tempData != "") {
              groupBayIndex4.push(data.charAt(3));
            }
          })
  
  
          groupBayIndex4 = uniqueArray(groupBayIndex4);
          setQuickSearch3(value);
          setNumArrayFourth(groupBayIndex4);
          break;
          
        case '4':
          setQuickSearch4(value);
          break;
        default:
          // code block
      }
  
      let searchValuesString = quickSearch1+quickSearch2+quickSearch3+quickSearch4;
      setSearchValue(searchValuesString);
    } catch (error) {
      
    }



  };

  const menu = () => {
    let letterArray = ["A","B","C","D","E","F","G","2","3","4","5","6"];
    let numArray = ["1","2","3","4","5","6","7","8","9","0"];
    let lastSeriesArray = ["L","R"];

    return (
      <Menu multiple={true} className={'menu-quick-search'}>
        <Menu.Item key="0">
          <Row span={24} >
              <Col span={6}>
                <Radio.Group onChange={(e) => quickSearchChange(e,'1','direct') } value={quickSearch1}>
                  {letterArray.map((data, index) =>
                    <Row key={index} type="flex" justify="space-between" align="middle" style={{textAlign:"center"}}>
                      <Radio className={'quickOption'+data} value={data}>{(data.includes("00s") ? data.replace("00s","") : data)}</Radio>
                    </Row>
                  )}
                </Radio.Group>
              </Col>

              <Col span={6}>
                <Radio.Group onChange={(e) => quickSearchChange(e,'2','direct') } value={quickSearch2}>
                  {numArraySecond.map((data, index) =>
                    <Row key={index} type="flex" justify="space-between" align="middle" style={{textAlign:"center"}}>
                      <Radio value={data}>{data}</Radio>
                    </Row>
                  )}
                </Radio.Group>
              </Col>

              <Col span={6}>
                <Radio.Group onChange={(e) => quickSearchChange(e,'3','direct') } value={quickSearch3}>
                  {numArrayThird.map((data, index) =>
                    <Row key={index} type="flex" justify="space-between" align="middle" style={{textAlign:"center"}}>
                      <Radio value={data}>{data}</Radio>
                    </Row>
                  )}
                </Radio.Group>
              </Col>

              <Col span={6}>
                <Radio.Group onChange={(e) => quickSearchChange(e,'4','direct') } value={quickSearch4}>
                  {numArrayFourth.map((data, index) =>
                    <Row key={index} type="flex" justify="space-between" align="middle" style={{textAlign:"center"}}>
                      <Radio value={data}>{data}</Radio>
                    </Row>
                  )}
                </Radio.Group>
              </Col>

          </Row>
        </Menu.Item>
      </Menu>
    )
  }


  const getSubFilter = () => {
    try {
      let gmcCheckedSessionSub = sessionStorage.getItem("pbrsession_filter2");

      if (gmcCheckedSessionSub.length > 0) {
        gmcCheckedSessionSub = gmcCheckedSessionSub.split(",");
        if ( gmcCheckedSessionSub[0] != "" ) {
          let tempholder = [];
          tempholder = gmcCheckedSessionSub;
          return tempholder;
  
        } else {
          return [];
        }
      }      
    } catch (error) {
      
    }

  }
  
  const handleClick = (e) => {
    try {
      if(e.target){
        if (node.current.contains(e.target)) {
          return;
        } else {
          setqsVisibility(false);
        }
      }
    } catch (error) {
      
    }
  }

  const sessionFilterGet = (sessionName) => {
    let sessionValue = "";
    //Get the session data base on the parameter
    sessionValue = sessionStorage.getItem(sessionName);

    try {
      if (sessionValue.length !== null && sessionValue.length > 0) {
        sessionValue = sessionValue.split(",");
        if ( sessionValue[0] != "" ) {
          return sessionValue;
        } else {
          return [];
        }
      } else {
        return [];
      }
    } catch (error) {
      return [];
    }

  }

  if (userInfo.length > 0) {
    return userInfo[0] == "atco" ? (
        <React.Fragment>
            <div className="Appbar">
            <Row type="flex" justify="space-between" align="middle">
                <Col>
                  <Button type="link" style={{ color: `${variables.appColor}` }}>
                      <img
                      src={icon_profile_svg}
                      alt="icon_user"
                      style={{
                          marginRight: 10,
                          position: "relative",
                          top: -2,
                          width: "15px",
                      }}
                      />
                      {userInfo[1]}
                  </Button>
                  <Divider type="vertical" style={styles.divider} />
                  <Button
                      type="link"
                      style={{ color: `${variables.appColor}` }}
                      onClick={showAtcoInfoModal}
                  >
                      <img
                      src={icon_information_svg}
                      alt="icon_info"
                      style={{
                          marginRight: 5,
                          position: "relative",
                          top: "-2px",
                          width: "15px",
                      }}
                      />
                      Information
                  </Button>
                  <Divider type="vertical" style={styles.divider} />
                  <Button
                    type="link"
                    style={{ color: `${variables.appColor}` }}
                    onClick={() => {
                    dispatch(wsDisconnect()); //disconnect websocket
                    sessionStorage.removeItem("pbrsession");
                    sessionStorage.removeItem("pbrsession_filter1");
                    sessionStorage.removeItem("pbrsession_filter2");
                    dispatch(updateUser(null)); //logout -> clear user data
                    window.location.reload();
                    }}
                >
                    Log Out
                </Button>
                </Col>
                <div ref={node}>
                <Dropdown visible={qsVisibility} overlayClassName={'qsSearchDropdown'} overlay={menu} trigger={['click']}>
                  <Input.Search
                    size="small"
                    placeholder="Quick Search"
                    suffix={(searchValue != "") ? <Link style={{color:"white"}} to={"pbr?bay=" + searchValue.toLowerCase()}><EnterOutlined/></Link> : <EnterOutlined/> }
                    className={'qsSearch'}
                    value={searchValue}
                    onChange={(e) => setSearchValue(e.target.value) }
                    onClick={e => {e.preventDefault(); setqsVisibility(true)}}
                    onSearch={value => (value != '') ? window.open(
                      //constants.CHANGIGMC_URL + pbdata.bay.toLowerCase()
                      "#/way2go/pbr?bay=" + value.toLowerCase(),
                      "_self"
                    ) : '' }
                    style={{
                        width: 200,
                        backgroundColor: `${variables.appBackgroundColor}`,
                    }}
                  />
                </Dropdown>
                </div>

            </Row>
            <Row
                type="flex"
                align="middle"
                style={{ margin: "30px 10px 10px 10px" }}
            >
                <Col span={14}>
                  <Row type="flex" justify="space-between">

                      {/* <Checkboxcustom checked={gmc1Checked} setChecked={setGMC1Checked}>
                        GMC 1
                      </Checkboxcustom>
                      <Checkboxcustom checked={gmc2Checked} setChecked={setGMC2Checked}>
                        GMC 2
                      </Checkboxcustom>
                      <Checkboxcustom checked={gmc3Checked} setChecked={setGMC3Checked}>
                        GMC 3
                      </Checkboxcustom>
                      <Checkboxcustom checked={gmc4Checked} setChecked={setGMC4Checked}>
                        GMC 4
                      </Checkboxcustom> */}
                      <div>
                        {(gmc1Checked || gmc2Checked || gmc3Checked || gmc4Checked) ? 
                          <CloseOutlined onClick={() => {
                              setGMC1Checked(false)
                              setGMC2Checked(false)
                              setGMC3Checked(false)
                              setGMC4Checked(false)
                            }} 
                              style={{ 
                                fontSize: '16px', 
                                verticalAlign:"middle",
                                position: 'relative',
                                top: "-4px"
                              }}  
                          /> 
                          : ""
                        }
                      </div>

                  </Row>
                </Col>
            </Row>

            <Row
              type="flex"
              align="middle"
              style={{ margin: "5px 10px 10px 18px", fontSize:"18px"}}
            >
              <Col span={23}>
                <Checkbox.Group className={'label-group secondary-filter'} style={{color:"white"}}
                  options={subFilterOption}
                  defaultValue={ secFilterFlag }
                  //onChange={(e) => setSelectedSubFilter(e)}
                  value = {secFilterFlag}
                  onChange={ (e) => {onSubFilterChange(e); setSelectedSubFilter(e) } }
                />
              </Col>
              {(secondaryFilter >= 1) ? 
              <Col span={1}>
                <CloseOutlined onClick={(e) => {sessionStorage.removeItem("pbrsession_filter2"); setSecondaryFilter(0); setSelectedSubFilter([]); setSecFilterFlag([]) } } style={{  fontSize: '16px', Align:"middle", position: 'relative', top: "-4px" }} />
                {/* <span style={{fontSize:"12px"}}>{secondaryFilter}</span> {secFilterFlag} */}
              </Col>
              :
              <div></div>
              }

            </Row>
            </div>
    
            <div style={styles.container}>
            <FlipMove duration={700} staggerDurationBy={100}>
              
              {pierArray && selectedSubFilter &&
                pierArray.map((pbdata, index) => (
                  <Row
                  type="flex"
                  key={pbdata}
                  align="middle"
                  className={'pbrow-cover ' + (selectedSubFilter.includes(pbdata) ? "visibility-true" : "visibility-false")}
                  style={{ borderLeft: "5px solid #DDC0A3", marginBottom: "20px"}}
                  gutter={[16, 16]}
                >
                    {<Col className={'highlight'} span={1}> </Col>}
                    <Col span={23}>
                    <Row gutter={[16, 16]}>
                        {groupOfPier[pbdata] && groupOfPier[pbdata].map((pbrow, pbrowindex) => (
                              
                          <Col key={pbrow.bay} span={4}>
                            <Button style={{ 
                              background: "#1B1C28", 
                              border: "2px solid #DDC0A3", 
                              boxShadow: "0px 5px 5px #000000", 
                              borderRadius: "15px",
                              fontFamily: "Roboto",
                              fontStyle: "normal",
                              fontWeight: "bold",
                              fontSize: "16px",
                              lineHeight: "19px",
                              /* identical to box height */
                              textAlign: "center",
                              letterSpacing: "0.1em",
                              color: "#FFFFFF",
                              height: "36px",
                              width: "90px"}} 
                              type="default" shape="round" size={'large'}
                              onClick={() =>
                                window.open(
                                  //constants.CHANGIGMC_URL + pbdata.bay.toLowerCase()
                                  "#/way2go/pbr?bay=" + pbrow.bay.toLowerCase(),
                                  "_self"
                                )
                              }
                              >
                              {pbrow.bay}
                            </Button>
                          </Col>

                        ))}
                      </Row>
                    </Col>
                  </Row>
                ))
              }
            </FlipMove>

              <ModalInformation
                infoModalVisibility={infoModalVisibility}
                closeAtcoInfoModal={closeAtcoInfoModal}
              />
            </div>
    
            <div style={{ paddingTop: 50, paddingBottom: 100, textAlign: "center" }}>
            <Row type="flex" justify="center">
                <img src={app_logo} alt="app_logo" style={{ width: 200, height: "100%" }} />
            </Row>
            </div>
        </React.Fragment>
    ) : (
        <Login />
    )      
  } else {
    return (<Login />)
  }

};

export default PBRHOME;
