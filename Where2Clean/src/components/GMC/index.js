import React, { useState, useEffect, useRef } from "react"
import { useSelector, useDispatch } from "react-redux";
import { Layout, Row, Col, Button, Divider, Input, Icon, Radio, Card, Collapse, AutoComplete, Empty, Modal, Dropdown, Menu} from "antd";
import { EnterOutlined, CloseOutlined, DoubleLeftOutlined, DoubleRightOutlined, CaretUpOutlined, CaretDownOutlined } from '@ant-design/icons';
import SplashScreen from "../SplashScreen";
import Login from "../Login";
import style from "./style.css";
// import icon_anchor from "../../assets/images/icon_anchor.png";
import { getPbRestriction } from "../../store/session";
import constants from "../../configs/constants";
import {
    doesFileExist,
    groupBy, 
    sortByLetterFirst, 
    uniqueArray
  } from "../../configs/utils";
import { Link } from "react-router-dom";
import variables from "../../variables.scss";

// const pbImageDefault = require("../../assets/images/b8_after_facenorth.png");
// const legendImage = require("../../assets/images/gmc_restrictions_legend.png");
// const icon_homepbr = require("../../assets/images/home_pbr.svg");

const { Header, Footer, Sider, Content } = Layout;
const { Search } = Input;
const { Panel } = Collapse;

const styles = {
    container: {
      minHeight: "100vh",
      padding: "260px 20px 30px 20px",
    },
    row: {
      color: `${variables.appColor}`,
      boxShadow: "0px 4px 5px rgba(0, 0, 0, 0.3)",
      textAlign: "center",
      background: "#2B2C41",
      marginBottom: "1em",
      fontSize: 14,
    },
    divider: {
      background: "rgba(255, 255, 255, 0.5)",
      height: 16,
      top: 0,
    },
  };
  
const GMC = (props) => {

    const dispatch = useDispatch();


    const user = useSelector(state => state.session.user);
    const pbRestrictionMapping = useSelector(state => state.session.pbRestrictionMapping);

    const [currentMappingData, setMappingData] = useState("");
    const [bayNumber, setBayNumber] = useState("");
    const [pbInstruction1, setPbInstruction1] = useState("");
    const [pbInstruction2, setPbInstruction2] = useState("");
    const [pbInstruction3, setPbInstruction3] = useState("");
    const [pbInstructionBtn1, setPbInstructionBtn1] = useState("button-atc gmc-pb");
    const [pbInstructionBtn2, setPbInstructionBtn2] = useState("button-default gmc-pb");
    const [pbInstructionBtn3, setPbInstructionBtn3] = useState("button-default gmc-pb");
    const [selectedPusback, setSelectedPusback] = useState("");
    const [pushbackType, setPushbackType] = useState('during');
    const [searchList, setSearchList] = useState([]);
    const [radioTrans, setRt] = useState("");
    const [instructionImage, setInstructionImage] = useState("");
    const [modalVisibility, setInfoModalVisibility] = useState(false);
    const [animationImage, setAnimationImage] = useState("");
    const [animationImageFlag, setAnimationImageFlag] = useState("");
    const [pbIndexSelected, setPbIndexSelected] = useState(1);
    const [pbImage, setPbImage] = useState("");
    const [hasResult, setHasResult] = useState(0);
    const [displayNoneImage, setDisplayNoneImage] = useState("");
    const [displayMainImage, setDisplayMainImage] = useState("");
    const [currentBayIndex, setCurrentBayIndex] = useState("");
    const [nextBayIndex, setNextBayIndex] = useState("");
    const [prevBayIndex, setPrevBayIndex] = useState("");
    const [pbDescription, setPBDescription] = useState("");

    const userDetails = sessionStorage.getItem('pbrsession');

    let userDetailsList = "";
    if(userDetails) {
        try {
        userDetailsList = userDetails.split(',');
        } catch (error) {
            
        }
    } else {
        userDetailsList = "";
    }
    
    const userInfo = userDetailsList;
    
    const [searchValue, setSearchValue] = useState("");
    const [quickSearch1, setQuickSearch1] = useState("");
    const [quickSearch2, setQuickSearch2] = useState("");
    const [quickSearch3, setQuickSearch3] = useState("");
    const [quickSearch4, setQuickSearch4] = useState("");
    const [qsVisibility, setqsVisibility] = useState("");
    const [numArrayFirst, setNumArrayFirst] = useState([]);
    const [numArraySecond, setNumArraySecond] = useState([]);
    const [numArrayThird, setNumArrayThird] = useState([]);
    const [numArrayFourth, setNumArrayFourth] = useState([]);
    const node = useRef();

    useEffect(() => {
        dispatch(getPbRestriction());
      }, [dispatch]
    );

    useEffect(() => {
        // Search for Query in the URL

        const params = new URLSearchParams(props.location.search);
        const bay = params.get("bay"); // Finds the bay parameter

        // Check if bay is available on the mapping
        if (bay != null) {
            // Set the bay on the state
            setBayNumber(bay.toUpperCase());
            try {
                // Get the row that currently matched
                let bayDetails = pbRestrictionMapping.data.filter(
                    row => row.bay.toUpperCase() == bay.toUpperCase()
                );

                if(bayDetails.length > 0) {
                    setHasResult(bayDetails.length);
                    setMappingData(bayDetails);
                    setSelectedPusback(bayDetails[0].app_pushback_instruction_1);
                    setPbInstruction1(bayDetails[0].app_pushback_instruction_1);
                    setPbInstruction2(bayDetails[0].app_pushback_instruction_2);
                    setPbInstruction3(bayDetails[0].app_pushback_instruction_3);
                    setRt(bayDetails[0].pushback_instruction_1_radio); // default RT
                    //setAnimationImage(bayDetails[0].app_pushback_instruction_1_animation);
                    setInstructionImage(bayDetails[0].pushback_instruction_1_during_image)
                    let imageRender = window.location.origin+constants.MEDIA_PATH+bayDetails[0].pushback_instruction_1_during_image+".png";
                    //let imageRender = "https://uat-ao.changiairport.com/"+constants.MEDIA_PATH+bayDetails[0].pushback_instruction_1_during_image+".png";
                    checkImage( imageRender, 
                        function(){ setPbImage(imageRender); console.log("image available"); setDisplayMainImage(true); setDisplayNoneImage(false)}, 
                        function(){ setPbImage(""); console.log("image not available");setDisplayMainImage(false); setDisplayNoneImage(true)} 
                    );

                    let animationImageRender = window.location.origin+constants.MEDIA_PATH+bayDetails[0].app_pushback_instruction_1_animation+".gif";
                    //let animationImageRender = "https://uat-ao.changiairport.com/"+constants.MEDIA_PATH+bayDetails[0].app_pushback_instruction_1_animation+".gif";
                    checkImageAnimation( animationImageRender, 
                        function() { 
                            setAnimationImage(animationImageRender); 
                            setAnimationImageFlag(true); 
                            //displayAnimation(); 
                        }, 
                        function() { 
                            //let pdfDetails = "https://uat-ao.changiairport.com/"+constants.MEDIA_PATH+bayDetails[0].app_pushback_instruction_1_animation+".pdf";
                            let pdfDetails = window.location.origin+constants.MEDIA_PATH+bayDetails[0].app_pushback_instruction_1_animation+".pdf";
                            setAnimationImage(pdfDetails); 
                            setAnimationImageFlag(false)
                        } 
                    );

                    setPbIndexSelected(1);
                    setPBDescription(bayDetails[0]["pushback_instruction_"+pbIndexSelected+"_during_description"]);
                    
                    
                }


                //Get recommendation next or next before bay
                if(bayDetails.length > 0) {

                    let bayLength = pbRestrictionMapping.data.length - 1;
                    let bayIndex = 0;
                    let bayIndexNext = 0;
                    let bayIndexPrev = 0;

                    pbRestrictionMapping.data.map((data,index) => {

                        if(data.bay == bayDetails[0].bay) {
                            bayIndex = index;
                            
                        }
                    });

                    setCurrentBayIndex(bayIndex);

                    //If current index is the first index
                    if(bayIndex == 0) {
                        bayIndexNext = bayIndex + 1;
                        setNextBayIndex(bayIndexNext);
                        setPrevBayIndex(bayLength);     
                    } else if(bayIndex == bayLength) {
                    //If current index is the last index
                        bayIndexNext = bayIndex + 1;
                        bayIndexPrev = bayIndex - 1 ;
                        setNextBayIndex(0);
                        setPrevBayIndex([bayIndexPrev]);
                    } else {
                    //if Currnet index is in the range
                        bayIndexNext = bayIndex + 1;
                        bayIndexPrev = bayIndex - 1 ;
                        setNextBayIndex(bayIndexNext);
                        setPrevBayIndex(bayIndexPrev);
                    }



                }

            } catch (error) {
                //console.log(error);
            }

        }


    },[pbRestrictionMapping]);


    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])

    const pbButtonChange = (btnValue, btnInstruction) => {
        setSelectedPusback(btnInstruction);
        setPbIndexSelected(btnValue);
        let animationImageRender = window.location.origin+constants.MEDIA_PATH+currentMappingData[0]["app_pushback_instruction_"+btnValue+"_animation"]+".gif";
        //let animationImageRender = "https://uat-ao.changiairport.com/"+constants.MEDIA_PATH+currentMappingData[0]["app_pushback_instruction_"+btnValue+"_animation"]+".gif";
        checkImageAnimation( animationImageRender, 
            function() {  
                setAnimationImage(animationImageRender); 
                setAnimationImageFlag(true); 
            }, 
            function() {
                let pdfDetails = window.location.origin+constants.MEDIA_PATH+currentMappingData[0]["app_pushback_instruction_"+btnValue+"_animation"]+".pdf";
                setAnimationImage(pdfDetails); 
                setAnimationImageFlag(false)
            } 
        );

        switch(btnValue) {
            case 1:
                setPbInstructionBtn1("button-atc gmc-pb");
                setPbInstructionBtn2("button-default gmc-pb");
                setPbInstructionBtn3("button-default gmc-pb");
                setRt(currentMappingData[0].pushback_instruction_1_radio);
                break;

            case 2:
                setPbInstructionBtn1("button-default gmc-pb");
                setPbInstructionBtn2("button-atc gmc-pb");
                setPbInstructionBtn3("button-default gmc-pb");
                setRt(currentMappingData[0].pushback_instruction_2_radio);
                break;

            case 3:
                setPbInstructionBtn1("button-default gmc-pb");
                setPbInstructionBtn2("button-default gmc-pb");
                setPbInstructionBtn3("button-atc gmc-pb");
                setRt(currentMappingData[0].pushback_instruction_3_radio);
                break;
            default:
                setPbInstructionBtn1("button-atc gmc-pb");
                setPbInstructionBtn2("button-default gmc-pb");
                setPbInstructionBtn3("button-default gmc-pb");
                setRt(currentMappingData[0].pushback_instruction_1_radio);
                setPbIndexSelected(1);
        }

        if(pushbackType == "during") {
            let imageRender = window.location.origin+constants.MEDIA_PATH+currentMappingData[0]["pushback_instruction_"+btnValue+"_during_image"]+".png";
            checkImage(imageRender, function(){ setPbImage(imageRender); console.log("image available"); setDisplayMainImage(true); setDisplayNoneImage(false)}, function(){ setPbImage(""); console.log("image not available");setDisplayMainImage(false); setDisplayNoneImage(true)} );
            
            setPBDescription(currentMappingData[0]["pushback_instruction_"+pbIndexSelected+"_during_description"]);
            
        }
        if(pushbackType == "complete") {
            let imageRender = window.location.origin+constants.MEDIA_PATH+currentMappingData[0]["pushback_instruction_"+btnValue+"_after_image"]+".png";
            checkImage(imageRender, function(){ setPbImage(imageRender); console.log("image available"); setDisplayMainImage(true); setDisplayNoneImage(false)}, function(){ setPbImage(""); console.log("image not available");setDisplayMainImage(false); setDisplayNoneImage(true)} );

            setPBDescription(currentMappingData[0]["pushback_instruction_"+pbIndexSelected+"_after_description"]);
        }
    }

    const searchBay = (valueSearch) => {
        try {
            let searchDetails = [];
            pbRestrictionMapping.data.filter(
                row => {
                    let defaultString = row.bay.toLowerCase();
                    let searchString = valueSearch.toLowerCase();
                    if (searchString != "") {
                        if (defaultString.startsWith(searchString)) {
                            searchDetails.push(renderItem(row.bay));
                        }
                    }
                }
            );
            
            //Sort Alphanumeric 
            let myArrayObjects = searchDetails.sort(function(a, b) {
                return a.value.localeCompare(b.value, undefined, {
                    numeric: true,
                    sensitivity: 'base'
                });
            });
            setSearchList(myArrayObjects);
            
        } catch (error) {
            //console.log(error)
        }

    }

    const searchEnter = (valueEnter) => {
        reInitializeDisplay(valueEnter);
    }

    const renderItem = (title, count) => {
    return {
        value: title
    };
    };

    useEffect(() => {
        try {
          let splitValue = searchValue.toUpperCase();
          splitValue = splitValue.split("");
      
          let tempValue = "";
          let pbSorted = pbRestrictionMapping.data;
          let groupBay = [];
          let groupBayIndex2 = [];
          let groupBayIndex3 = [];
          let groupBayIndex4 = [];
      
          pbSorted.sort(GetSortOrder("bay"));
          pbSorted.sort(function(a, b) {
            return a.bay.localeCompare(b.bay, undefined, {
                numeric: true,
                sensitivity: 'base'
            });
          });
      
          pbSorted.sort(GetSortOrder("bay"));
          pbSorted.sort(function(a, b) {
            return a.bay.localeCompare(b.bay, undefined, {
                numeric: true,
                sensitivity: 'base'
            });
          });
      
      
          if(splitValue.length >= 1) {
            splitValue.map((data,index) => {
              switch(index) {
                case 0:
                  //Get bays
                  let grouppier = groupBy(pbSorted, 'filter_location');
                  
                  let indexSearchValue1 = "";
                  if (data == "2" || data == "3" || data == "4" || data == "5" || data == "6") {
                    indexSearchValue1 = data+"00s";
                  } else {
                    indexSearchValue1 = data;
                  }
          
                  grouppier = grouppier[indexSearchValue1];
                  grouppier.map( (index,data) => {
                    groupBay.push(index.bay);
                  });
          
                  //Get second index of bays
                  groupBay.map((data,index) => {
                    //get the second char on the string
                    groupBayIndex2.push(data.charAt(1));
                  });
          
                  groupBayIndex2 = uniqueArray(groupBayIndex2);
          
                  setNumArraySecond(groupBayIndex2);
          
                  if(data.includes("00s")) {
                    tempValue = tempValue.replace("00s","");
                  };
                  
                  setQuickSearch1(data);
                  setQuickSearch2("");
                  setQuickSearch3("");
                  setQuickSearch4("");
                  setNumArrayThird([]);
                  setNumArrayFourth([]);
                  break;
                case 1:
                  //Get bays
                  let indexSearchValue = quickSearch1+data;
                  let grouppier2 = groupBy(pbSorted, 'filter_location');
                  let tempGroupBayIndex3 = [];
                  
                  if (quickSearch1 == '2' || quickSearch1 == '3' || quickSearch1 == '4' || quickSearch1 == '5' || quickSearch1 == '6' ) {
                    grouppier2 = grouppier2[quickSearch1+"00s"];  
                  } else {
                    grouppier2 = grouppier2[quickSearch1]; 
                  }
          
                  grouppier2.map( (index,data) => {
                    groupBay.push(index.bay);
                  });
          
                  //Get second index of bays
                  groupBay.map((datagroup,index) => {
                    //combined first and second to check what possible for third
                    if(datagroup.includes(indexSearchValue)) {
                      tempGroupBayIndex3.push(datagroup);
                    };
                  });
          
                  tempGroupBayIndex3.map((datagroup,index) => {
                    let tempData = datagroup.charAt(2);
          
                    if (tempData != "") {
                      groupBayIndex3.push(datagroup.charAt(2));
                    }
                  })
          
          
          
                  groupBayIndex3 = uniqueArray(groupBayIndex3);
                  setNumArrayThird(groupBayIndex3);
                  setQuickSearch2(data);
                  
                  if (groupBayIndex3.length == 0 && numArraySecond.includes(data)) {
                    reInitializeDisplay(searchValue);
                  }
    
                  setNumArrayFourth([]);
                  break;
                case 2:
                  //Get bays
                  let indexSearchValue3 = quickSearch1+quickSearch2+data;
                  let grouppier3 = groupBy(pbSorted, 'filter_location');
                  let tempGroupBayIndex4 = [];
          
                  if (quickSearch1 == '2' || quickSearch1 == '3' || quickSearch1 == '4' || quickSearch1 == '5' || quickSearch1 == '6' ) {
                    grouppier3 = grouppier3[quickSearch1+"00s"];  
                  } else {
                    grouppier3 = grouppier3[quickSearch1]; 
                  }
          
                  grouppier3.map( (index,data) => {
                    groupBay.push(index.bay);
                  });
          
                  //Get third index of bays
                  groupBay.map((datagroup,index) => {
                    //combined first and second and third to check what possible for third
                    if(datagroup.includes(indexSearchValue3)) {
                      tempGroupBayIndex4.push(datagroup);
                    };
                  });
          
                  tempGroupBayIndex4.map((datagroup,index) => {
                    let tempData = datagroup.charAt(3);
                    if (tempData != "") {
                      groupBayIndex4.push(datagroup.charAt(3));
                    }
                  })
          
          
                  groupBayIndex4 = uniqueArray(groupBayIndex4);
                  setQuickSearch3(data);
    
                  if (groupBayIndex4.length == 0 && numArrayThird.includes(data)) {
                    reInitializeDisplay(searchValue);
                  }
    
                  setNumArrayFourth(groupBayIndex4);
                  break;
                case 3:
                  setQuickSearch4(data);
                  if (numArrayFourth.includes(data)) {
                    reInitializeDisplay(searchValue);
                  }
                  break;
                default:
                  setQuickSearch1("");
                  setQuickSearch2("");
                  setQuickSearch3("");
                  setQuickSearch4("");
                  // code block
              }
        
            })
          } else {
            setQuickSearch1("");
            setQuickSearch2("");
            setQuickSearch3("");
            setQuickSearch4("");
            setNumArraySecond([]);
            setNumArrayThird([]);
            setNumArrayFourth([]);
          }
        } catch (error) {
          
        }
    
    
    
      }, [
        searchValue
      ]);

    useEffect(() => {
        
        let stringValue = quickSearch1+quickSearch2+quickSearch3+quickSearch4;
        setSearchValue(stringValue);
    
    
    
      }, [
        quickSearch1,
        quickSearch2,
        quickSearch3,
        quickSearch4
      ]);

    useEffect(() => {
        // add when mounted
        document.addEventListener("mousedown", handleClick);
        // return function to be called when unmounted
        return () => {
          document.removeEventListener("mousedown", handleClick);
        };
      }, []);

      
    const SearchOptions = [
        {
            options: searchList
        }
    ];
    
    const searchOptionChange = (value) => {
        if(bayNumber != value) {
            reInitializeDisplay(value);
        }
    }

    const reInitializeDisplay = (bay) => {
        // Check if bay is available on the mapping
        if (bay != null) {

            try {
                // Get the row that currently matched
                let bayDetails = pbRestrictionMapping.data.filter(
                    row => row.bay.toUpperCase() == bay.toUpperCase()
                );
    
                if(bayDetails.length > 0) {
                    // Set the bay on the state
                    setBayNumber(bay.toUpperCase());
                    setHasResult(bayDetails.length);
                    setMappingData(bayDetails);
                    setSelectedPusback(bayDetails[0].app_pushback_instruction_1);
                    setPbInstruction1(bayDetails[0].app_pushback_instruction_1);
                    setPbInstruction2(bayDetails[0].app_pushback_instruction_2);
                    setPbInstruction3(bayDetails[0].app_pushback_instruction_3);
                    setPbInstructionBtn1("button-atc gmc-pb");
                    setPbInstructionBtn2("button-default gmc-pb");
                    setPbInstructionBtn3("button-default gmc-pb");
                    setPushbackType("during");
                    setRt(bayDetails[0].pushback_instruction_1_radio); // default RT
                    //setAnimationImage(bayDetails[0].app_pushback_instruction_1_animation);
                    setInstructionImage(bayDetails[0].pushback_instruction_1_during_image)
                    let imageRender = window.location.origin+constants.MEDIA_PATH+bayDetails[0].pushback_instruction_1_during_image+".png";
                    //let imageRender = "https://uat-ao.changiairport.com/"+constants.MEDIA_PATH+bayDetails[0].pushback_instruction_1_during_image+".png";
                    checkImage(imageRender, function(){ setPbImage(imageRender); console.log("image available"); setDisplayMainImage(true); setDisplayNoneImage(false)}, function(){ setPbImage(""); console.log("image not available");setDisplayMainImage(false); setDisplayNoneImage(true)} );

                    let animationImageRender = window.location.origin+constants.MEDIA_PATH+bayDetails[0].app_pushback_instruction_1_animation+".gif";
                    //let animationImageRender = "https://uat-ao.changiairport.com/"+constants.MEDIA_PATH+bayDetails[0].app_pushback_instruction_1_animation+".gif";
                    checkImageAnimation( animationImageRender, 
                        function() { 
                            console.log("image animation available"); 
                            setAnimationImage(animationImageRender); 
                            setAnimationImageFlag(true); 
                            //displayAnimation(); 
                        }, 
                        function() {
                            let pdfDetails = window.location.origin+constants.MEDIA_PATH+bayDetails[0].app_pushback_instruction_1_animation+".pdf";  
                            setAnimationImage(pdfDetails); 
                            console.log("image animation not available"); 
                            setAnimationImageFlag(false)
                        } 
                    );

                    setPbIndexSelected(1);
                    setPBDescription(bayDetails[0]["pushback_instruction_1_during_description"]);
                }

                //Get recommendation next or next before bay
                if(bayDetails.length > 0) {


                    let bayLength = pbRestrictionMapping.data.length - 1;
                    let bayIndex = 0;
                    let bayIndexNext = 0;
                    let bayIndexPrev = 0;

                    pbRestrictionMapping.data.map((data,index) => {

                        if(data.bay == bayDetails[0].bay) {
                            bayIndex = index;
                            
                        }
                    });


                    setCurrentBayIndex(bayIndex);

                    //If current index is the first index
                    if(bayIndex == 0) {
                        bayIndexNext = bayIndex + 1;
                        setNextBayIndex(bayIndexNext);
                        setPrevBayIndex(bayLength);     
                    } else if(bayIndex == bayLength) {
                    //If current index is the last index
                        bayIndexNext = bayIndex + 1;
                        bayIndexPrev = bayIndex - 1 ;
                        setNextBayIndex(0);
                        setPrevBayIndex([bayIndexPrev]);
                    } else {
                    //if Currnet index is in the range
                        bayIndexNext = bayIndex + 1;
                        bayIndexPrev = bayIndex - 1 ;
                        setNextBayIndex(bayIndexNext);
                        setPrevBayIndex(bayIndexPrev);
                    }



                }
            } catch (error) {
                //console.log(error);
            }

        }
    }

    const setPushbackTypeChange = (value) => {
        setPushbackType(value);
        if(value == "during") {
            let imageRender = window.location.origin+constants.MEDIA_PATH+currentMappingData[0]["pushback_instruction_"+pbIndexSelected+"_during_image"]+".png";
            checkImage(imageRender, function(){ setPbImage(imageRender); console.log("image available"); setDisplayMainImage(true); setDisplayNoneImage(false)}, function(){ setPbImage(""); console.log("image not available");setDisplayMainImage(false); setDisplayNoneImage(true)} );
            setPBDescription(currentMappingData[0]["pushback_instruction_"+pbIndexSelected+"_during_description"]);

        }
        if(value == "complete") {
            let imageRender = window.location.origin+constants.MEDIA_PATH+currentMappingData[0]["pushback_instruction_"+pbIndexSelected+"_after_image"]+".png";
            checkImage(imageRender, function(){ setPbImage(imageRender); console.log("image available"); setDisplayMainImage(true); setDisplayNoneImage(false)}, function(){ setPbImage(""); console.log("image not available");setDisplayMainImage(false); setDisplayNoneImage(true)} );
            setPBDescription(currentMappingData[0]["pushback_instruction_"+pbIndexSelected+"_after_description"]);
        }

        
    }

    const showInfoModal = () => {
        if (animationImageFlag) {
            setInfoModalVisibility(true);
        } else {
            setInfoModalVisibility(false);
            window.open(animationImage, "_blank");
        }
    };
    
    const closeModal = () => {
        setInfoModalVisibility(false);
    };

    const checkImage = (imageSrc, good, bad) => {
        var img = new Image();
        
        img.onload = good; 
        img.onerror = bad;
        img.src = imageSrc;

    }

    const checkImageAnimation = (imageSrc, good, bad) => {
        console.log("inside checkImageAnimation(): "+ imageSrc);
        var img = new Image();
        
        img.onload = good; 
        img.onerror = bad;
        img.src = imageSrc;

    }

    const displayImage = () => {
        return <img style={{width:"100%"}}  src={pbImage} />;
    }

    const displayEmptyImage = () => {
        return <Empty  description={'No image available'} style={{border: "1px solid white", padding:"20px"}} />;
    }

    const displayAnimation = () => {
        return (
            <img style={{width:"100%"}}  src={window.location.origin+constants.MEDIA_PATH+animationImage} />
        )
    }

    const customExpandIcon = (props) => {
        if (props.isActive) {
            return <span><CaretUpOutlined style={{width:"1em", height:"1em"}} /></span>
        } else {

            return <span><CaretDownOutlined style={{width:"1em", height:"1em"}} /></span>
        }
    }

    const quickSearchChange = (e,index,type) => {

    


        try {
    
          setqsVisibility(true);
          let value = "";
          if (type == "direct") {
            e.preventDefault();      
            value = e.target.value;
          } else {
            value = e;
          }
    
          let tempValue = value;
          let pbSorted = pbRestrictionMapping.data;
          let groupBay = [];
          let groupBayIndex2 = [];
          let groupBayIndex3 = [];
          let groupBayIndex4 = [];
      
          pbSorted.sort(GetSortOrder("bay"));
          pbSorted.sort(function(a, b) {
            return a.bay.localeCompare(b.bay, undefined, {
                numeric: true,
                sensitivity: 'base'
            });
          });
      
          pbSorted.sort(GetSortOrder("bay"));
          pbSorted.sort(function(a, b) {
            return a.bay.localeCompare(b.bay, undefined, {
                numeric: true,
                sensitivity: 'base'
            });
          });
      
          switch(index) {
            case '1':
              //Get bays
              let grouppier = groupBy(pbSorted, 'filter_location');
              
              let indexSearchValue1 = "";
              if (value == "2" || value == "3" || value == "4" || value == "5" || value == "6") {
                indexSearchValue1 = value+"00s";
              } else {
                indexSearchValue1 = value;
              }
      
              grouppier = grouppier[indexSearchValue1];
              grouppier.map( (index,data) => {
                groupBay.push(index.bay);
              });
      
              //Get second index of bays
              groupBay.map((data,index) => {
                //get the second char on the string
                groupBayIndex2.push(data.charAt(1));
              });
      
              groupBayIndex2 = uniqueArray(groupBayIndex2);
      
              setNumArraySecond(groupBayIndex2);
      
              if(value.includes("00s")) {
                tempValue = tempValue.replace("00s","");
              };
              
              setQuickSearch1(tempValue);
              setQuickSearch2("");
              setQuickSearch3("");
              setQuickSearch4("");
              setNumArrayThird([]);
              setNumArrayFourth([]);
      
              break;
            case '2':
              //Get bays
              let indexSearchValue = quickSearch1+value;
              let grouppier2 = groupBy(pbSorted, 'filter_location');
              let tempGroupBayIndex3 = [];
              
              if (quickSearch1 == '2' || quickSearch1 == '3' || quickSearch1 == '4' || quickSearch1 == '5' || quickSearch1 == '6' ) {
                grouppier2 = grouppier2[quickSearch1+"00s"];  
              } else {
                grouppier2 = grouppier2[quickSearch1]; 
              }
      
              grouppier2.map( (index,data) => {
                groupBay.push(index.bay);
              });
      
              //Get second index of bays
              groupBay.map((data,index) => {
                //combined first and second to check what possible for third
                if(data.includes(indexSearchValue)) {
                  tempGroupBayIndex3.push(data);
                };
              });
      
              tempGroupBayIndex3.map((data,index) => {
                let tempData = data.charAt(2);
      
                if (tempData != "") {
                  groupBayIndex3.push(data.charAt(2));
                }
              })
      
      
      
              groupBayIndex3 = uniqueArray(groupBayIndex3);
              setNumArrayThird(groupBayIndex3);
      
      
              setQuickSearch2(value);
              setNumArrayFourth([]);
              break;
      
            case '3':
              //Get bays
              let indexSearchValue3 = quickSearch1+quickSearch2+value;
              let grouppier3 = groupBy(pbSorted, 'filter_location');
              let tempGroupBayIndex4 = [];
      
              if (quickSearch1 == '2' || quickSearch1 == '3' || quickSearch1 == '4' || quickSearch1 == '5' || quickSearch1 == '6' ) {
                grouppier3 = grouppier3[quickSearch1+"00s"];  
              } else {
                grouppier3 = grouppier3[quickSearch1]; 
              }
      
              grouppier3.map( (index,data) => {
                groupBay.push(index.bay);
              });
      
              //Get third index of bays
              groupBay.map((data,index) => {
                //combined first and second and third to check what possible for third
                if(data.includes(indexSearchValue3)) {
                  tempGroupBayIndex4.push(data);
                };
              });
      
              tempGroupBayIndex4.map((data,index) => {
                let tempData = data.charAt(3);
                if (tempData != "") {
                  groupBayIndex4.push(data.charAt(3));
                }
              })
      
      
              groupBayIndex4 = uniqueArray(groupBayIndex4);
              setQuickSearch3(value);
              setNumArrayFourth(groupBayIndex4);
              break;
              
            case '4':
              setQuickSearch4(value);
              break;
            default:
              // code block
          }
      
          let searchValuesString = quickSearch1+quickSearch2+quickSearch3+quickSearch4;
          setSearchValue(searchValuesString);
        } catch (error) {
          
        }
    
    
    
      };
    
    const menu = () => {
    let letterArray = ["A","B","C","D","E","F","G","2","3","4","5","6"];
    let numArray = ["1","2","3","4","5","6","7","8","9","0"];
    let lastSeriesArray = ["L","R"];



    


    return (
        <Menu multiple={true} className={'menu-quick-search'}>
        <Menu.Item key="0">
            <Row span={24} >
                <Col span={6}>
                <Radio.Group onChange={(e) => quickSearchChange(e,'1','direct') } value={quickSearch1}>
                    {letterArray.map((data, index) =>
                    <Row key={index} type="flex" justify="space-between" align="middle" style={{textAlign:"center"}}>
                        <Radio className={'quickOption'+data} value={data}>{(data.includes("00s") ? data.replace("00s","") : data)}</Radio>
                    </Row>
                    )}
                </Radio.Group>
                </Col>

                <Col span={6}>
                <Radio.Group onChange={(e) => quickSearchChange(e,'2','direct') } value={quickSearch2}>
                    {numArraySecond.map((data, index) =>
                    <Row key={index} type="flex" justify="space-between" align="middle" style={{textAlign:"center"}}>
                        <Radio value={data}>{data}</Radio>
                    </Row>
                    )}
                </Radio.Group>
                </Col>

                <Col span={6}>
                <Radio.Group onChange={(e) => quickSearchChange(e,'3','direct') } value={quickSearch3}>
                    {numArrayThird.map((data, index) =>
                    <Row key={index} type="flex" justify="space-between" align="middle" style={{textAlign:"center"}}>
                        <Radio value={data}>{data}</Radio>
                    </Row>
                    )}
                </Radio.Group>
                </Col>

                <Col span={6}>
                <Radio.Group onChange={(e) => quickSearchChange(e,'4','direct') } value={quickSearch4}>
                    {numArrayFourth.map((data, index) =>
                    <Row key={index} type="flex" justify="space-between" align="middle" style={{textAlign:"center"}}>
                        <Radio value={data}>{data}</Radio>
                    </Row>
                    )}
                </Radio.Group>
                </Col>

            </Row>
        </Menu.Item>
        </Menu>
    )
    }

    const GetSortOrder = (prop) => {  
        return function(a, b) {  
            if (a[prop] > b[prop]) {  
                return 1;  
            } else if (a[prop] < b[prop]) {  
                return -1;  
            }  
            return 0;  
        }  
      }

      const handleClick = (e) => {
        try {
          if(e.target){
            if (node.current.contains(e.target)) {
              return;
            } else {
              setqsVisibility(false);
            }
          }
        } catch (error) {
          
        }
      }

    if (userInfo.length > 0) {

        return userInfo[0] == "atco" ? 
        (            
            <React.Fragment>
                <Content>
                    <div className="Appbar">
                        <Row type="flex" justify="space-between" align="middle">
                            <Col xs={{order:1, span:6}} md={{order:1, span:6}}>

                                {/* <img src={icon_homepbr} style={{color:"#C7A481"}} onClick={ () =>
                                    window.open(
                                        //constants.CHANGIGMC_URL + pbdata.bay.toLowerCase()
                                        "#/way2go/pbrhome",
                                        "_self"
                                      )
                                } /> */}
                                
                            </Col>
                            <Col xs={{order:3, span:24}} md={{order:2, span:10}} className="gmc-title">
                                <Row justify="space-around" align="middle">
                                    <Col xs={20} sm={16} md={12} lg={8} xl={4} align="middle">
                                            {bayNumber}
                                        </Col>
                                    </Row>
                            </Col>
                            <Col xs={{order:2, span:18}} md={{order:3, span:8}} style={{textAlign:"right"}}>
                                {/* <AutoComplete
                                    style={{ maxWidth: "200px", height: 31, marginBottom: 10}}
                                    options={SearchOptions}
                                    onSelect={value => searchOptionChange(value.toUpperCase())}
                                    backfill={false}
                                >
                                    <Input.Search onChange={e => searchBay(e.target.value)} onPressEnter={e => searchEnter(e.target.value)} placeholder="Search for bay"  />
                                </AutoComplete> */}
                                <div ref={node}>
                                <Dropdown visible={qsVisibility} overlayClassName={'qsSearchDropdown'} overlay={menu} trigger={['click']}>
                                    <Input.Search
                                        size="small"
                                        placeholder="Quick Search"
                                        suffix={(searchValue != "") ? <EnterOutlined onClick={ (value) => reInitializeDisplay(value)  }/> : <EnterOutlined/> }
                                        className={'qsSearch'}
                                        value={searchValue}
                                        onChange={(e) => setSearchValue(e.target.value) }
                                        onClick={e => {e.preventDefault(); setqsVisibility(true)}}
                                        onSearch={value => (value != '') ? reInitializeDisplay(value) : '' }
                                        style={{
                                            width: 200,
                                            backgroundColor: `${variables.appBackgroundColor}`,
                                        }}
                                    />
                                </Dropdown>
                                </div>
                            </Col>
                        </Row>
                    </div>
                    {(hasResult > 0) && (
                        <div className="gmc-main-content">
                            <Row type="flex" style={{ marginTop: 30 }}>
                                <Col xs={{span:24, order:1}} md={{span:12, order:1}} style={{marginBottom:20}}>
                                    <Row type="flex">
                                        {pbInstruction1 && (
                                            <Col span={8}>
                                                <Row>
                                                    <Button
                                                        size="large"
                                                        className={pbInstructionBtn1}
                                                        onClick={() => pbButtonChange(1, pbInstruction1) }
                                                    >
                                                        {pbInstruction1}
                                                    </Button>
                                                </Row>
                                            </Col>
                                        )}
                                        {pbInstruction2 && (
                                            <Col span={8}>
                                                <Row>
                                                    <Button
                                                        size="large"
                                                        className={pbInstructionBtn2}
                                                        onClick={() => pbButtonChange(2, pbInstruction2) }
                                                    >
                                                        {pbInstruction2}
                                                    </Button>
                                                </Row>
                                            </Col>
                                        )}
                                        {pbInstruction3 && (
                                            <Col span={8}>
                                                <Row>
                                                    <Button
                                                        size="large"
                                                        className={pbInstructionBtn3}
                                                        onClick={() => pbButtonChange(3, pbInstruction3) }
                                                    >
                                                        {pbInstruction3}
                                                    </Button>
                                                </Row>
                                            </Col>
                                        )}
        
                                    </Row>
                                </Col>
        
                                <Col xs={{span:24,order:2}} md={{span:12, order:2}} style={{marginBottom:20}}>
                                    <Row type="flex" justify="end">
                                        <Radio.Group
                                            buttonStyle="solid"
                                            defaultValue={pushbackType}
                                            value={pushbackType}
                                            onChange={e => setPushbackTypeChange(e.target.value)}
                                        >
                                            <Radio.Button value="during">During</Radio.Button>
                                            <Radio.Button value="complete">After</Radio.Button>
                                        </Radio.Group>
                                    </Row>
                                </Col>
                            </Row>
                            <Row type="flex" style={{ marginTop: 20 }}>
                                <Col span={24} style={{ textAlign:"center", fontWeight: 500, fontSize: 30, lineHeight: "35px", textAlign: "center", letterSpacing: "0.02em"}}>
                                        {selectedPusback} | {(pushbackType == "during") ? "PUSHBACK DURING" : "PUSHBACK AFTER"}
                                </Col>
                            </Row>
                            <Row type="flex" style={{ marginTop: 20 }}>
                                <Col xs={24} md={24}>
                                    {displayNoneImage && (
                                        displayEmptyImage()
                                    )}
        
                                    {displayMainImage && (
                                        displayImage()
                                    )}
                                    
                                </Col>
                            </Row>
                            <Row type="flex" style={{ marginTop: 25 }}>
                                <Col xs={24} md={18} >
                                    
                                    <Row>
                                        <Col style={{  fontFamily: "Roboto", fontStyle: "normal", fontWeight: 500, fontSize: 18, lineHeight: "21px", textAlign: "left", letterSpacing: "0.02em", marginBottom:"40px" }}>
                                            {pbDescription}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col style={{  fontFamily: "Roboto", fontStyle: "normal", fontWeight: 500, fontSize: 25, lineHeight: "29px", textAlign: "left", letterSpacing: "0.02em", marginBottom:"40px" }}>
                                            RT: {radioTrans}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <Button type="link" className={""} onClick={showInfoModal} style={{ color: "#FFFFFF", fontWeight: 500, fontSize: 21, lineHeight: "25px", textAlign: "left", letterSpacing: "0.02em", marginBottom:"10px", paddingLeft:0 }}>
                                            {/* <img
                                                src={icon_anchor}
                                                alt="icon_anchor"
                                                style={{
                                                    marginRight: 7,
                                                    marginLeft: 7,
                                                    height: 12,
                                                    cursor: "pointer"
                                                }}
                                                /> */}
                                                  View pushback procedure
                                            </Button>
                                            
                                        </Col>
                                    </Row>
                                </Col>
                                <Col xs={24} md={6}>
                                    <Row>
                                        {/* <img style={{width:"100%"}} src={legendImage}/> */}
                                    </Row>
                                </Col>
                                {/* <Col xs={24} md={24} lg={24}>
                                    <Row type="flex">

                                        <Collapse
                                            defaultActiveKey={['1']}
                                            expandIconPosition={'right'}
                                            style={{width:"100%", backgroundColor:"#1B1C28"}}
                                            className={'collapse-description'}
                                            expandIcon={(props) => customExpandIcon(props)}
                                        >
                                            <Panel header="" key="1" style={{backgroundColor:"#1B1C28"}}>
                                                <Row span={24}>
                                                    <Col span={10}>
                                                        {(setAnimationImageFlag == 'true' || setAnimationImageFlag == true) ? (<img src={animationImage}/>) : (<Empty  description={'No image available'} style={{border: "1px solid white", padding:"20px"}} />)} 
                                                    </Col>
                                                    <Col span={14} style={{fontFamily: "Roboto", fontStyle: "normal", fontWeight: "normal", fontSize: "18px", lineHeight: "21px", letterSpacing: "0.05em",color: "#FFFFFF",}}>
                                                        <div>{'The aircraft (on idle thrust) shall be pushed back onto TWY U1 to face North until its nosewheel is at the intersection of the aircraft stand lead-in line and TWY U1 centreline. The aircraft shall then be towed forward until its nosewheel is at the intersection of aircraft stand B9 lead-in line and TWY U1 centreline. The aircraft may breakaway from there.'}</div>
                                                    </Col>
                                                </Row>
                                                
                                            </Panel>
                                        </Collapse>
                                    </Row>
                                </Col> */}
                            </Row>
        
                            <div className="App-Modal-Info">
                                <Modal
                                    className={"gmcPushbackModal"}
                                    title=""
                                    footer=""
                                    visible={modalVisibility}
                                    onOk={''}
                                    onCancel={ () => closeModal()}
                                >
                                    <Row type="flex" style={{ marginTop: 10 }}>
                                        <Col xs={24} md={24}>
                                            {modalVisibility && (
                                                <img style={{width:"100%"}}  src={animationImage} />
                                            )}
                                        </Col>
                                    </Row>
        
                                </Modal>
                            </div>
                        </div>
                    )}
                </Content>
                </React.Fragment>
        
        ) : (

            <Login />
        )

    } else {
        return (
            <Login />
        );
    }   

}

export default GMC;
