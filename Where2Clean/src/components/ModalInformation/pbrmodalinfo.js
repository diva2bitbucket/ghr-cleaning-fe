import React from "react";
import { Row, Col, Button, Icon, Modal, Input} from "antd";
import { SearchOutlined, EnterOutlined } from "@ant-design/icons";
// import selectionFilterImage from "../../assets/images/selection_filter.jpg";

const PBRModalInfo = props => {

    return (
    <div className="App-Modal-Info">
        <Modal
            className={"infoAtcoModal pbrhomemdal"}
            title=""
            footer=""
            visible={props.infoModalVisibility}
            onOk={''}
            onCancel={ () => {
                props.closeAtcoInfoModal()
            }}
        >     
            <Row>
                <Col span={11}>
                    <Button
                    size="large"
                    className={"button-filterbygmc-infomodal"}
                    >
                        FILTER BY GMC
                    </Button>
                </Col>
                <Col span={13}>
                    <div className="button-driver-infomodal-text">
                        Select one or more GMCs to filter.
                    </div>
                </Col>
            </Row>

            <Row>
                <Col span={11}>
                    <Button
                    size="large"
                    className={"button-filterbylocation-infomodal"}
                    >
                        FILTER BY GROUPS
                    </Button>
                </Col>
                <Col span={13}>
                    <div className="button-atc-infomodal-text">
                        Select one or more groups of bays to filter.
                    </div>
                </Col>
            </Row>

            <Row style={{marginBottom:"20px"}}>
                <Col span={11}>
                    <Input.Search
                    size="small"
                    placeholder="Quick Search"
                    suffix={<EnterOutlined/>}
                    className={'qsSearch-modal'}
                    value={''}
                    style={{
                        width: 200,
                        /* backgroundColor: `${variables.appBackgroundColor}`, */
                    }}
                  />
                </Col>
                <Col span={13}>
                    <div className="button-match-infomodal-text">
                        Tap on search bar to select bays based on dropdown. Click on enter icon to search.
                    </div>
                </Col>
            </Row>
            
            <Row>
                <Col span={11}>
                    {/* <img src={selectionFilterImage} /> */}
                </Col>
                <Col span={13}>
                <div className="button-required-infomodal-text" style={{marginTop:"60px"}}>
                    Bays are grouped with  vertical bars on the left.
                </div>
                </Col>
            </Row>
            
        </Modal>
    </div>
    );
};

export default PBRModalInfo;
