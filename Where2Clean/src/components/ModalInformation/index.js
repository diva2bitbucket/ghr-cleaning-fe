import React from "react";
import { Row, Col, Button, Icon, Modal, Divider, Input, Select } from "antd";
import { CaretRightOutlined, SearchOutlined, CaretDownOutlined } from "@ant-design/icons";
import "./modal.scss";
const icon_arr_svg = require("../../assets/images/arr-icon.svg");
const icon_dep_svg = require("../../assets/images/dep-icon.svg");


const ModalInformation = props => {

    return (
        <div className="App-Modal-Info">
            <Modal
                className={"infoAtcoModal"}
                title=""
                footer=""
                visible={props.infoModalVisibility}
                onOk={''}
                onCancel={() => {
                    props.closeAtcoInfoModal()
                }}
            >
                <Row align="middle" className="tableHeader">
                    <Col span={3} className="colHeader" align="center">Gate</Col>
                    <Col span={5} className="colHeader">Flight Info</Col>
                    <Col span={3} className="colHeader"> Arr/Dep</Col>
                    <Col span={5} className="colHeader"> Free from</Col>
                    <Col span={3} className="colHeader">Free till</Col>
                    <Col span={4} className="colHeader">Time left</Col>
                </Row>
                <Row align="middle" className="box">
                    <Col span={3} align="center" >
                        <Row align="center" className="tableText colUpperText">Gate</Row>
                        <Row align="center" className="tableText colText">Call sign</Row>
                    </Col>
                    <Divider className="vDivider" type="vertical" />
                    <Col span={4} align="center" >
                        <Row align="center" className="tableText colUpperText">Flight #</Row>
                        <Row align="center" className="tableText colText">Aircraft size</Row>
                    </Col>
                    <Divider className="vDivider" type="vertical" />
                    <Col span={3} className="tableText colText">
                        Arriving or Departing Flight
                    </Col>
                    <Divider className="vDivider" type="vertical" />
                    <Col span={3} className="tableText colText">
                        Gate will be free from this time
                    </Col>
                    <Divider className="vDivider" type="vertical" />
                    <Col span={3} className="tableText colText">
                        Gate is free until this time
                    </Col>
                    <Divider className="vDivider" type="vertical" />
                    <Col span={4} className="tableText colText">
                        Time left to clean gate
                    </Col>
                </Row>
                <Row align="middle" className="box-mobile">
                    <Col span={2} align="center" style={{ marginLeft: "9px" }} >
                        <Row align="center" className="tableText colUpperText">Gate</Row>
                        <Row align="center" className="tableText colText">Call sign</Row>
                    </Col>
                    <Divider className="vDivider" type="vertical" />
                    <Col span={3} align="center" >
                        <Row align="center" className="tableText colUpperText">Flight #</Row>
                        <Row align="center" className="tableText colText">Aircraft size</Row>
                    </Col>
                    <Divider className="vDivider" type="vertical" />
                    <Col span={3} className="tableText colText">
                        ARR or DEP Flight
                    </Col>
                    <Divider className="vDivider" type="vertical" />
                    <Col span={3} className="tableText colText">
                        Gate is free from this time
                    </Col>
                    <Divider className="vDivider" type="vertical" />
                    <Col span={3} className="tableText colText">
                        Gate is free until this time
                    </Col>
                    <Divider className="vDivider" type="vertical" />
                    <Col span={2} className="tableText colText">
                        Time left to clean
                    </Col>
                </Row>

                <Row align="middle" className="box-mobile-after">
                    <Col span={7} style={{ display: "flex", justifyContent: "space-between" }}>
                        <div style={{ background: "#5348FF", width: "6px", height: "51px" }}></div>
                        <img
                            src={icon_arr_svg}
                            alt="icon_arr"
                            className="info-icon-dep"
                        // style={{
                        //     marginTop: "auto",
                        //     marginBottom: "auto",
                        //     position: "relative",
                        //     top: 15,
                        //     width: "53px",
                        //     height: "53px"
                        // }}
                        />
                    </Col>
                    <Col span={4}>

                    </Col>
                    <Col span={13}>
                        <div className="button-atc-infomodal-text">
                            Arriving Flight
                    </div>
                    </Col>
                </Row>
                <Row align="middle">
                    <Col span={7} style={{ display: "flex", justifyContent: "space-between" }}>
                        <div style={{ background: "#FFB928", width: "6px", height: "51px" }}></div>
                        <img
                            src={icon_dep_svg}
                            alt="icon_dep"
                            className="info-icon-dep"
                        // style={{
                        //     marginTop: "auto",
                        //     marginBottom: "auto",
                        //     position: "relative",
                        //     top: 12,
                        //     width: "53px",
                        //     height: "53px"
                        // }}
                        />
                    </Col>
                    <Col span={4}>

                    </Col>
                    <Col span={13}>
                        <div className="button-atc-infomodal-text">
                            Departing Flight
                    </div>
                    </Col>
                </Row>

                <Row align="middle">
                    <Col span={11} align="center">
                        <Button
                            size="large"
                            className="btn-available-infomodal"
                            disabled
                        >
                            AVAILABLE <CaretRightOutlined style={{ float: "right" }} />
                        </Button>
                    </Col>
                    <Col span={13}>
                        <div className="button-atc-infomodal-text">
                            Area is not cleaned and is currently available for cleaning. <span className="boldText">Tap to start cleaning.</span>
                        </div>
                    </Col>
                </Row>

                <Divider className="modal-divider" />

                <Row align="middle">
                    <Col span={11} align="center">
                        <Button
                            size="large"
                            className="btn-cleannow-infomodal"
                            disabled
                        >
                            <div style={{paddingLeft:"10px"}}>CLEAN NOW </div> <CaretRightOutlined style={{ float: "right" }} />
                        </Button>
                    </Col>
                    <Col span={13}>
                        <div className="button-atc-infomodal-text">
                            Area was not cleaned for a previous flight and has been prioritised. <span className="boldText">Tap to start cleaning.</span>
                        </div>
                    </Col>
                </Row>

                <Divider className="modal-divider" />

                <Row align="middle">
                    <Col span={11} align="center">
                        <Button
                            size="large"
                            className="btn-cleaning-infomodal"
                            disabled
                        >
                            <div style={{textAlign:"center"}} > CLEANING  </div> <CaretRightOutlined style={{ float: "right" }} /> 
                        </Button>
                    </Col>
                    <Col span={13}>
                        <div className="button-match-infomodal-text">
                            Area is currently being cleaned. <span className="boldText">Tap when cleaning is completed </span> to mark as cleaned.
                        </div>
                    </Col>
                </Row>
                <Divider className="modal-divider" />

                <Row align="middle">
                    <Col span={11} align="center">
                        <Col span={23}>
                            <Button
                                size="large"
                                className="btn-unavail-infomodal"
                                disabled
                            >
                                UNAVAIL. <CaretRightOutlined style={{ display: "flex", justifyContent: "flex-end" }} />
                            </Button>
                        </Col>
                    </Col>
                    <Col span={13}>
                        <div className="button-required-infomodal-text">
                            Area is unavailable for cleaning. Tap to see the reason for unavailability.
                    </div>
                    </Col>
                </Row>
                <Divider className="modal-divider" />
                <Row align="middle" className="modal-last-item1">
                    <Col span={8} align="center" >
                        <Row align="center" className="term-complete">COMPLETE</Row>
                        <Row align="center" className="term-date" >
                            05/06/2020 15:00
                        </Row>
                    </Col>
                    <Col span={3}>

                    </Col>
                    <Col span={13}>
                        <div className="button-driver-infomodal-text">
                            Cleaning completed for area. Time and date of completion.
                        </div>
                    </Col>
                </Row>
                {/* mobile */}
                <Row align="middle" className="modal-last-item2">
                    <Col span={11} align="center" >
                        <Row align="center" className="term-complete">COMPLETE</Row>
                        <Row align="center" className="term-date" >
                            05/06/2020 15:00
                        </Row>
                    </Col>
                    <Col span={13}>
                        <div className="button-driver-infomodal-text">
                            Cleaning completed for area. Time and date of completion.
                        </div>
                    </Col>
                </Row>


                <Divider className="modal-divider" />
                <Row align="middle">
                    <Col span={11} align="center">
                        <Row>
                            <Col span={23} align="center">
                                <div style={{ padding: "8px 0px" }}>
                                    <Input className="input-box" placeholder="Quick Search" disabled="true"
                                        suffix={<SearchOutlined />} />
                                </div>
                            </Col>
                        </Row>
                    </Col>
                    <Col span={13}>
                        <div className="button-required-infomodal-text">
                            Tap on bar and type in gate or flight number to search.
                    </div>
                    </Col>

                </Row>
                <Divider className="modal-divider" />

                <Row>
                    <Col span={11} align="center">
                        <Row>
                            <Col span={23} align="center">
                                <div style={{ padding: "8px 0px" }}>
                                    <Input
                                        suffix={<CaretDownOutlined
                                            style={{ fontSize: 15 }} />}
                                        className="modalInputSelect"
                                        disabled="true"
                                        defaultValue="Filter by Status"
                                    />
                                </div>
                            </Col>
                        </Row>
                    </Col>
                    <Col span={13}>
                        <div className="button-required-infomodal-text">
                            Filter based on <span className="boldText">ALL, ALL AVAILABLE </span> and <span className="boldText">TO BE CLEANED</span>
                        </div>
                    </Col>
                </Row>

                {/* <Row>
                    <Col span={11} align="center">
                        <Row>
                            <Col span={23} align="center">
                                <div style={{ padding: "8px 0px" }}>
                                    <Input
                                        suffix={<CaretDownOutlined
                                            style={{ fontSize: 15 }} />}
                                        className="modalInputSelect"
                                        disabled="true"
                                        defaultValue="ALL AVAILABLE"
                                    />
                                </div>
                            </Col>
                        </Row>
                    </Col>
                    <Col span={13}>
                        <div className="button-required-infomodal-text">
                            Display only gates available for cleaning
                        </div>
                    </Col>
                </Row> */}

                {/* <Row>
                    <Col span={11} align="center">
                        <Row>
                            <Col span={23} align="center">
                                <div style={{ padding: "8px 0px" }}>
                                    <Input
                                        suffix={<CaretDownOutlined
                                            style={{ fontSize: 15 }} />}
                                        className="modalInputSelect"
                                        disabled="true"
                                        defaultValue="TO BE CLEANED"
                                    />
                                </div>
                            </Col>
                        </Row>
                    </Col>
                    <Col span={13}>
                        <div className="button-required-infomodal-text">
                            Default filter. Includes <span className="boldText">Cleaning, Available, Unavailable</span>
                        </div>
                    </Col>
                </Row> */}
                <Divider className="modal-divider" />
                <Row align="middle" className="modal-last-item1">
                    <Col span={8} align="center" className="term">Aircraft size</Col>
                    <Col span={3}>

                    </Col>
                    <Col span={13}>
                        <div className="button-required-infomodal-text">
                            ‘S’, ‘M’, ‘L’ and ‘XL’ indicates the sizes of the aircraft
                        </div>
                    </Col>
                </Row>

                {/* mobile */}
                <Row align="middle" className="modal-last-item2">
                    <Col span={11} align="center" className="term">Aircraft size</Col>
                    <Col span={13}>
                        <div className="button-required-infomodal-text">
                            ‘S’, ‘M’, ‘L’ and ‘XL’ indicates the sizes of the aircraft
                        </div>
                    </Col>
                </Row>
                {/* <Row align="middle">
                    <Col span={6} align="center" className="term">Free from</Col>
                    <Col span={18}>
                        <div className="button-required-infomodal-text">
                            For ARR flights, Actual Time of Arrival + 35 min <br />
                            For DEP flights, Actual Time of Departure + 15 min
                        </div>
                    </Col>
                </Row>
                <Row align="middle">
                    <Col span={6} align="center" className="term">Free till</Col>
                    <Col span={18}>
                        <div className="button-required-infomodal-text">
                            For Arrrival and Departure flights,
                            100 min before Scheduled Time of Departure
                        </div>
                    </Col>
                </Row>
                <Row align="middle">
                    <Col span={6} align="center" className="term">Time left</Col>
                    <Col span={18}>
                        <div className="button-required-infomodal-text">
                            Length of time between ‘Free till’ and current time
                        </div>
                    </Col>
                </Row> */}

            </Modal>
        </div>
    );
};

export default ModalInformation; 
