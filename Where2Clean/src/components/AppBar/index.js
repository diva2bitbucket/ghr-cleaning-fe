import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import { Row, Col, Icon, Button, Menu, Drawer, Select } from "antd";
import { setLanguage } from "../../store/session";
import { useDispatch, useSelector } from "react-redux";
const { Option } = Select;

const AppBar = props => {
  const lang = useSelector(state => state.session.lang);
  const [drawerState, setDrawerState] = useState(false);
  const [selectedKey, setSelectedKey] = useState("");
  const dispatch = useDispatch();

  useEffect(() => {
    switch (props.location.pathname) {
      case "/":
        setSelectedKey("home");
        break;
      case "/login":
        setSelectedKey("login");
        break;
      case "/tests":
        setSelectedKey("tests");
        break;

      default:
        break;
    }
  }, [props.location.pathname]);

  return (
    <React.Fragment>
      <CustomFooter setDrawerState={setDrawerState} />
      <Drawer
        placement="right"
        closable={false}
        onClose={() => setDrawerState(false)}
        visible={drawerState}
      >
        <div style={{ padding: 10 }}>
          <span style={{ marginRight: 10 }}>Language/语言:</span>
          <Select
            style={{ width: 100 }}
            size="small"
            defaultValue={lang}
            onChange={value => {
              dispatch(setLanguage(value));
            }}
          >
            <Option value="en">English</Option>
            <Option value="zh">中文</Option>
          </Select>
        </div>

        <Menu mode="inline" selectedKeys={selectedKey}>
          <Menu.Item
            key="home"
            onClick={() => {
              setDrawerState(false);
              props.history.push("/");
            }}
          >
            <Icon type="home" />
            <span>Home</span>
          </Menu.Item>
        </Menu>
      </Drawer>
    </React.Fragment>
  );
};

const CustomFooter = props => (
  <Row type="flex" className="Appbar" justify="end">
    <Col style={{ marginRight: 20 }}>
      <Button
        type="link"
        onClick={() => props.setDrawerState(true)}
        icon="menu"
        style={{ fontSize: 26, color: "white" }}
      />
    </Col>
  </Row>
);

export default withRouter(AppBar);
