import React from "react";
import ChangiLogo from "../ChangiLogo";
import { Spin } from "antd";

const SplashScreen = props => {
  return (
    <div className="App-layout">
      <div style={{position: 'fixed', top: '40%'}}>
        <ChangiLogo style={{ height: 40, marginBottom: 40 }} type="dark" /><br/>
        <Spin size="large" />
      </div>
    </div>
  );
};

export default SplashScreen;
