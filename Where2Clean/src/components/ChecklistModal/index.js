import React, {useState, useEffect, useRef} from "react";
import { Row, Col, Button, Icon, Modal, Divider, Input, Select, Checkbox} from "antd";
import { CaretRightOutlined, SearchOutlined, CaretDownOutlined } from "@ant-design/icons";
import "./styles.scss";
import moment from "moment";
import { wsConnect, wsDisconnect, wsSend } from "../../store/pushback";
import constants from "../../configs/constants";
import { useSelector, useDispatch } from "react-redux";
import { getAllByTestId } from "@testing-library/react";
import checklistJson from "../../assets/data/checklist_per_terminal.json";


const ChecklistModal = props => {
    const [wallFinishesGoodCondition, setWallFinishesGoodCondition] = useState("")
    const [ceilingFinishesGoodCondition, setCeilingFinishesGoodCondition] = useState("")
    const [floorFinishesGoodCondition, setFloorFinishesGoodCondition] = useState("")
    const [waterCoolerGoodCondition, setWaterCoolerGoodCondition] = useState("")
    const [seatsGoodCondition, setSeatsGoodCondition] = useState("")
    const [binsGoodCondition, setBinsGoodCondition] = useState("")
    const [downRampGoodCondition, setDownRampGoodCondition] = useState("")
    const [otherFixturesGoodCondition, setOtherFixturesGoodCondition] = useState("")
    const [handRailingGoodCondition, setHandRailingGoodCondition] = useState("")
    const [airconGoodCondition, setAirconGoodCondition] = useState("")

    const [checkboxCheck, setCheckboxCheck]=useState(false)
    const [allNeedsCleaning, setAllNeedsCleaning] = useState(false)

    const [itemChecklist, setItemChecklist] = useState([])

    const dispatch = useDispatch()
    const user = useSelector((state) => state.session.user);

    //getting the cleaning data from the redux store
    const cleaningData = useSelector((state) => state.session.cleaningData);
    const [outputChecklist, setOutputChecklist] = useState()
    
    // console.log("PROPS TERMINAL", props.terminal);
    const [wallTick,setWallTick] = useState(false)
    const [floorTick,setFloorTick] = useState(false)
    const [ceilingTick,setCeilingTick] = useState(false)
    const [waterCoolerTick,setWaterCoolerTick] = useState(false)
    const [binsTick,setBinsTick] = useState(false)
    const [downRampTick,setDownRampTick] = useState(false)
    const [seatTick,setSeatTick] = useState(false)
    const [cleanAllTick,setCleanAllTick] = useState(false)
    const [otherFixturesTick, setOtherFixturesTick] = useState(false)
    const [airconTick, setAirconTick] = useState(false)
    const [handRailTick, setHandRailTick] = useState(false)
    // const [dataList, setDataList] = useState([])
    const [checklist, setChecklist] = useState()


    let dataList = [];
    
    let arr = []

    useEffect(()=>{

   
    if(checklistJson){
        let checklistType = checklistJson.map((e)=>{
            let checklistItem = {}
            
            if(e.direction){
                checklistItem.direction = e.direction
            }

            if(e.terminal){
                checklistItem.terminal = e.terminal
            }

            if(e.checklist_item){
                checklistItem.checklist_item = e.checklist_item
            }
            if(e.field_mapping){
                checklistItem.field_mapping = e.field_mapping
            }
            return checklistItem
        })
        setChecklist(checklistType)
    }
    },[checklistJson])
    
    useEffect(()=>{
        let itemArr= []
        let itemArrName =[]
        if(props.terminal && props.direction){
            // console.log("terminal and direction:")
            // console.log(props.terminal)
            // console.log(props.direction)
        // if(checklist){
            for(var i = 0; i< checklist.length ; i++){
                if(props.terminal == checklist[i].terminal && props.direction == checklist[i].direction){
                    itemArr.push(checklist[i].checklist_item)
                    itemArrName.push(checklist[i].field_mapping)
                }
            }
            // console.log("items::")
            // console.log(itemArr)
        // }

        }

        var output = {}
        if(itemArr){
            for(var i =0; i<itemArr.length;i++){
                output[itemArrName[i]]=itemArr[i]
            }
 
        }
        setOutputChecklist(output)
        
        
        // console.log("checking checklist of flight id")
        // console.log(item)
    },[props.terminal, props.direction,checklist])
    
    

    const checkWallFinishes = (e)=>{
        if(e.target.checked){
            setWallTick(true)
            setWallFinishesGoodCondition("TRUE")
        }
        else{
            setWallTick(false)
            setWallFinishesGoodCondition("FALSE")
        }

    }
    const checkCeilingFinishes = (e)=>{
        if(e.target.checked){
            setCeilingTick(true)
            setCeilingFinishesGoodCondition("TRUE")
        }
        else{
            setCeilingTick(false)
            setCeilingFinishesGoodCondition("FALSE")
        }

    }
    const checkFloorFinishes = (e)=>{
        if(e.target.checked){
            setFloorTick(true)
            setFloorFinishesGoodCondition("TRUE")
        }
        else{
            setFloorTick(false)
            setFloorFinishesGoodCondition("FALSE")
        }

        
    }
    const checkWaterCooler = (e)=>{
        if(e.target.checked){
            setWaterCoolerTick(true)
            setWaterCoolerGoodCondition("TRUE")
        }
        else{
            setWaterCoolerTick(false)
            setWaterCoolerGoodCondition("FALSE")
        }
    }
    const checkSeats = (e)=>{
        if(e.target.checked){
            setSeatTick(true)
            setSeatsGoodCondition("TRUE")
        }
        else{
            setSeatTick(false)
            setSeatsGoodCondition("FALSE")
        }
    }
    const checkBins = (e)=>{
        if(e.target.checked){
            setBinsTick(true)
            setBinsGoodCondition("TRUE")
        }
        else{
            setBinsTick(false)
            setBinsGoodCondition("FALSE")
        }
    }
    const checkDownRamp = (e)=>{
        if(e.target.checked){
            setDownRampTick(true)
            setDownRampGoodCondition("TRUE")
        }
        else{
            setDownRampTick(false)
            setDownRampGoodCondition("FALSE")
        }

    }
    const checkOtherFixtures = (e)=>{
        if(e.target.checked){
            setOtherFixturesTick(true)
            setOtherFixturesGoodCondition("TRUE")
        }
        else{
            setOtherFixturesTick(false)
            setOtherFixturesGoodCondition("FALSE")
        }

    }

    const checkAircon = (e)=>{
        if(e.target.checked){
            setAirconTick(true)
            setAirconGoodCondition("TRUE")
        }
        else{
            setAirconTick(false)
            setAirconGoodCondition("FALSE")
        }

    }
    const checkHandRail = (e)=>{
        if(e.target.checked){
            setHandRailTick(true)
            setHandRailingGoodCondition("TRUE")
        }
        else{
            setHandRailTick(false)
            setHandRailingGoodCondition("FALSE")
        }

    }
    
    const cleanAll = (e,direction)=>{
        if(e.target.checked){
            //uncheck checkboxes
            setWallTick(false)
            setWaterCoolerTick(false)
            setOtherFixturesTick(false)
            setBinsTick(false)
            setSeatTick(false)
            setDownRampTick(false)
            setCeilingTick(false)
            setFloorTick(false)
            setHandRailTick(false)
            setAirconTick(false)

            setAllNeedsCleaning(true)
            setCleanAllTick(true)
            // console.log("all needs cleaning is set True")
            
            if(direction == "ARR"){
                if (props.terminal == "4") {
                    setCeilingFinishesGoodCondition("FALSE");
                    setWallFinishesGoodCondition("FALSE");
                    setFloorFinishesGoodCondition("FALSE");
                    setHandRailingGoodCondition("FALSE");
                    setAirconGoodCondition("FALSE");
                    setDownRampGoodCondition("");
                    setBinsGoodCondition("");
                    setWaterCoolerGoodCondition("");
                    setSeatsGoodCondition("");
                    setOtherFixturesGoodCondition("");
                } else {
                    setCeilingFinishesGoodCondition("FALSE")
                    setWallFinishesGoodCondition("FALSE");
                    setFloorFinishesGoodCondition("FALSE");
                    setHandRailingGoodCondition("FALSE");
                    setOtherFixturesGoodCondition("FALSE");
                    setAirconGoodCondition("");
                    setDownRampGoodCondition("");
                    setBinsGoodCondition("");
                    setWaterCoolerGoodCondition("");
                    setSeatsGoodCondition("");
                    
                }   
                
            }
            else{
                //direction == DEP

                if(props.terminal == "1" || props.terminal == "2"){
                    setCeilingFinishesGoodCondition("FALSE")
                    setWallFinishesGoodCondition("FALSE");
                    setFloorFinishesGoodCondition("FALSE");
                    setHandRailingGoodCondition("FALSE");
                    setOtherFixturesGoodCondition("FALSE");
                    setDownRampGoodCondition("");
                    setBinsGoodCondition("");
                    setWaterCoolerGoodCondition("");
                    setSeatsGoodCondition("");
                    setAirconGoodCondition("");
                }
                else if(props.terminal == "3"){
                    setCeilingFinishesGoodCondition("FALSE");
                    setWallFinishesGoodCondition("FALSE");
                    setFloorFinishesGoodCondition("FALSE");
                    setHandRailingGoodCondition("FALSE")
                    setOtherFixturesGoodCondition("FALSE");
                    setDownRampGoodCondition("");
                    setBinsGoodCondition("");
                    setWaterCoolerGoodCondition("");
                    setSeatsGoodCondition("");
                    setAirconGoodCondition("");
                    
                }
                else if(props.terminal == "4"){
                    setWallFinishesGoodCondition("FALSE");
                    setFloorFinishesGoodCondition("FALSE");
                    setSeatsGoodCondition("FALSE");
                    setBinsGoodCondition("FALSE");
                    setDownRampGoodCondition("FALSE");
                    setCeilingFinishesGoodCondition("")
                    setOtherFixturesGoodCondition("")
                    setAirconGoodCondition("")
                    setHandRailingGoodCondition("")
                    }
            }
            
        }
        else{
            setCleanAllTick(false)
            setAllNeedsCleaning(false)

            setWallFinishesGoodCondition("")
            setFloorFinishesGoodCondition("")
            setCeilingFinishesGoodCondition("")
            setDownRampGoodCondition("")
            setBinsGoodCondition("")
            setWaterCoolerGoodCondition("")
            setSeatsGoodCondition("")
            setOtherFixturesGoodCondition("")
            setAirconGoodCondition("")
            setHandRailingGoodCondition("")
        }

    }
    
   

    useEffect(() =>{
        // if(props.direction){
            if(props.direction == "ARR"){
                if (props.terminal == "4") {
                    if(ceilingFinishesGoodCondition == ""){
                        setCeilingFinishesGoodCondition("FALSE");
                    }
                    if(wallFinishesGoodCondition == ""){
                        setWallFinishesGoodCondition("FALSE");
                    }
                    if(floorFinishesGoodCondition == ""){
                        setFloorFinishesGoodCondition("FALSE");
                    }
                    if(handRailingGoodCondition == ""){
                        setHandRailingGoodCondition("FALSE");
                    }
                    if(airconGoodCondition == ""){
                        setAirconGoodCondition("FALSE");
                    }
                } else {
                    if(ceilingFinishesGoodCondition == ""){
                        setCeilingFinishesGoodCondition("FALSE");
                    }
                    if(wallFinishesGoodCondition == ""){
                        setWallFinishesGoodCondition("FALSE");
                    }
                    if(floorFinishesGoodCondition == ""){
                        setFloorFinishesGoodCondition("FALSE");
                    }
                    if(handRailingGoodCondition == ""){
                        setHandRailingGoodCondition("FALSE");
                    }
                    if(otherFixturesGoodCondition == ""){
                        setOtherFixturesGoodCondition("FALSE");
                    }
                }
                
            }
            else {
                //direction == "DEP"
                
                if(props.terminal == "1" || props.terminal =="2"){
                    //if mapped items are not checked, return false
                    //if items that are not mapped are not checked, return ""
                    // console.log("set empty t1 properties as false(not good condition)")
                    if(ceilingFinishesGoodCondition == ""){
                        setCeilingFinishesGoodCondition("FALSE");
                    }
                    if(wallFinishesGoodCondition == ""){
                        setWallFinishesGoodCondition("FALSE");
                    }
                    if(floorFinishesGoodCondition == ""){
                        setFloorFinishesGoodCondition("FALSE");
                    }
                    if(handRailingGoodCondition == ""){
                        setHandRailingGoodCondition("FALSE");
                    }
                    if(otherFixturesGoodCondition == ""){
                        setOtherFixturesGoodCondition("FALSE");
                    }
                }
                else if(props.terminal == "3"){
                    if(ceilingFinishesGoodCondition == ""){
                        setCeilingFinishesGoodCondition("FALSE");
                    }
                    if(wallFinishesGoodCondition == ""){
                        setWallFinishesGoodCondition("FALSE");
                    }
                    if(floorFinishesGoodCondition == ""){
                        setFloorFinishesGoodCondition("FALSE");
                    }
                    if(handRailingGoodCondition == ""){
                        setHandRailingGoodCondition("FALSE");
                    }
                    if(otherFixturesGoodCondition == ""){
                        setOtherFixturesGoodCondition("FALSE");
                    }
                }
                else if(props.terminal == "4"){
                    if(wallFinishesGoodCondition == ""){
                        setWallFinishesGoodCondition("FALSE");
                    }
                    if(floorFinishesGoodCondition == ""){
                        setFloorFinishesGoodCondition("FALSE");
                    }
                    if(seatsGoodCondition == ""){
                        setSeatsGoodCondition("FALSE");
                    }
                    if(binsGoodCondition == ""){
                        setBinsGoodCondition("FALSE");
                    }
                    if(downRampGoodCondition == ""){
                        setDownRampGoodCondition("FALSE");
                    }
                }
            }
        // }
        
        

        
    },[props, wallFinishesGoodCondition,
        ceilingFinishesGoodCondition,
        floorFinishesGoodCondition,
        waterCoolerGoodCondition,
        seatsGoodCondition,
        binsGoodCondition,
        downRampGoodCondition,
        otherFixturesGoodCondition,
        handRailingGoodCondition,
        airconGoodCondition
    ])
    const cancelSelection=(props)=>{
        setWallTick(false)
        setFloorTick(false)
        setCeilingTick(false)
        setBinsTick(false)
        setDownRampTick(false)
        setWaterCoolerTick(false)
        setSeatTick(false)
        setCleanAllTick(false)
        setOtherFixturesTick(false)
        setAirconTick(false)
        setHandRailTick(false)

        setWallFinishesGoodCondition("")
        setFloorFinishesGoodCondition("")
        setCeilingFinishesGoodCondition("")
        setDownRampGoodCondition("")
        setBinsGoodCondition("")
        setWaterCoolerGoodCondition("")
        setSeatsGoodCondition("")
        setOtherFixturesGoodCondition("")
        setAirconGoodCondition("")
        setHandRailingGoodCondition("")
        setAllNeedsCleaning(false)

        props.closeAvailableChecklistModal()
    }

    
    
    const sendInfo =(e, flight_id,gate) =>{
        
        let time = moment().format("YYYY-MM-DD HH:mm:ss")
        // console.log("TIME:")
        // console.log(time)


        let data = {
        action:constants.ACTION_UPDATE_VALIDATE_INSTRUCTION,
        flight_id: flight_id,
        user_id: user.idToken.payload['cognito:username'],
        ceiling_finishes_good_condition: ceilingFinishesGoodCondition,
        wall_finishes_good_condition: wallFinishesGoodCondition, 
        floor_finishes_good_condition: floorFinishesGoodCondition,
        seats_good_condition: seatsGoodCondition,
        bins_good_condition: binsGoodCondition,
        down_ramp_good_condition: downRampGoodCondition,
        other_fixtures_good_condition: otherFixturesGoodCondition,
        last_cleaned_start: time,
        cleaning_status: "CLEANING",
        last_cleaned_complete: "",
        access_token: user.accessToken.jwtToken,
        water_cooler_good_condition: waterCoolerGoodCondition,
        current_gate: gate,
        hand_railings_good_condition: handRailingGoodCondition,
        air_con_good_condition: airconGoodCondition
        };

        // console.log("DATA MODAL", data);
        // console.log(wsSend(data))
        dispatch(wsSend(data));


        if (window.localStorage.getItem("dataList") === null || window.localStorage.getItem("dataList") === undefined) {
            dataList.push(data)
        } else {
            dataList = window.localStorage.getItem("dataList");

            // console.log("string test", dataList);

            dataList = dataList ? JSON.parse(dataList) : [];
            // dataList = JSON.parse(dataList);

            dataList.push(data)
        
            // console.log("data list array:", dataList);
        }

        window.localStorage.setItem("dataList", JSON.stringify(dataList));

        

        // setCleaningList(localStorage.setItem("cleaningList", dataList));
        //setting the cleaningList state
        // setCleaningList(dataList);

        // console.log('CLEANING LIST', cleaningList);

        // //passing the data to the 
        // dispatch(updateCleaningData(cleaningList)); 

        // console.log("CLEANING DATA1", cleaningData);

        // console.log("RESTORING DEFAULT SETTINGS ")

        setWallFinishesGoodCondition("")
        setFloorFinishesGoodCondition("")
        setCeilingFinishesGoodCondition("")
        setDownRampGoodCondition("")
        setBinsGoodCondition("")
        setWaterCoolerGoodCondition("")
        setSeatsGoodCondition("")
        setOtherFixturesGoodCondition("")
        setAirconGoodCondition("")
        setHandRailingGoodCondition("")
        setAllNeedsCleaning(false)

        setWallTick(false)
        setWaterCoolerTick(false)
        setOtherFixturesTick(false)
        setBinsTick(false)
        setSeatTick(false)
        setDownRampTick(false)
        setCeilingTick(false)
        setFloorTick(false)
        setCleanAllTick(false)
        setAirconTick(false)
        setHandRailTick(false)
        props.closeAvailableChecklistModal()

    }

    useEffect(()=> {
        // console.log(allNeedsCleaning)
        if(wallFinishesGoodCondition == "TRUE" ||
            ceilingFinishesGoodCondition == "TRUE" || 
            floorFinishesGoodCondition == "TRUE" || 
            waterCoolerGoodCondition == "TRUE" || 
            seatsGoodCondition == "TRUE" || 
            binsGoodCondition == "TRUE" || 
            downRampGoodCondition == "TRUE" || 
            otherFixturesGoodCondition == "TRUE" || 
            handRailingGoodCondition == "TRUE" ||
            airconGoodCondition == "TRUE"||
            allNeedsCleaning == true){
                setCheckboxCheck(true)
    }
        else {
                    // console.log("button is now unavailable")
                    setCheckboxCheck(false)
        }

    },[wallFinishesGoodCondition,
        ceilingFinishesGoodCondition,
        floorFinishesGoodCondition,
        waterCoolerGoodCondition,
        seatsGoodCondition,
        binsGoodCondition,
        downRampGoodCondition,
        otherFixturesGoodCondition,
        allNeedsCleaning, handRailingGoodCondition,airconGoodCondition,
     cleaningData])
    


    let count = 0
    const counter = () =>{
        
        count++
        return count
    }



    return (
        <div className="Available-Modal">
            <Modal
                className="availableModal pre-cleaning-modal"
                title=""
                footer=""
                visible={props.availableModalVisibility}
                onOk={''}
                closeIcon=" "
                onCancel={ () => cancelSelection(props)}
            > 
            <Row align="center" className="checklist-header">
                START CLEANING
            </Row>
            <Row align="center" className="checklist-subHeader">
                for gate
            </Row>
            <Row align="center" className="checklist-gate">
                {props.gate}
            </Row>
            <Row style={{marginTop: "20px"}}>
                <Col span={2} style={{margin:"0px"}}></Col>
                <Col span={8} className="checklist-colHeader">
                    ITEM
                </Col>
                <Col span={10} className="checklist-colHeader">
                GOOD CONDITION
                </Col>
            </Row>
            <Divider className="checklist-divider1" />
            {/* wall finishes*/}
            {outputChecklist && outputChecklist.wall_finishes_good_condition ? <Row className="checklist-row" align="middle">
                <Col span={1} align="center" className="checklist-colText checklist-colText1">
                {counter()}
                </Col>
                <Col span={8} align="center" className="checklist-colText checklist-colText2">
                {outputChecklist.wall_finishes_good_condition}
                </Col>
                <Col span={10} align="center" className="checklist-rowCheck">
                {allNeedsCleaning ? <Checkbox  disabled checked={false}/> :
                <Checkbox onChange={e => checkWallFinishes(e)}  checked={wallTick}/>}
                </Col>
            </Row> :null}
            {outputChecklist && outputChecklist.wall_finishes_good_condition ? <Divider className="checklist-divider2" />: null}

            {/* floor finishes*/}
            {outputChecklist && outputChecklist.floor_finishes_good_condition ? <Row className="checklist-row" align="middle">
                <Col span={1} align="center" className="checklist-colText checklist-colText1">
                {counter()}
                </Col>
                <Col span={8} align="center" className="checklist-colText checklist-colText2">
                {outputChecklist.floor_finishes_good_condition}
                </Col>
                <Col span={10} align="center" className="checklist-rowCheck">
                {allNeedsCleaning ? <Checkbox  disabled checked={false}/> :
                <Checkbox onChange={e => checkFloorFinishes(e)}  checked={floorTick}/>}
                </Col>
            </Row> :null}
            {outputChecklist && outputChecklist.floor_finishes_good_condition ? <Divider className="checklist-divider2" />: null}

            {/* ceiling finishes*/}
            {outputChecklist && outputChecklist.ceiling_finishes_good_condition ? <Row className="checklist-row" align="middle">
                <Col span={1} align="center" className="checklist-colText checklist-colText1">
                {counter()}
                </Col>
                <Col span={8} align="center" className="checklist-colText checklist-colText2">
                {outputChecklist.ceiling_finishes_good_condition}
                </Col>
                <Col span={10} align="center" className="checklist-rowCheck">
                {allNeedsCleaning ? <Checkbox  disabled checked={false}/> :
                <Checkbox onChange={e => checkCeilingFinishes(e)}  checked={ceilingTick}/>}
                </Col>
            </Row> :null}
            {outputChecklist && outputChecklist.ceiling_finishes_good_condition ? <Divider className="checklist-divider2" />: null}

            {/* water cooler*/}
            {outputChecklist && outputChecklist.water_cooler_good_condition ? <Row className="checklist-row" align="middle">
                <Col span={1} align="center" className="checklist-colText checklist-colText1">
                {counter()}
                </Col>
                <Col span={8} align="center" className="checklist-colText checklist-colText2">
                {outputChecklist.water_cooler_good_condition}
                </Col>
                <Col span={10} align="center" className="checklist-rowCheck">
                {allNeedsCleaning ? <Checkbox  disabled checked={false}/> :
                <Checkbox onChange={e => checkWaterCooler(e)}  checked={waterCoolerTick}/>}
                </Col>
            </Row> :null}
            {outputChecklist && outputChecklist.water_cooler_good_condition ? <Divider className="checklist-divider2" />: null}
            
            {/* seats*/}
            {outputChecklist && outputChecklist.seats_good_condition ? <Row className="checklist-row" align="middle">
                <Col span={1} align="center" className="checklist-colText checklist-colText1">
                {counter()}
                </Col>
                <Col span={8} align="center" className="checklist-colText checklist-colText2">
                {outputChecklist.seats_good_condition}
                </Col>
                <Col span={10} align="center" className="checklist-rowCheck">
                {allNeedsCleaning ? <Checkbox  disabled checked={false}/> :
                <Checkbox onChange={e => checkSeats(e)}  checked={seatTick}/>}
                </Col>
            </Row> :null}
            {outputChecklist && outputChecklist.seats_good_condition ? <Divider className="checklist-divider2" />: null}

            {/* bins*/}
            {outputChecklist && outputChecklist.bins_good_condition ? <Row className="checklist-row" align="middle">
                <Col span={1} align="center" className="checklist-colText checklist-colText1">
                {counter()}
                </Col>
                <Col span={8} align="center" className="checklist-colText checklist-colText2">
                {outputChecklist.bins_good_condition}
                </Col>
                <Col span={10} align="center" className="checklist-rowCheck">
                {allNeedsCleaning ? <Checkbox  disabled checked={false}/> :
                <Checkbox onChange={e => checkBins(e)}  checked={binsTick}/>}
                </Col>
            </Row> :null}
            {outputChecklist && outputChecklist.bins_good_condition ? <Divider className="checklist-divider2" />: null}

                {/* downramp*/}
                {outputChecklist && outputChecklist.down_ramp_good_condition ? <Row className="checklist-row" align="middle">
                <Col span={1} align="center" className="checklist-colText checklist-colText1">
                {counter()}
                </Col>
                <Col span={8} align="center" className="checklist-colText checklist-colText2">
                {outputChecklist.down_ramp_good_condition}
                </Col>
                <Col span={10} align="center" className="checklist-rowCheck">
                {allNeedsCleaning ? <Checkbox  disabled checked={false}/> :
                <Checkbox onChange={e => checkDownRamp(e)}  checked={downRampTick}/>}
                </Col>
            </Row> :null}
            {outputChecklist && outputChecklist.down_ramp_good_condition ? <Divider className="checklist-divider2" />: null}
            

            {/* hand railing*/}
            {outputChecklist && outputChecklist.hand_railings_good_condition ? <Row className="checklist-row" align="middle">
                <Col span={1} align="center" className="checklist-colText checklist-colText1">
                {counter()}
                </Col>
                <Col span={8} align="center" className="checklist-colText checklist-colText2">
                {outputChecklist.hand_railings_good_condition}
                </Col>
                <Col span={10} align="center" className="checklist-rowCheck">
                {allNeedsCleaning ? <Checkbox  disabled checked={false}/> :
                <Checkbox onChange={e => checkHandRail(e)}  checked={handRailTick}/>}
                </Col>
            </Row> :null}
            {outputChecklist && outputChecklist.hand_railings_good_condition ? <Divider className="checklist-divider2" />: null}

            {/* Other Fixtures*/}
            {outputChecklist && outputChecklist.other_fixtures_good_condition ? <Row className="checklist-row" align="middle">
                <Col span={1} align="center" className="checklist-colText checklist-colText1">
                {counter()}
                </Col>
                <Col span={8} align="center" className="checklist-colText checklist-colText2">
                {outputChecklist.other_fixtures_good_condition}
                </Col>
                <Col span={10} align="center" className="checklist-rowCheck">
                {allNeedsCleaning ? <Checkbox  disabled checked={false}/> :
                <Checkbox onChange={e => checkOtherFixtures(e)}  checked={otherFixturesTick}/>}
                </Col>
            </Row> :null}
            {outputChecklist && outputChecklist.other_fixtures_good_condition ? <Divider className="checklist-divider2" />: null}
            
            {/* air con*/}
            {outputChecklist && outputChecklist.air_con_good_condition ? <Row className="checklist-row" align="middle">
                <Col span={1} align="center" className="checklist-colText checklist-colText1">
                {counter()}
                </Col>
                <Col span={8} align="center" className="checklist-colText checklist-colText2">
                {outputChecklist.air_con_good_condition}
                </Col>
                <Col span={10} align="center" className="checklist-rowCheck">
                {allNeedsCleaning ? <Checkbox  disabled checked={false}/> :
                <Checkbox onChange={e => checkAircon(e)}  checked={airconTick}/>}
                </Col>
            </Row> :null}
            {outputChecklist && outputChecklist.air_con_good_condition ? <Divider className="checklist-divider2" />: null}


            {/* <Row className="checklist-row" align="middle">
                <Col span={1} align="center" className="checklist-colText checklist-colText1">
                1
                </Col>
                <Col span={8} align="center" className="checklist-colText checklist-colText2">
                Wall Finishes
                </Col>
                <Col span={10} align="center" className="checklist-rowCheck">
                {allNeedsCleaning ? <Checkbox  disabled checked={false}/> :
                <Checkbox onChange={e => checkWallFinishes(e)}  checked={wallTick}/>}
                </Col>
            </Row>
            <Divider className="checklist-divider2" />

            <Row className="checklist-row" align="middle">
                <Col span={1} align="center" className="checklist-colText checklist-colText1">
                2
                </Col>
                <Col span={8} align="center" className="checklist-colText checklist-colText2">
                Floor Finishes
                </Col>
                <Col span={10} align="middle" className="checklist-rowCheck" >
                {allNeedsCleaning ? <Checkbox disabled checked={false}/>:
                <Checkbox onChange={e => checkFloorFinishes(e)} checked={floorTick}/>} 
                </Col>
            </Row>
            <Divider className="checklist-divider2" />

            {(props.terminal == "1" || props.terminal == "2") && 
            <Row align="middle" className="checklist-row">
                <Col span={1} align="center" className="checklist-colText checklist-colText1">
                3
                </Col>
                <Col span={8} align="center" className="checklist-colText checklist-colText2">
                Ceiling Finishes
                </Col>
                <Col span={10} align="center" className="checklist-rowCheck">
                {allNeedsCleaning ? <Checkbox disabled checked={false}/>:
                <Checkbox onChange={e => checkCeilingFinishes(e)} checked={ceilingTick}/>}
                </Col>
            </Row> }
            {(props.terminal == "1"|| props.terminal == "2") && 
            <Divider className="checklist-divider2" />}

            {(props.terminal == "4") && 
            <Row align="middle" className="checklist-row">
                <Col span={1} align="center" className="checklist-colText checklist-colText1">
                3
                </Col>
                <Col span={8} align="center" className="checklist-colText checklist-colText2">
                Seats
                </Col>
                <Col span={10} align="center" className="checklist-rowCheck">
                {allNeedsCleaning ? <Checkbox disabled checked={false}/>:
                <Checkbox onChange={e => checkSeats(e)} checked={seatTick}/>}
                </Col>
            </Row> }
            {(props.terminal == "4") && 
            <Divider  className="checklist-divider2" />}

            {(props.terminal == "4") && 
            <Row align="middle" className="checklist-row">
                <Col span={1} align="center" className="checklist-colText checklist-colText1">
                4
                </Col>
                <Col span={8} align="center" className="checklist-colText checklist-colText2">
                Bins
                </Col>
                <Col span={10} align="center" className="checklist-rowCheck">
                {allNeedsCleaning ? <Checkbox disabled checked={false}/>:
                <Checkbox onChange={e => checkBins(e)} checked={binsTick}/>}
                </Col>
            </Row> }
            {(props.terminal == "4") && 
            <Divider  className="checklist-divider2" />}

            {(props.terminal == "4") && 
            <Row align="middle" className="checklist-row">
                <Col span={1} align="center" className="checklist-colText checklist-colText1">
                5
                </Col>
                <Col span={8} align="center" className="checklist-colText checklist-colText2">
                Down Ramp
                </Col>
                <Col span={10} align="center" className="checklist-rowCheck">
                {allNeedsCleaning ? <Checkbox disabled checked={false}/>:
                <Checkbox onChange={e => checkDownRamp(e)} checked={downRampTick}/>}
                </Col>
            </Row> }
            {(props.terminal == "4") && 
            <Divider className="checklist-divider2" />}

            {(props.terminal == "1" || props.terminal == "2") && 
            <Row align="middle" className="checklist-row">
                <Col span={1} align="center" className="checklist-colText checklist-colText1">
                4
                </Col>
                <Col span={8} align="center" className="checklist-colText checklist-colText2">
                Other Fixtures
                </Col>
                <Col span={10} align="center" className="checklist-rowCheck">
                {allNeedsCleaning ? <Checkbox disabled checked={false}/>:
                <Checkbox onChange={e => checkOtherFixtures(e)} checked={otherFixturesTick}/>}
                </Col>
            </Row> }
            {(props.terminal == "1" || props.terminal == "2") && 
            <Divider className="checklist-divider2" />}

            {(props.terminal == "3") && 
            <Row align="middle"  className="checklist-row">
                <Col span={1} align="center" className="checklist-colText checklist-colText1">
                3
                </Col>
                <Col span={8} align="center"  className="checklist-colText checklist-colText2">
                Other Fixtures
                </Col>
                <Col span={10} align="center" className="checklist-rowCheck">
                {allNeedsCleaning ? <Checkbox disabled checked={false}/>:
                <Checkbox onChange={e => checkOtherFixtures(e)} checked={otherFixturesTick}/>}
                </Col>
            </Row> }
            {(props.terminal == "3") && 
            <Divider className="checklist-divider2" />} */}





            {/*final divider*/}
            <Row align="middle" className="checklist-rowAll">
                <Col span={2} align="center" style={{display:"flex", float:"left", marginLeft: "15px"}}>
                    <Checkbox className={"checkbox"} onChange={e => cleanAll(e, props.direction)} checked={cleanAllTick}/>
                </Col>
                <Col span={18} className="checklist-rowAllText">
                All items require cleaning.
                </Col>

            </Row>
            <Row align="middle" className="checklist-buttonRow">
                <Col span={10} align="center">
                    <Button
                        className={"button-cancel"} onClick={(e)=>cancelSelection(props)}
                        >
                            CANCEL
                    </Button>
                </Col>
                <Col span={10} align="center">
                    {checkboxCheck ? <Button
                        className={"button-start"}  onClick={e=>sendInfo(e, props.flight_id, props.gate)}
                        >
                                START
                            
                    </Button>:
                    <Button
                        className={"button-start"} disabled
                        >
                                START
                            
                    </Button>}
                </Col>

            </Row>
            </Modal>

        </div>
   
    );
};

export default ChecklistModal; 
