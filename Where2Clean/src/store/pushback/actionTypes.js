export const WS_CONNECT = 'WS_CONNECT';
export const WS_CONNECTING = 'WS_CONNECTING';
export const WS_CONNECTED = 'WS_CONNECTED';
export const WS_DISCONNECT = 'WS_DISCONNECT';
export const WS_DISCONNECTED = 'WS_DISCONNECTED';
export const WS_SEND = 'WS_SEND';
export const WS_UPDATE = 'WS_UPDATE';
export const WS_MESSAGE = 'WS_MESSAGE';
