import * as types from "./actionTypes";

const initialState = {
  wsOpened: false,
  gatesData: null,
  cleanedGatesData: null,
  getUpdateData: false,
};

const pushback = (state = initialState, action) => {
  switch (action.type) {
    case types.WS_CONNECTED:
      console.log("ws connected:", action.host);

      return { ...state, wsOpened: true };
    case types.WS_DISCONNECTED:
      console.log("ws disconnected");
      return { ...state, wsOpened: false, gatesData: null, cleanedGatesData: null };
    case types.WS_MESSAGE:
      let result = action.payload;

      // console.log("results:")
      console.log("results:", result)
      // console.log(typeof (result))
      let gatesdata = null;
      let cleanedGatesData = null;
      let getUpdateData = false;
      if (result.message_type == "getGates") {

        // console.log("message type is getgates")
        gatesdata = result.data.map(e => {
          // e.message_type = result.message_type;
          // e.aircraft_size = result.aircraft_size;
          e.message_type = result.message_type;
          return e;

        });
      }  else if (result.message_type == "onGateUpdate") {
        console.log("message type is onGateUpdate");
        // console.log("state", ...state.gatesData);
        // console.log(...state.gatesData)
        let tempGatesData = state.gatesData ? [...state.gatesData] : [];
        // let tempCleanedGatesData = state.cleanedGatesData ? [...state.cleanedGatesData] : [];
        // let tempGatesData =  [...state.gatesData];
        console.log("results reducer", result);
        // console.log(result)
        // console.log(result.data[1])

        for (let key = 0; key < result.data.length; key++) {
          const element = result.data[key].new_image;

          const index = tempGatesData.findIndex(
            pbdata => 
              pbdata.current_gate === element.current_gate
            // pbdata.flight_id === element.flight_id
            // pbdata.flight_no === element.flight_no
          );
          console.log("index onGateUpdate", index);
          if (index == -1) {
            tempGatesData = [...tempGatesData, element];
          } else {
            tempGatesData = [
              ...tempGatesData.slice(0, index), // everything before current passenger
              element,
              ...tempGatesData.slice(index + 1) // everything after current passenger
            ];
          }

          
        }
        
        gatesdata = tempGatesData.map(e => {
          e.message_type = result.message_type;
          return e;
        });

        getUpdateData = true;



      } else if (result.message_type == "getCleanedFlights") {
        console.log("message type is getCleanedFlights", result.data);

         // get getsData 
        //  let tempGatesData = [...state.gatesData];
        let tempGatesData = state.gatesData ? [...state.gatesData] : [];
        gatesdata = tempGatesData;

        //  gatesdata = tempGatesData.map(e => {
        //    e.message_type = result.message_type;
        //    return e;
        //  });

        // console.log("data reducer", result);
        if (result.data) {
          cleanedGatesData = result.data.map(e => {
            e.message_type = result.message_type;
            return e;
          });
        } else {
          cleanedGatesData = []
        }
       
      } else if (result.message_type == "update") {
        console.log("message type is update")
        let tempGatesData = [...state.gatesData];

        const element = result.data.new_record;

        const index = tempGatesData.findIndex(
          pbdata => pbdata.flight_id === element.flight_id
        );
        console.log("index update", index);
        if (index == -1) {
          tempGatesData = [...tempGatesData, element];
        } else {
          tempGatesData = [
            ...tempGatesData.slice(0, index), // everything before current passenger
            element,
            ...tempGatesData.slice(index + 1) // everything after current passenger
          ];
        }

        gatesdata = tempGatesData.map(e => {
          e.message_type = result.message_type;
          return e;
        });
      } else {
        gatesdata = state.gatesData.map(e => {
          e.message_type = result.message_type;
          return e;
        });
      }

      return { ...state, gatesData: gatesdata, cleanedGatesData: cleanedGatesData, getUpdateData: getUpdateData };

    default:
      return state;
  }
};

export default pushback;
