import * as actions from "./actions";
import * as types from "./actionTypes";
import constants from "../../configs/constants";
import sessionStorageValue from "../../components/Login";


const socketMiddleware = () => {
  let socket = null;


  const onOpen = store => event => {
    console.log("ONOPEN FIRST")
    store.dispatch(actions.wsConnected(event.target.url));
    // console.log("----event----", event);
  };

  const onClose = store => () => {
    store.dispatch(actions.wsDisconnected());
  };

  const onMessage = store => event => {
    // console.log("ONMESSAGE SECOND", event.data)
    // console.log("event")
    // console.log(event)
    // console.log("event.data")
    // console.log(event.data)
    // console.log(JSON.parse(event.data))
    const payload = JSON.parse(event.data);
    console.log("payload to seeeeeend", payload)
    // console.log(payload)

    // console.log("received payload:", payload);
    if (payload.message_type) {
      store.dispatch(actions.wsMessage(payload));
    }
  };


  return store => next => action => {
    switch (action.type) {
      case types.WS_CONNECT:
        if (socket !== null) {
          socket.close();
        }
        // console.log("CONNECT FIRST?")
        // connect to the remote host
        // socket = new WebSocket("wss://j4hu4g1fg3.execute-api.ap-southeast-1.amazonaws.com/devW2C?user_id=supervisor1@ghrc.com");
        // socket = new WebSocket("wss://j4hu4g1fg3.execute-api.ap-southeast-1.amazonaws.com/devW2C?user_id=" + "CAG_T3_FM")
        socket = new WebSocket(action.host);
        // websocket handlers
        socket.onmessage = onMessage(store);
        socket.onclose = onClose(store);
        socket.onopen = onOpen(store);
        // console.log("STORE", onMessage(store));
        // console.log(onMessage(store))





        break;
      case types.WS_DISCONNECT:
        if (socket !== null) {
          socket.close();
        }
        socket = null;
        break;
      case types.WS_SEND:
        // console.log("wssend second")
        const accessToken = localStorage.getItem("accessToken")
        socket.send(JSON.stringify({ "action": "getGates", "access_token": accessToken }));
        console.log("PAYYYYLOAD", action.payload)
        // console.log(action.payload)
        socket.send(JSON.stringify(action.payload));
        break;
      case types.WS_UPDATE:
        // console.log("wssend second")
        const accessToken2 = localStorage.getItem("accessToken")
        socket.send(JSON.stringify({ "action": "getCleanedFlights", "access_token": accessToken2 }));
        console.log("PAYYYYLOAD Cleaned gates", action.payload)
        socket.send(JSON.stringify(action.payload));
        break;
      default:
        return next(action);
    }
  };
};

export default socketMiddleware();
