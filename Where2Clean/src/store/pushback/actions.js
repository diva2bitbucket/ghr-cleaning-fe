import * as types from "./actionTypes";

export const wsConnect = host => ({ type: types.WS_CONNECT, host });
export const wsConnecting = host => ({ type: types.WS_CONNECTING, host });
export const wsConnected = host => ({ type: types.WS_CONNECTED, host });
export const wsDisconnect = host => ({ type: types.WS_DISCONNECT, host });
export const wsDisconnected = host => ({ type: types.WS_DISCONNECTED, host });
export const wsSend = payload => ({ type: types.WS_SEND, payload });
export const wsUpdate = payload => ({ type: types.WS_UPDATE, payload });
export const wsMessage = payload => ({ type: types.WS_MESSAGE, payload });
