import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import reducer from './reducer'
import pushbackWSMiddleware from '../store/pushback/middleware';

const configureStore = () => {
  const middleware = [thunk, pushbackWSMiddleware]
  return createStore(reducer, applyMiddleware(...middleware))
}

export { configureStore }
