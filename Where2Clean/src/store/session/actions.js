import * as types from "./actionTypes";
import constants from "../../configs/constants";
// import cag_airline_code_mapping_csv from "../../assets/data/cag_airline_code_mapping.csv";
// import aircraft_bay_pushback_mapping_csv from "../../assets/data/aircraft_bay_pushback_mapping.csv";
// import pb_restriction_csv from "../../assets/data/aircraft_bay_pushback_mapping.csv";
import aircraft_size_mapping_csv from "../../assets/data/aircraft_size_mapping.csv";
import gate_mapping_csv from "../../assets/data/gate_mapping.csv";
import {
  CognitoUserPool,
} from "amazon-cognito-identity-js";

const Papa = require("papaparse");

export const restoreSession = () => {
  return dispatch => {
    if (constants.DEBUG) {
      console.log("restoring session...");
    }
    dispatch({ type: types.RESTORE_SESSION_REQUEST });
    Papa.parse(aircraft_size_mapping_csv, {
      download: true,
      header: true,
      complete: function (aircraft_size_mapping_json) {
        console.log(aircraft_size_mapping_json);
        Papa.parse(gate_mapping_csv, {
          download: true,
          header: true,
          complete: function (gate_mapping_json) {
            console.log("GATE MAPPING JSON ", gate_mapping_json);
            let userPool = new CognitoUserPool({
              UserPoolId: constants.USERPOOLID,
              ClientId: constants.CLIENTID,
            });

            dispatch({
              type: types.RESTORE_SESSION_SUCCESS,
              aircraftSizeMapping: aircraft_size_mapping_json,
              gateMapping: gate_mapping_json,
              //pushbackMapping
              //airlineCodeMapping
              userPool
            });
          }
        });
      }
    });
  };
};

export const clearError = () => {
  return dispatch => {
    dispatch({ type: types.CLEAR_ERROR_REQUEST });
  };
};

export const updateUser = (user) => {
  return dispatch => {
    dispatch({ type: types.UPDATE_USER_REQUEST, user });
  };
};


export const getPbRestriction = () => {
  return dispatch => {
    // Papa.parse(pb_restriction_csv, {
    //   download: true,
    //   header: true,
    //   complete: function(pbRestriction_json) {
    //     console.log("actions.js");
    //     console.log(pbRestriction_json);
    //     dispatch({
    //       type: types.GET_PB_RESTRICTION,
    //       pbRestrictionMapping: pbRestriction_json 
    //     });
    //   }
    // })

  };
};

