import * as types from "./actionTypes";

const initialState = {
  restoring: null,
  loading: false,
  error: null,
  user: null,
  userPool: null,
  aircraftSizeMapping: null,
  gateMapping: null,
  wsOpened: false,
  // gatesData: null,
  // pbRestrictionMapping: null
};

const session = (state = initialState, action) => {
  switch (action.type) {
    case types.RESTORE_SESSION_REQUEST:
      return { ...state, restoring: true };
    case types.RESTORE_SESSION_SUCCESS:
      
      return {
        ...state,
        restoring: false,
        userPool: action.userPool,
        aircraftSizeMapping: action.aircraftSizeMapping,
        gateMapping: action.gateMapping
        
      };
    case types.RESTORE_SESSION_FAIL:
      return { ...state, restoring: false };
    case types.CLEAR_ERROR_REQUEST:
      return { ...state, error: null };
    case types.UPDATE_USER_REQUEST:
      return { ...state, user: action.user };
    // case types.GET_PB_RESTRICTION:
    //   return {
    //     ...state,
    //     pbRestrictionMapping: action.pbRestrictionMapping
    //   };
    default:
      return state;
  }
};

export default session;
