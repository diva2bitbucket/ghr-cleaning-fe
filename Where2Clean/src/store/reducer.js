import { combineReducers } from 'redux'

import session from './session';
import pushback from './pushback'

export default combineReducers({
  session,
  pushback
})
 