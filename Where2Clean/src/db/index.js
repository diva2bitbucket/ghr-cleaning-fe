export const appDB = {
  DB_NAME: "appDB",
  DB_VERSION: 1,
  DB_STORE_NAME: "appDBStorage",
  DB_STORE_KEY_PATH: "key",
  instance: {},

  open: () => {
    let promise = new Promise((resolve, reject) => {
      var request = window.indexedDB.open(appDB.DB_NAME, appDB.DB_VERSION);
      request.onerror = error => {
        reject(error);
        console.log("error:", error.target.code);
      };

      request.onupgradeneeded = e => {
        var _db = e.target.result,
          names = _db.objectStoreNames,
          name = appDB.DB_STORE_NAME;

        if (!names.contains(name)) {
          _db.createObjectStore(name, {
            keyPath: appDB.DB_STORE_KEY_PATH,
            autoIncrement: true
          });
        }
        resolve(null);
      };

      request.onsuccess = function(e) {
        appDB.instance = request.result;

        appDB.instance.onerror = error => {
          reject(error);
          console.log("error:", error.target.code);
        };
        resolve(null);
      };
    });
    return promise;
  },

  getObjectStore: mode => {
    var txn, store;
    mode = mode || "readonly";
    txn = appDB.instance.transaction([appDB.DB_STORE_NAME], mode);
    store = txn.objectStore(appDB.DB_STORE_NAME);
    return store;
  },

  save: data => {
    let promise = new Promise((resolve, reject) => {
      appDB.open().then(() => {
        var store,
          request,
          mode = "readwrite";

        store = appDB.getObjectStore(mode);

        request = data[appDB.DB_STORE_KEY_PATH]
          ? store.put(data)
          : store.add(data);

        request.onsuccess = () => {
          resolve(null);
        };
      });
    });
    return promise;
  },

  getAll: () => {
    let promise = new Promise((resolve, reject) => {
      appDB.open().then(() => {
        var store = appDB.getObjectStore(),
          cursor = store.openCursor(),
          data = [];

        cursor.onsuccess = function(e) {
          var result = e.target.result;

          if (result && result !== null) {
            data.push(result.value);
            result.continue();
          } else {
            resolve(data);
          }
        };
      });
    });
    return promise;
  },

  get: key => {
    let promise = new Promise((resolve, reject) => {
      appDB.open().then(() => {
        var store = appDB.getObjectStore(),
          request = store.get(key);

        request.onsuccess = e => {
          resolve(e.target.result);
        };
      });
    });
    return promise;
  },

  delete: key => {
    let promise = new Promise((resolve, reject) => {
      appDB.open().then(() => {
        var mode = "readwrite",
          store,
          request;

        store = appDB.getObjectStore(mode);

        request = store.delete(key);

        request.onsuccess = e => {
          resolve(null);
        };
      });
    });
    return promise;
  },

  deleteAll: () => {
    let promise = new Promise((resolve, reject) => {
      appDB.open().then(() => {
        var mode, store, request;

        mode = "readwrite";
        store = appDB.getObjectStore(mode);
        request = store.clear();

        request.onsuccess = e => {
          resolve(null);
        };
      });
    });
    return promise;
  }
};
