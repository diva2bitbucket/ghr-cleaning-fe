import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import "./styles.scss";
import { configureStore } from "../src/store";
import { HashRouter as Router } from "react-router-dom";
import * as serviceWorker from "./serviceWorker";

const store = configureStore();
const { Provider } = require("react-redux");

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>,
  document.getElementById("root")
);

serviceWorker.unregister();