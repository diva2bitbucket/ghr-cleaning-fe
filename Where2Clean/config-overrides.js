const { override, fixBabelImports, addLessLoader } = require("customize-cra");

function myOverrides(config) {
  config.module.rules = config.module.rules.map(rule => {
    if (rule.oneOf instanceof Array) {
      return {
        ...rule,
        oneOf: [
          {
            test: /\.(svg|png|jpg|jpeg|gif|bmp|tiff|csv|pdf|mp3)$/i,
            use: [
              {
                loader: "file-loader",
                options: {
                  name: "content/dam/way2go/where2clean/static/media/" + "[name].[ext]"
                }
              }
            ]
          },
          ...rule.oneOf
        ]
      };
    }

    return rule;
  });
  return config;
}

module.exports = override(
  myOverrides,
  fixBabelImports("import", {
    libraryName: "antd",
    libraryDirectory: "es",
    style: true
  }),
  addLessLoader({
    javascriptEnabled: true,
    modifyVars: {
      "@primary-color": "#4F5074",
      "@font-size-base": "15px",
      "@btn-height-lg": "45px",
      "@input-height-lg": "45px",
      "@drawer-body-padding": "0px",
      "@process-icon-color": "#5D5145",
      "@process-title-color": "#5D5145",
      "@process-tail-color": "#8c8c8c",
      "@wait-tail-color": "#8c8c8c",
      "@finish-tail-color": "#5D5145",
      "@btn-disable-color": "#FFFFFF",
      "@btn-disable-bg": "#C4C4C4",
      "@rate-star-bg": "#C4C4C4",
      "@input-bg": "#1B1C28",
      "@input-color": "#FFFFFF",
      "@radio-button-bg": "#1B1C28",
      "@radio-button-color": "#FFFFFF"
    }
  })
);
